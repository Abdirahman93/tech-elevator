package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataStringCalculatorTest {
	
private KataStringCalculator calculator;
	
	@Before
	public void setup(){
		calculator = new KataStringCalculator(); 
	}
	
	@Test
	public void empty_results_zero(){
		int sum = calculator.add("");
		Assert.assertEquals(0, sum);
	}
	
	@Test
	public void one_results_one(){
		int sum = calculator.add("1");
		Assert.assertEquals(1, sum);
	}
	
	@Test
	public void one_plus_two_results_three(){
		int sum = calculator.add("1,2");
		Assert.assertEquals(3, sum);
	}
	
	@Test
	public void one_plus_two_plus_three_results_six(){
		int sum = calculator.add("1,2,3");
		Assert.assertEquals(6, sum);
	}
	
	@Test
	public void random(){
		int sum = calculator.add("45,25,26,223,53");
		Assert.assertEquals(372, sum);
	}
	
	@Test
	public void one_plus_two_results_three_comma_at_end(){
		int sum = calculator.add("1,2,");
		Assert.assertEquals(3, sum);
	}
	
	@Test
	public void numbers_with_spaces(){
		int sum = calculator.add("1\n2\n3");
		Assert.assertEquals(6, sum);
	} 
	
	@Test
	public void numbers_with_space_at_end(){
		int sum = calculator.add("1\n2\n3\n");
		Assert.assertEquals(6, sum);
	}
	
	@Test
	public void numbers_with_spaces_and_commas(){
		int sum = calculator.add("3\n5\n2,4");
		Assert.assertEquals(14, sum);
	}
	
//	@Test
//	public void numbers_with_delimeters(){
//		int sum = calculator.add("//;\n1;2");
//		Assert.assertEquals(3, sum);
//	}
//	
//	@Test
//	public void numbers_with_different_delimeters(){
//		int sum = calculator.add("//!\n4!9");
//		Assert.assertEquals(13, sum);
//	}
//	
}
