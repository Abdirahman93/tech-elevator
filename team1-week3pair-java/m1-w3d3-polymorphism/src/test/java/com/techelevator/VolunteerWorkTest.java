package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VolunteerWorkTest {

	private VolunteerWorker bobby;
	
	@Before
	public void setup(){
		bobby = new VolunteerWorker();
	}
	
	@Test
	public void initializes_properly(){
		Assert.assertEquals("Bobby", bobby.getFirstName());
		Assert.assertEquals("Perry", bobby.getLastName());
	}
	
	@Test
	public void calculate_weekly_(){
		Assert.assertEquals(0, bobby.calculateWeeklyPay(40), 0.01);
	}
}
