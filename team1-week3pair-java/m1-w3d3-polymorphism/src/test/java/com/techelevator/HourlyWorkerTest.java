package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HourlyWorkerTest {
	
	private HourlyWorker lebron;
	
	@Before
	public void setup(){
		lebron = new HourlyWorker(20);
	}
	
	@Test
	public void initializes_properly(){
		Assert.assertEquals("Lebron", lebron.getFirstName());
		Assert.assertEquals("James", lebron.getLastName());
	}
	
	@Test
	public void calculate_weekly_without_overtime(){
		Assert.assertEquals(800, lebron.calculateWeeklyPay(40), 0.01);
	}
	
	@Test
	public void calculate_weekly_with_overtime(){
		Assert.assertEquals(1100, lebron.calculateWeeklyPay(50), 0.01);
	}
}
