package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SalaryWorkerTest {
	private SalaryWorker john;
	
	@Before
	public void setup(){
		john = new SalaryWorker(52000);
	}
	
	@Test
	public void initializes_properly(){
		Assert.assertEquals("John", john.getFirstName());
		Assert.assertEquals("Doe", john.getLastName());
	}
	
	@Test
	public void calculate_weekly(){
		Assert.assertEquals(1000, john.calculateWeeklyPay(40), 0.01);
	}
}
