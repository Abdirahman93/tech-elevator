package com.techelevator;

public class SalaryWorker implements WorkerInterface {

	private double annualSalary;
	
	public SalaryWorker(double annualSalary){
		this.annualSalary = annualSalary;
	}
	
	@Override
	public String getFirstName() {
		return "John";
	}

	@Override
	public String getLastName() {
		return "Doe";
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		return annualSalary/52;
	}
}
