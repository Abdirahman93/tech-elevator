package com.techelevator;

public class Program {
	public static void main (String[] args){
		int totalHours = 0;
		float totalPay = 0;
		WorkerInterface[] workers = new WorkerInterface[] { new SalaryWorker(100000.00), new HourlyWorker(30), new VolunteerWorker()};
		
		for (WorkerInterface eachworker: workers){
			String employee = eachworker.getLastName() + ", " + eachworker.getFirstName();
			int hoursWorked = (int)(Math.random() + 1) * 10; 
			float pay = (float)eachworker.calculateWeeklyPay(hoursWorked);
			totalHours = totalHours + hoursWorked;
			totalPay = totalPay + pay;
			System.out.println("Employee: "+ employee + " Hours Worked: "+ hoursWorked + " Pay: " + pay);
		}
		System.out.println("Total Hours: " + totalHours);
		System.out.println("Total Pay: " + totalPay);
	}
}
