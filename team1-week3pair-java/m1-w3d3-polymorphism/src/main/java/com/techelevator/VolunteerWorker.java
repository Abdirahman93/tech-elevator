package com.techelevator;

public class VolunteerWorker implements WorkerInterface {
	
	public VolunteerWorker(){
	}
	
	@Override
	public String getFirstName() {
		return "Bobby";
	}

	@Override
	public String getLastName() {
		return "Perry";
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		return hoursWorked * 0;
	}
}
