package com.techelevator;

public interface WorkerInterface {
	
	String getFirstName( );
	String getLastName( );
	double calculateWeeklyPay(int hoursWorked);
	
}
