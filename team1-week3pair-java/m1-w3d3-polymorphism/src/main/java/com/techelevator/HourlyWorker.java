package com.techelevator;

public class HourlyWorker implements WorkerInterface {

	private double hourlyRate;
	
	public HourlyWorker(double hourlyRate){
		this.hourlyRate = hourlyRate;
	}
	
	@Override
	public String getFirstName() {
		return "Lebron";
	}

	@Override
	public String getLastName() {
		return "James";
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		int overtime = hoursWorked - 40;
		double pay = hourlyRate * hoursWorked;
		if(overtime > 0){
			pay = hourlyRate * 40;
			return pay + (hourlyRate * overtime * 1.5);
		}
		return pay;
	}
	
}
