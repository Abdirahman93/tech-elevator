package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckingAccountTest {
	private CheckingAccount check;
	
	@Before
	public void setup(){
		check = new CheckingAccount();
	}
	
	@Test
	public void first(){
	check.deposit(new DollarAmount(100));
	DollarAmount wit = check.withdraw(new DollarAmount(40));
	Assert.assertEquals(60,check.getBalance().getCents());	
	}
	@Test
	public void withdraw_part_two(){
		check.deposit(new DollarAmount(100));
		DollarAmount wit = check.withdraw(new DollarAmount(150));
		Assert.assertEquals(-50, check.getBalance().getCents());
	}
	@Test
	public void withdraw_part_three(){
		check.deposit(new DollarAmount(100));
		DollarAmount wit = check.withdraw(new DollarAmount(15000));
		Assert.assertEquals(0, check.getBalance().getCents());
	}
	
}
