package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SavingAccountTest {

	private SavingAccount saving;
	
	@Before
	public void setup(){
		saving = new SavingAccount();
	}
	
	@Test
	public void first(){
	saving.deposit(new DollarAmount(10000));
	DollarAmount wit = saving.withdraw(new DollarAmount(5000));
	Assert.assertEquals(48, saving.getBalance().getDollars());
	
	}
	

	@Test
	public void withdraw_part_two(){
		saving.deposit(new DollarAmount(20000));
		DollarAmount wit = saving.withdraw(new DollarAmount(30000));
		Assert.assertEquals(200, saving.getBalance().getDollars());
	}
	
	@Test
	public void withdraw_part_three(){
		saving.deposit(new DollarAmount(20000));
		DollarAmount wit = saving.withdraw(new DollarAmount(20000));
		Assert.assertEquals(200, saving.getBalance().getDollars());
	}
	
}
