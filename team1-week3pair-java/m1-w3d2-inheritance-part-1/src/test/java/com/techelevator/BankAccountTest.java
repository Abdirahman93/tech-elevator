package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankAccountTest {
	private BankAccount bank;
	private DollarAmount bal;
	private DollarAmount dep;
	private DollarAmount wit;
	private DollarAmount tran;
	private BankAccount bank1;
	
	@Before
	public void setup(){
		bank = new BankAccount();
		bank1 = new BankAccount();
		bal = new DollarAmount(0);
		dep = new DollarAmount(50);
		wit = new DollarAmount(25);
	}
	
	@Test
	public void set_account_number(){
		bank.setAccountNumber("12");
		Assert.assertEquals("12",bank.getAccountNumber());
	}
	
	@Test
	public void get_balance(){
		Assert.assertEquals(0,bank.getBalance().getDollars());
	}
	
	@Test
	public void deposit(){
		bank.deposit(dep);
		Assert.assertEquals(50, bank.getBalance().getCents());
	}
	
	@Test
	public void withdraw(){
		bank.deposit(dep);
		bank.withdraw(wit);
		Assert.assertEquals(25, bank.getBalance().getCents());
	}
	
	@Test
	public void transfer(){
		tran = new DollarAmount(25);
		bal = new DollarAmount(50);
		DollarAmount startingBalance = bal;
		DollarAmount balanceAfter = bal.minus(tran);
		bank.transfer(bank1, tran);
		Assert.assertEquals(25,bank1.getBalance().getCents());
	}
	
	@Test
	public void transfer_part_two(){
		tran = new DollarAmount(0);
		bal = new DollarAmount(0);
		DollarAmount startingBalance = bal;
		DollarAmount balanceAfter = bal;
		bank.transfer(bank1, tran);
		Assert.assertTrue(startingBalance.equals(balanceAfter));
	}
}
