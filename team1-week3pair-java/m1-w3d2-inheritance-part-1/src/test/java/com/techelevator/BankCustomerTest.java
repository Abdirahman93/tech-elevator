package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

public class BankCustomerTest {
	private BankCustomer one;
	
	@Before
	public void setup(){
		one = new BankCustomer("A", "address", "1223");
	}
	
	@Test
	public void initialization(){
		Assert.assertEquals("A", one.getName());
		Assert.assertEquals("address",one.getAddress());
		Assert.assertEquals("1223", one.getPhoneNumber());
		Assert.assertNotNull(one.getAccounts());
	}
	
	@Test
	public void able_to_add_account(){
		BankAccount bankacc = new BankAccount();
		one.addAccount(bankacc);
		
		Assert.assertEquals(1, one.getAccounts().size());
		Assert.assertEquals(bankacc, one.getAccounts().get(0));
	}
	
	@Test
	public void vip(){
		BankAccount bankacc = new BankAccount();
		bankacc.deposit(new DollarAmount(3000000));
		one.addAccount(bankacc);
		Assert.assertTrue(one.isVIP());
	}
	
	
	
	
}
