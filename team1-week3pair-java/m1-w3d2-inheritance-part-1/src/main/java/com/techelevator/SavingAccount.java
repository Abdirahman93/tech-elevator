package com.techelevator;

public class SavingAccount extends BankAccount{
	@Override
	public DollarAmount withdraw(DollarAmount amountToWithdraw){

		if(getBalance().isLessThan(new DollarAmount(15000))){
		return super.withdraw(amountToWithdraw.plus(new DollarAmount(200)));
		} else if(getBalance().isLessThan(amountToWithdraw)){
		return super.getBalance();
		}else{
		return super.getBalance();
		}
	}
}

