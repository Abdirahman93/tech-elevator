package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class BankCustomer extends BankAccount {
	private String name;
	private String address;
	private String phoneNumber;
	private List<BankAccount> accounts = new ArrayList();
	
	
	public BankCustomer(String name, String address, String phoneNumber){
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	
	public String getName(){
		return name;
	}
	
	public String getAddress(){
		return address;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public List<BankAccount> getAccounts(){
		return accounts;
		
	}
	
	public void addAccount(BankAccount newAccount){ 
		accounts.add(newAccount);
	}
	
	public boolean isVIP(){
		DollarAmount threshhold = new DollarAmount(2500000);
		DollarAmount totalAmount = new DollarAmount(0);

		for(BankAccount account : accounts){
			totalAmount = totalAmount.plus(account.getBalance());
		}
		
		return totalAmount.isGreaterThanOrEqualTo(threshhold);
	}
}


