package com.techelevator;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class StockItemsTest {
	private StockItems items;
	private List<Products> productList;
	@Before
	public void setup(){
		items = new StockItems();
		productList = items.getProducts();

		

	}
	

	
	@Test
	public void get_products(){

		Assert.assertNotNull(items);
		Assert.assertEquals(16, productList.size());
		
		for(Products product : productList) {
			Assert.assertNotNull(product.getName());
			Assert.assertNotNull(product.getSlotNumber());
			Assert.assertNotNull(product.getPrice());
			Assert.assertEquals(5, product.getQuantity());
			Assert.assertEquals(0, product.getQtySold());
		}
	}
}
