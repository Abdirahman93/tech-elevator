package com.techelevator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.techelevator.Purchase;

public class PurchaseTest {
	private Purchase purchase;
	private ByteArrayOutputStream output;
	@Before
	public void setup(){
		purchase = new Purchase(System.in,System.out);
		output = new ByteArrayOutputStream();
	}
	
	@Test
	public void set_money(){
		purchase.setMoneyIn(new DollarAmount(0));
		Assert.assertEquals(new DollarAmount(0), purchase.getMoneyIn());
	}
	
	@Test
	public void set_total_sales(){
		purchase.setTotalSales(new DollarAmount(0));
		Assert.assertEquals(new DollarAmount(0), purchase.getTotalSales());
	}
	
	@Test
	public void displays_a_list_of_menu_options_and_prompts_user_to_make_a_choice() {
		Object[] options = new Object[] {  new Integer(3), "Blind", "Mice" };
		Purchase purchase = getMenuForTesting();
		
		purchase.getChoiceFromOptions(options);
		
		String expected = "\n"+
				 		  "1) "+options[0].toString()+"\n" + 
						  "2) "+options[1].toString()+"\n" +
						  "3) "+options[2].toString()+"\n" +
						     "Current Money Provided: " + purchase.getMoneyIn()+"\n\n"+
						  "Please choose an option >>> ";
		Assert.assertEquals(expected, output.toString());	  
	}
	
	@Test
	public void returns_object_corresponding_to_user_choice() {
		Integer expected = new Integer(456);
		Integer[] options = new Integer[] {  new Integer(123), expected, new Integer(789) };
		Purchase purchase = getMenuForTestingWithUserInput("2\n");

		Integer result = (Integer)purchase.getChoiceFromOptions(options);
		
		Assert.assertEquals(expected, result);	  
	}
	
	@Test
	public void redisplays_menu_if_user_does_not_choose_valid_option() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Purchase purchase = getMenuForTestingWithUserInput("4\n1\n");
		
		purchase.getChoiceFromOptions(options);
		
		String menuDisplay = "\n"+
				 			 "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n" +
						     "Current Money Provided: " + purchase.getMoneyIn()+"\n\n"+
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** 4 is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void redisplays_menu_if_user_chooses_option_less_than_1() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Purchase purchase = getMenuForTestingWithUserInput("0\n1\n");
		
		purchase.getChoiceFromOptions(options);
		
		String menuDisplay = "\n"+
				 			 "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n" +
						     "Current Money Provided: " + purchase.getMoneyIn()+"\n\n"+
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** 0 is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}
	
	@Test
	public void redisplays_menu_if_user_enters_garbage() {
		Object[] options = new Object[] {  "Larry", "Curly", "Moe" };
		Purchase purchase = getMenuForTestingWithUserInput("Mickey Mouse\n1\n");
		
		purchase.getChoiceFromOptions(options);
		
		String menuDisplay = "\n"+
							 "1) "+options[0].toString()+"\n" + 
						     "2) "+options[1].toString()+"\n" +
						     "3) "+options[2].toString()+"\n" +
						     "Current Money Provided: " + purchase.getMoneyIn()+"\n\n"+
						     "Please choose an option >>> ";
		
		String expected = menuDisplay + 
					      "\n*** Mickey Mouse is not a valid option ***\n\n" +
					      menuDisplay;
		
		Assert.assertEquals(expected, output.toString());
	}

	private Purchase getMenuForTestingWithUserInput(String userInput) {
		ByteArrayInputStream input = new ByteArrayInputStream(String.valueOf(userInput).getBytes());
		return new Purchase(input, output);
	}

	private Purchase getMenuForTesting() {
		return getMenuForTestingWithUserInput("1\n");
	}
	
}
