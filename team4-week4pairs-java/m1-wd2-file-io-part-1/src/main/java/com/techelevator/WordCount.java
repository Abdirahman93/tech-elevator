package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class WordCount {

	public static void main(String[] args) {
		File file = getFileFromUser();
		
		try(Scanner wordCount = new Scanner (file)){
			int  count=0, sCount = 0;
			char punct[]={'.','!','?'}; 
			while(wordCount.hasNext()){
				String word = wordCount.next();
				count++;
				if(word.contains(".")  || word.contains("!") || word.contains("?")) {
					sCount++;
				}

			}	
			System.out.println("Number of words: " + count);
			System.out.println("Number of sentences: " + sCount);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		private static File getFileFromUser(){
			Scanner input = new Scanner(System.in);
				System.out.println("Please enter the path to the file: ");
				String path = input.nextLine();
				
				File file = new File(path);
				if(!file.exists()){
					System.out.print("That file doesn't exist");
					System.exit(1);
				}else if(!file.isFile()){
					System.out.println(path + " is a directory.");
					System.exit(1);
				}
				return file;
				

	}
}
