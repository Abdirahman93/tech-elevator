package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class FindAndReplace {

	public static void main(String[] args) {
		File file = getFileFromUser();
		File outputFile = saveFileFromUser();
		
		String search = getSearchWord();
		String replace = getReplaceWord();
		
		try(Scanner fileInput = new Scanner(file)) {
			try(PrintWriter writer = new PrintWriter(outputFile)){
				while(fileInput.hasNextLine()){
					String line = fileInput.nextLine();
					line = line.replaceAll(search, replace);
				
				writer.println(line);
				}
				System.out.println("It worked!");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("It didn't write!");
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("It didn't search!");
		}
	}
	
	private static String getSearchWord(){
		System.out.println("Please enter search word");
		Scanner input = new Scanner(System.in);
		String search = input.nextLine();
		return search;
	}

	private static String getReplaceWord(){
		System.out.println("Please enter replace word");
		Scanner input = new Scanner(System.in);
		String replace = input.nextLine();
		return replace;
	}
	
	private static File saveFileFromUser(){
		System.out.println("Where would you like to save this file?");
		Scanner userInput = new Scanner(System.in);
		String path = userInput.nextLine();
		
		File outputFile = new File(path);
		if(outputFile.exists()){
			System.out.println("File already exists!");
			System.exit(1);
		}
		
		return outputFile;
	}
	
	
	//Getting source file
	private static File getFileFromUser(){
			Scanner input = new Scanner(System.in);
				System.out.println("Please enter the path to the file: ");
				String path = input.nextLine();
				
				File file = new File(path);
				if(!file.exists()){
					System.out.println("That file doesn't exist");
					System.exit(1);
				}else if(!file.isFile()){
					System.out.println(path + " is a directory.");
					System.exit(1);
				}
				return file;
				

	}
}
