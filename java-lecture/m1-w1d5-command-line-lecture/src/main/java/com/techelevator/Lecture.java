package com.techelevator;

import java.util.Scanner;

public class Lecture {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int maxNumber = 50;
		int targetNumber = (int) (Math.random()*maxNumber)+1;
		int numOfChances = (int) ((Math.random()*5)+6);
		
		System.out.println("Guess a number (1 - " + maxNumber + "): ");
		System.out.println("You have " + numOfChances + " guesses");
		
		for(int i = 0; i < numOfChances; i++){
			int guess = input.nextInt();
			input.nextLine();
		
			if(guess == targetNumber){
				System.out.println("YOU WIN!!");
				System.exit(0);
			}else{
				System.out.println("WRONG!");
				if(guess < 0){
					System.out.println("we'r ony looking for positive numbers!");
					i--;
				}else if(guess > maxNumber){
					System.out.println("Max number is 50!");
					i--;
				} else if(guess < targetNumber){
					System.out.println("Too low!");
				}else{
					System.out.println("Too high!");
				}
			}
		}
		
		System.out.println("You have failed! The number you were lookin for was " + targetNumber);
	}

}