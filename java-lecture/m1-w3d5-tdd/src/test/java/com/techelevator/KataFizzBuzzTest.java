package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest {
	private KataFizzBuzz fizzBuzz;
	
	@Before
	public void setup(){
		fizzBuzz = new KataFizzBuzz();
	}
	
	@Test
	public void one_return_one(){
		String result = fizzBuzz.fizzBuzz(1);
		Assert.assertEquals("1", result);
	}
	
	@Test
	public void one_return_two(){
		String result = fizzBuzz.fizzBuzz(2);
		Assert.assertEquals("2", result);
	}
	
	@Test
	public void one_return_3(){
		String result = fizzBuzz.fizzBuzz(3);
		Assert.assertEquals("Fizz", result);
	}
	
	@Test
	public void one_return_4(){
		String result = fizzBuzz.fizzBuzz(4);
		Assert.assertEquals("4", result);
	}
	
	@Test
	public void one_return_five(){
		String result = fizzBuzz.fizzBuzz(5);
		Assert.assertEquals("Buzz", result);
	}
	
	@Test
	public void one_return_six(){
		String result = fizzBuzz.fizzBuzz(6);
		Assert.assertEquals("Fizz", result);
	}
	
	@Test
	public void one_return_ten(){
		String result = fizzBuzz.fizzBuzz(10);
		Assert.assertEquals("Buzz", result);
	}
	
	@Test
	public void one_return_fifteen(){
		String result = fizzBuzz.fizzBuzz(15);
		Assert.assertEquals("FizzBuzz", result);
	}
	
	@Test
	public void zere_returns_empty_string(){
		String result = fizzBuzz.fizzBuzz(0);
		Assert.assertEquals("", result);
	}
	
	@Test
	public void one_hundred_and_one_returns_empty_string(){
		String result = fizzBuzz.fizzBuzz(101);
		Assert.assertEquals("", result);
	}
	
	@Test
	public void forty_three_returns_fizz(){
		String result = fizzBuzz.fizzBuzz(43);
		Assert.assertEquals("Fizz",  result);
	}
	
	@Test
	public void thirty_seven_returns_fizz(){
		String result = fizzBuzz.fizzBuzz(37);
		Assert.assertEquals("Fizz",  result);
	}
	
	@Test
	public void fifty_four_returns_fizz(){
		String result = fizzBuzz.fizzBuzz(58);
		Assert.assertEquals("Buzz",  result);
	}
	
	@Test
	public void eighty_five_returns_fizz(){
		String result = fizzBuzz.fizzBuzz(85);
		Assert.assertEquals("Buzz",  result);
	}
	
	@Test
	public void fifty_three_returns_fizz(){
		String result = fizzBuzz.fizzBuzz(35);
		Assert.assertEquals("FizzBuzz",  result);
	}
	@Test
	public void thirty_five_returns_fizz(){
		String result = fizzBuzz.fizzBuzz(53);
		Assert.assertEquals("FizzBuzz",  result);
	}
}
