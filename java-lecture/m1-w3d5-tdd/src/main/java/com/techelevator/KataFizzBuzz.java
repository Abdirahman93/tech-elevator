package com.techelevator;

public class KataFizzBuzz {
	public String fizzBuzz(int number){
		if(number <= 0 || number > 100){
			return "";
		}else if(isFizz(number) && isBuzz(number)){
			return "FizzBuzz";
		}else if(isFizz(number)){
			return "Fizz";
		}else if(isBuzz(number)){
			return"Buzz";
		}
		return Integer.toString(number);
	}
	
	private boolean isFizz(int number){
		return number % 3 == 0 || number % 10 == 3 || number / 10 == 3;
	}
	
	private boolean isBuzz(int number){
		return number % 5 == 0 || number / 10 == 5; 
	}
	
}