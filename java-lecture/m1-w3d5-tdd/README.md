## Kata Fizz Buzz

### Step 1

Write a program that prints the numbers from 1 to 100. But for multiples of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz?".

Write tests that call a single method `fizzBuzz()`. Given a positive integer input n, return the FizzBuzz output as a string.

*Sample Output*
```
1 ->  returns "1"
2 -> returns "2"
3 -> returns "Fizz"
4 -> returns "4"
5 -> returns "Buzz" 
15 -> returns "FizzBuzz"
... etc up to 100
```

* Any number outside of the boundaries should return empty string -> ""

** Test Cases**

- What happens if 0 is passed in?
- What happens if 101 is passed in?
- What test cases can we come up with for the core FizzBuzz exercise?

### Step 2

- A number is fizz if it is divisible by 3 or it has a 3 in it
- A number is buzz if it is divisible by 5 or it has a 5 in it


[Link](http://codingdojo.org/cgi-bin/index.pl?KataFizzBuzz)