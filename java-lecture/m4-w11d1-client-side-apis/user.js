$(document).ready(function(){
	$('#userShuffle').on('click', function(event) {
		$.ajax({
			url: "https://randomuser.me/api/",
			type: "GET",
			dataType: "json"
		}).done(function(data) {
			var firstName = data.results[0].name.first;
			var lastName = data.results[0].name.last;
			$('#userName').text(firstName + ' ' + lastName);
			$('#userImage').attr('src', data.results[0].picture.large);
		}).fail(function(xhr, status, error) {
			console.log(error);
		});
	});
});