package com.techelevator;

public class Wall {
	private int height;
	private int width;
	
	public Wall(int width, int height){
		this.width = width;
		this .height = height;
	}
	
	public int getHeight(){
		return height;
	}
	// Use void when using set
	public void setHeight(int height){
	//"this" gets the instance of the class. "this" is a reference to the current object we are in.
		this.height = height;
	}
	
	public int getWidth(){
		return width;
	}
	
	public void setWidth(int width){
		this.width = width;
	}
	
	public int getArea() {
		return width * height;
	}
	//overriding the implementation
	@Override
	public String toString(){
		return Integer.toString(width) + "x" + Integer.toString(height) + " - " + getArea() + "sqft";
	}
}
