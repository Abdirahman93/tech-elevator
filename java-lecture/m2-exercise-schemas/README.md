# Module 2 Exercise Schemas

This repository contains the SQL scripts to create the Module 2 exercise databases.
* world
* dvdstore

There are mulitple versions of each script.

| Database | Server | Operating System | SQL Script |
|--------|----------|------------------|------------|
| world | PostgreSQL | OS X | world/postgres/world.sql |
| world | PostgreSQL | Windows | world/postgres/win-world.sql |
| world | SQL Server | Windows | world/dvdstore/world.sql |
| | | | |
| dvdstore | PostgreSQL | OS X | dvdstore/postgres/dvdstore.sql |
| dvdstore | PostgreSQL | Windows | dvdstore/sqlserver/dvdstore.sql |

To create and populate the desired PostgreSQL database, open your terminal or command prompt, and run the following commands.  

For example, here's how to create and populate the `world` database. To begin, create an empty `world` database.

```
cd world/postgres
createdb -U postgres world

```

Then populate the database. If running PostgreSQL on OS X,
```
psql -U postgres -d world -f world.sql
```

Otherwise, if running PostgreSQL on Windows,
```
psql -U postgres -d world -f win-world.sql
```

You can confirm the tables have been populated correctly by checking the number of rows in each table against the list below.

| world | |
|-------|----------:|
| city | 4079 |
| country | 239 |
| countryLanguage | 984 | 

<p />

| dvdstore | |
|-------|----------:|
| actor | 200 |
| address | 603 |
| category | 16 |
| city | 600 |
| country | 109 |
| customer | 599 |
| film | 1000 |
| film_actor | 5462 |
| inventory | 4581 |
| language | 6 |
| payment | 16049 |
| rental | 16044 |
| staff | 2 |
| store | 2 |




