package com.techelevator.fbn;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.fbn.pageobject.CheckingAppAddressInfoPage;
import com.techelevator.fbn.pageobject.HomePage;


public class CheckingApplicationIntegrationTest {
	
	private static WebDriver webDriver;
	private HomePage homePage;
	
	/* Creating an instance of the WebDriver is time consuming
	 * since it opens up a browser window. Therefore, we do this
	 * only once at the start of the class and reuse the same
	 * browser window for all tests. */
	@BeforeClass
	public static void openWebBrowserForTesting() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver = new ChromeDriver();
	}
	
	@Before
	public void openHomePage() {
		webDriver.get("http://localhost:8080/optional-java-selenium-lecture/");
		homePage = new HomePage(webDriver);
	}
	
	@AfterClass
	public static void closeWebBrowser() {
		webDriver.close();
	}
	
	@Test
	public void test() {
		CheckingAppAddressInfoPage addressInfo = homePage.clickCheckingAccountApplicationLink()
				.enterFirstName("John")
				.enterLastName("Doe")
				.enterDateOfBirth("01/01/1985")
				.enterStateOfBirth("Ohio")
				.enterEmailAddress("jdoe@mail.com")
				.enterPhoneNumber("123-456-7890")
				.clickNext();
		
		addressInfo.enterStreetAddress("123 Main St.")
				.enterApartmentNumber("28")
				.enterCity("Columbus")
				.enterState("Ohio")
				.enterZipcode("43123")
				.clickNex();
	}

}
