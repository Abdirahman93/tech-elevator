package com.techelevator.fbn.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CheckingAppAddressInfoPage {
	private WebDriver webDriver;
	public CheckingAppAddressInfoPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public CheckingAppAddressInfoPage enterStreetAddress(String address){
		WebElement element = webDriver.findElement(By.id("streetAddress"));
		element.sendKeys(address);
		return this;
	}
	
	public CheckingAppAddressInfoPage enterApartmentNumber(String apartmentNumber){
		WebElement element = webDriver.findElement(By.id("apartmentNumber"));
		element.sendKeys(apartmentNumber);
		return this;

	}
	
	public CheckingAppAddressInfoPage enterCity(String city){
		WebElement element = webDriver.findElement(By.id("city"));
		element.sendKeys(city);
		return this;

	}
	
	public CheckingAppAddressInfoPage enterState(String state){
		WebElement element = webDriver.findElement(By.id("state"));
		Select select = new Select(element);
		select.selectByVisibleText(state);
		return this;

	}
	
	public CheckingAppAddressInfoPage enterZipcode(String zipCode){
		WebElement element = webDriver.findElement(By.id("zipCode"));
		element.sendKeys(zipCode);
		return this;

	}
	
	public CheckingAppAddressInfoPage clickNex() {
		WebElement element = webDriver.findElement(By.className("formSubmitButton"));
		element.click();
		return this;

	}
}
