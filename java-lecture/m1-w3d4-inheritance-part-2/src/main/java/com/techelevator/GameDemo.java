package com.techelevator;

import com.techelevator.checkers.Checkers;
import com.techelevator.game.Game;
import com.techelevator.monopoly.Monopoly;

public class GameDemo {

	public static void main(String[] args) {
		Game theGame = new Checkers();
		theGame.playGame();
	}

}
