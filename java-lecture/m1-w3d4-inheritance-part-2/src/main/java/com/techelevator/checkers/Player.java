package com.techelevator.checkers;

public class Player {
	private String color;
	private int numOfPieces;
	
	
	public Player(String color){
		this.color = color;
		numOfPieces = 12;
	}
	
	public int getNumOfPieces(){
		return numOfPieces;
	}
	
	public void losePiece(){
		numOfPieces--;
	}
	
	public String getColor(){
		return color;
	}
}
