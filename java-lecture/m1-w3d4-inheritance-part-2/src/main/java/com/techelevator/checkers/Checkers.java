package com.techelevator.checkers;

import com.techelevator.game.Game;

public class Checkers extends Game{
	
	private Player[] players;
	
	public Checkers() {
		super(2);
	}

	@Override
	protected void setup(int numberOfPlayers) {	
		players = new Player[numberOfPlayers];
		players[0] = new Player("Red");
		System.out.println("Added Red Player");
		
		players[1] = new Player("Black");
		System.out.print("Added Black Player");
	}

	@Override
	protected void takeTurn(int player) {
		Player currentPlayer = players[player-1];
		System.out.println(currentPlayer.getColor() + " player's turn");
		if(Math.random()<0.5){
			currentPlayer.losePiece();
			System.out.println(currentPlayer.getColor() + " player lost a piece");
		}
		
	}

	@Override
	protected boolean isGameOver() {
		return players[0].getNumOfPieces() == 0 || players[1].getNumOfPieces() == 0;	
		}

	@Override
	protected void finishGame() {
		Player winner = players[0].getNumOfPieces() == 0 ? players[1] : players[0];
		
		System.out.println("----=====[ " + winner.getColor() + " wins!]=====----");
	}

}
