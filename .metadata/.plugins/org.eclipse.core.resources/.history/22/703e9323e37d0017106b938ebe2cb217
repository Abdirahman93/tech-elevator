package com.techelevator;


import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.User;

@Component
public class JdbcUserDao implements UserDao{
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcUserDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public User saveUser(User user) {
		
		String sqlCreateUser = "INSERT INTO appuser(email, first_name, last_name, password, role) VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlCreateUser, user.getEmail(), user.getFirstName(), user.getLastName(), user.getPassword(), user.getRole());
		return user;
		
	}
	
//	private long getNextUserId(){
//		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('appuser_user_id_seq')");
//		if(nextIdResult.next()) {
//			return nextIdResult.getLong(1);
//		}else{
//			throw new RuntimeException("Something went wrong");
//		}
//	}

	@Override
	public boolean searchForEmailAndPassword(String email, String password) {
		String sqlSearchForUser = "SELECT * "+
							      "FROM appuser "+
							      "WHERE email = ?"+
							      "AND password = ?";
		
		return jdbcTemplate.queryForRowSet(sqlSearchForUser, email, password).next();
	}

	@Override
	public void updatePassword(String email, String password) {
		String sqlUpdatePassword = "UPDATE appuser SET password = ? WHERE email = ?";
		jdbcTemplate.update(sqlUpdatePassword, email, password);
		
	}
	
	@Override
	public User getUser(String email) {
		User user = null;
		
		String sqlGetUser = "Select * FROM appuser WHERE email = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetUser, email);
		while(results.next()){
			 user = mapRowToUser(results);
			
		}
		return user;
	}
	
	@Override
	public List<User> getStudentsByTeacherId(long teacherId) {
		List<User> studentsByTeacherId = new ArrayList<User>();
		String sqlStudentsByTeacherId = "SELECT * "+
										"FROM appuser "+
										"JOIN teacher_student ON appuser.user_id = teacher_student.student_id "+
										"WHERE teacher_student.teacher_id =? ";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentsByTeacherId, teacherId);
		while (result.next()) {
			User view = mapRowToUser(result);
			studentsByTeacherId.add(view);
		}
		return studentsByTeacherId;
	}
	
	@Override
	public List<User> getStudentsByParentId(long parentId) {
		List<User> students = new ArrayList<User>();
		String sqlStudentsByParentId = "SELECT appuser.* "+
										"FROM appuser "+
										"JOIN parent_student ON appuser.user_id = parent_student.student_id "+
										"WHERE parent_student.parent_id =? ";
										
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentsByParentId, parentId);
		while (result.next()) {
			User student = mapRowToUser(result);
			students.add(student);
		}
		return students;
	}
	
	@Override
	public List<User> getAllUsers() {
		List<User> allUsers = new ArrayList<User>();
		String sqlGetAllUsers = "SELECT * "+
								"FROM appuser";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllUsers);
		while (results.next()) {
			User user = mapRowToUser(results);
			allUsers.add(user);
		}
		return allUsers;
	}
	
	public void bindStudentToTeacher(long teacherId, long studentId) {
		String sqlStudent2Teacher = "INSERT INTO teacher_student(teacher_id, student_id) "+
									"VALUES (?, ?)";
		jdbcTemplate.update(sqlStudent2Teacher, teacherId, studentId);
	}
	
	public void bindStudentToTeacherS(long teacherId, long studentId) {
		String sqlStudent2Teacher = "SELECT * FROM teacher_student "+
									"WHERE teacher_id = ? AND student_id = ?";
		jdbcTemplate.update(sqlStudent2Teacher, teacherId, studentId);
	}
	
	public void bindStudentToParents(long parentId, long studentId) {
		String sqlStudent2Parents = "INSERT INTO parent_student(parent_id, student_id) "+
									"VALUES (?, ?)";
		jdbcTemplate.update(sqlStudent2Parents, parentId, studentId);
	}
	
	public void bindStudentToParentS(long parentId, long studentId) {
		String sqlStudent2Parent = "SELECT * FROM parent_student "+
									"WHERE parent_id = ? AND student_id = ?";
		jdbcTemplate.update(sqlStudent2Parent, parentId, studentId);
	}
	
	
	
	
	
	private User mapRowToUser(SqlRowSet results) {
	    User theUser;
	    theUser = new User();
	    theUser.setFirstName(results.getString("first_name"));
	    theUser.setEmail(results.getString("email"));
	    theUser.setLastName(results.getString("last_name"));
	    theUser.setRole(results.getString("role"));
	    theUser.setId(results.getLong("user_id"));
	    theUser.setPassword(results.getString("password"));

	    return theUser;
	}



}
