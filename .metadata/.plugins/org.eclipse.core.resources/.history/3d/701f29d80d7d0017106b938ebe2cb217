package com.techelevator.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.Assessment;
import com.techelevator.AssessmentDao;
import com.techelevator.BehaviorReport;
import com.techelevator.BehaviorReportDao;
import com.techelevator.QAssessment;
import com.techelevator.QAssessmentDao;
import com.techelevator.Question;
import com.techelevator.QuestionDao;
import com.techelevator.QuizQuestionsDao;
import com.techelevator.Resource;
import com.techelevator.ResourceDao;
import com.techelevator.Response;
import com.techelevator.ResponseDao;
import com.techelevator.UserDao;
import com.techelevator.quizQuestions;
import com.techelevator.quizResponses;
import com.techelevator.quizResponsesDao;
import com.techelevator.User;

@Controller
@SessionAttributes({"currentUser", "questionList"})
public class WorkController {

	private UserDao userDao;
	private ResourceDao resourceDao;
	private QuestionDao questionDao;
	private AssessmentDao assessmentDao;
	private BehaviorReportDao behaviorDao;
	private ResponseDao responseDao;
	private QAssessmentDao qAssessmentDao;
	private QuizQuestionsDao quizQuestionsDao;
	private quizResponsesDao quizResponsesDao;
	
	
	@Autowired
	public WorkController(UserDao userDao, ResourceDao resourceDao, QuestionDao questionDao, AssessmentDao assessmentDao, ResponseDao responseDao,  QAssessmentDao qAssessmentDao, quizResponsesDao quizResponsesDao, QuizQuestionsDao quizQuestionsDao) {
		this.userDao = userDao;
		this.resourceDao = resourceDao;
		this.questionDao = questionDao;
		this.assessmentDao = assessmentDao;
		this.responseDao = responseDao;
		this.qAssessmentDao= qAssessmentDao;
		this.quizQuestionsDao = quizQuestionsDao;
		this.quizResponsesDao = quizResponsesDao;
	}
	

	@RequestMapping(path="/user/assignQuiz", method=RequestMethod.GET)
	public String assignQuiz(ModelMap model) {
		User user = ((User)model.get("currentUser"));
		if (((User)model.get("currentUser")).getRole().equals("teacher")) {
			model.addAttribute("questionList", new ArrayList<Question>());
			return "user/assignQuiz";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/assignHomework", method=RequestMethod.GET)
	public String assignHomework(ModelMap model) {
		User user = ((User)model.get("currentUser"));
		if (((User)model.get("currentUser")).getRole().equals("teacher")) {
			model.addAttribute("questionList", new ArrayList<Question>());
			return "user/assignHomework";
		} 
			return "redirect:/user/userDashboard";
	}
	

	
	@RequestMapping(path="/user/submitResources", method=RequestMethod.GET)
	public String submitResources(ModelMap model) {
		if (((User)model.get("currentUser")).getRole().equals("teacher")) {
			
			return "user/submitResources";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/{teacherId}/submitResources", method=RequestMethod.POST)
	public String createNewResource(@PathVariable int teacherId,
									@RequestParam String linkContent,
									@RequestParam String textContent,
									@RequestParam String fileContent,
									ModelMap model) {
		Resource resource = new Resource();
		resource.setTeacherId(teacherId);
		resource.setLinkContent(linkContent);
		resource.setTextContent(textContent);
		resource.setFileContent(fileContent);
		resourceDao.saveResource(resource);
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/viewResources")
	public String viewResources(HttpServletRequest request, ModelMap model) {
		User user = (User)model.get("currentUser");
		if (user.getRole().equals("teacher")) {
			long teachId = user.getId();
			List<Resource> teacherResource = resourceDao.getResourceByTeacherId(teachId);
			request.setAttribute("resources", teacherResource);
			return "user/viewResources";
		} else if (user.getRole().equals("student")) {
			long tempId = user.getId();
			List<Resource> studentResource = resourceDao.getResource(tempId);
			request.setAttribute("resources", studentResource);
			return "user/viewResources";
		} else {
			return "redirect:/user/userDashboard";
		}
	}


	
	@RequestMapping(path="/user/submitQuiz", method=RequestMethod.GET)
	public String submitQuizPage(HttpServletRequest request, ModelMap model) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("student")) {
				long tempId = user.getId();
				List<QAssessment> studentQuizes = qAssessmentDao.studentGetAllQuiz(tempId);
				request.setAttribute("quizes", studentQuizes);
				
				return "user/viewQuizPage";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/submitQuiz/{quizId}", method=RequestMethod.GET)
	public String submitQuizPage2(HttpServletRequest request, ModelMap model, @PathVariable int quizId) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("student")) {
				long tempId = user.getId();
				List<quizQuestions> studentQuestions = quizQuestionsDao.getQuizQuestions(quizId, tempId);
				request.setAttribute("questions", studentQuestions);
				
				request.setAttribute("quizId", quizId);
				return "user/submitQuiz";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/submitQuiz/{quizId}", method=RequestMethod.POST)
	public String submitQuiz(@PathVariable int quizId,
									@RequestParam int[] qNum,
									@RequestParam String[] response,
									ModelMap model) {
		User user = (User)model.get("currentUser");
		
		for(int i = 0; i < qNum.length; i++) {
			quizResponses res = new quizResponses();
			res.setId(quizId);
			res.setqNum(qNum[i]);
			res.setResponse(response[i]);
			res.setsId(user.getId());
			quizResponsesDao.submitQuizResponse(res);
		}
		return "redirect:/user/userDashboard";
	}

	@RequestMapping(path="/user/submitHomework", method=RequestMethod.GET)
	public String submitHomeworkPage(HttpServletRequest request, ModelMap model) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("student")) {
				long tempId = user.getId();
				List<Assessment> studentAssignments = assessmentDao.studentGetAllHomework(tempId);
				request.setAttribute("assignments", studentAssignments);
				
				return "user/viewAssignmentsPage";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/submitHomework/{assignmentId}", method=RequestMethod.GET)
	public String submitHomeworkPage2(HttpServletRequest request, ModelMap model, @PathVariable int assignmentId) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("student")) {
				long tempId = user.getId();
				List<Question> studentQuestions = questionDao.getHomeworkQuestions(assignmentId, tempId);
				request.setAttribute("questions", studentQuestions);
				
				request.setAttribute("assignmentId", assignmentId);
				return "user/submitHomework";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/submitHomework/{assignmentId}", method=RequestMethod.POST)
	public String submitHomework(@PathVariable int assignmentId,
									@RequestParam int[] qNum,
									@RequestParam String[] response,
									ModelMap model) {
		User user = (User)model.get("currentUser");
		
		for(int i = 0; i < qNum.length; i++) {
			Response res = new Response();
			res.setId(assignmentId);
			res.setqNum(qNum[i]);
			res.setResponse(response[i]);
			res.setsId(user.getId());
			responseDao.submitHomeworkResponse(res);
		}
		return "redirect:/user/userDashboard";
	}
	
	

	
	@RequestMapping(path="/user/gradeHomework", method=RequestMethod.GET)
	public String gradeHomeworkPage(HttpServletRequest request, ModelMap model) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("teacher")) {
				long tempId = user.getId();
				List<User> students = userDao.getStudentsByTeacherId(tempId);
				request.setAttribute("students", students);
				
				return "user/viewGradeHWStudents";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/getHomework/{id}", method=RequestMethod.GET)
	public String gradeHomeworkPage1(HttpServletRequest request, ModelMap model, @PathVariable int id) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("teacher")) {
				
				List<Assessment> students = assessmentDao.studentGetAllHomework(id);
				request.setAttribute("assignments", students);
				request.setAttribute("contextPath", request.getContextPath());
				return "user/viewGradeHW";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/gradeHomework/{assignmentId}/{sId}", method=RequestMethod.GET)
	public String gradeHomeworkPage2(HttpServletRequest request, ModelMap model, @PathVariable int assignmentId, @PathVariable int sId) {
		User user = (User)model.get("currentUser");

		if (user.getRole().equals("teacher")) {
				List<Response> studentResponses = responseDao.getHomeworkResponses(assignmentId, sId);
				request.setAttribute("responses", studentResponses);
				
				request.setAttribute("assignmentId", assignmentId);
				return "user/gradeHomework";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/gradeHomework/{assignmentId}/{sId}", method=RequestMethod.POST)
	public String submitHomeworkGrade(@PathVariable int assignmentId,
									@PathVariable int sId,
									@RequestParam int[] qNum,
									@RequestParam String[] response,
									@RequestParam String[] grade,
									ModelMap model) {
		
		for(int i = 0; i < qNum.length; i++) {
			Response res = new Response();
			res.setId(assignmentId);
			res.setqNum(qNum[i]);
			res.setResponse(response[i]);
			res.setsId(sId);
			res.setGrade(grade[i]);
			responseDao.submitHomeworkResponse(res);
		}
		return "redirect:/user/userDashboard";
	}
	

	
	@RequestMapping(path="/user/gradeQuiz")
	public String gradeQuiz(ModelMap model) {
		if (((User)model.get("currentUser")).getRole().equals("teacher")) {
			return "user/gradeQuiz";
		} 
			return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/submitBehaviorReport")
	public String submitBehaviorReport(@PathVariable int teacherId,
										@RequestParam int studentId,
										@RequestParam LocalDate issued,
										@RequestParam String reportText,
										@RequestParam boolean positive,
										ModelMap model) {
		BehaviorReport behavior = new BehaviorReport();
		behavior.setTeacherId(teacherId);
		behavior.setStudentId(studentId);
		behavior.setIssued(issued);
		behavior.setReportText(reportText);
		behavior.setPositive(positive);
		behaviorDao.saveBehaviorReport(behavior);
		
		if (((User)model.get("currentUser")).getRole().equals("teacher")) {
			return "user/submitBehaviorReport";
		} 
			return "redirect:/user/userDashboard";
	}
	
	
	
}
