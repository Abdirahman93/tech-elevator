package com.techelevator;


import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.User;

@Component
public class JdbcUserDao implements UserDao{
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcUserDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public User saveUser(User user) {
		
		String sqlCreateUser = "INSERT INTO appuser(email, first_name, last_name, password, role) VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlCreateUser, user.getEmail(), user.getFirstName(), user.getLastName(), user.getPassword(), user.getRole());
		return user;
		
	}
	
	public List<User> getAllStudents() {
		String sql = "SELECT * FROM app_user WHERE role = 'student'";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql);
		return mapResultsToUsers(results);
	}
	
//	private long getNextUserId(){
//		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('appuser_user_id_seq')");
//		if(nextIdResult.next()) {
//			return nextIdResult.getLong(1);
//		}else{
//			throw new RuntimeException("Something went wrong");
//		}
//	}

	@Override
	public boolean searchForEmailAndPassword(String email, String password) {
		String sqlSearchForUser = "SELECT * "+
							      "FROM appuser "+
							      "WHERE email = ?"+
							      "AND password = ?";
		
		return jdbcTemplate.queryForRowSet(sqlSearchForUser, email, password).next();
	}

	@Override
	public void updatePassword(String email, String password) {
		String sqlUpdatePassword = "UPDATE appuser SET password = ? WHERE email = ?";
		jdbcTemplate.update(sqlUpdatePassword, email, password);
		
	}
	
	@Override
	public User getUser(String email) {		
		String sqlGetUser = "Select * FROM appuser WHERE email = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetUser, email);
		if(results.next()){
			 return mapRowToUser(results);
			
		}
		return null;
	}
	
	@Override
	public List<User> getStudentsByTeacherId(long teacherId) {
		String sqlStudentsByTeacherId = "SELECT * "+
										"FROM appuser "+
										"JOIN teacher_student ON appuser.user_id = teacher_student.student_id "+
										"WHERE teacher_student.teacher_id =? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlStudentsByTeacherId, teacherId);
		return mapResultsToUsers(results);
	}
	
	@Override
	public List<User> getStudentsByParentId(long parentId) {
		String sqlStudentsByParentId = "SELECT appuser.* "+
										"FROM appuser "+
										"JOIN parent_student ON appuser.user_id = parent_student.student_id "+
										"WHERE parent_student.parent_id =? ";
										
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlStudentsByParentId, parentId);
		return mapResultsToUsers(results);
	}
	
	@Override
	public List<User> getAllUsers() {
		String sqlGetAllUsers = "SELECT * "+
								"FROM appuser";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllUsers);
		return mapResultsToUsers(results);
	}
	
	@Override
	public void bindStudentToTeacher(long teacherId, long studentId) {
		String sqlStudent2Teacher = "INSERT INTO teacher_student(teacher_id, student_id) "+
									"VALUES (?, ?)";
		jdbcTemplate.update(sqlStudent2Teacher, teacherId, studentId);
	}
	
	@Override
	public List<User> bindStudentToTeacherS(long studentId) {
		String sqlStudent2Teacher =  "SELECT * FROM appuser "
				+ "WHERE role = 'teacher' "
				+ "AND user_id NOT IN "
				+ "( SELECT teacher_id FROM teacher_student WHERE student_id = 11)";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlStudent2Teacher, studentId);
		return mapResultsToUsers(results);
	}
	
	@Override
	public void bindStudentToParents(long parentId, long studentId) {
		String sqlStudent2Parents = "INSERT INTO parent_student(parent_id, student_id) "+
									"VALUES (?, ?)";
		jdbcTemplate.update(sqlStudent2Parents, parentId, studentId);
	}
	
	@Override
	public List<User> bindStudentToParentS(long studentId) {
		String sqlStudent2Parent = "SELECT * FROM appuser "
								+ "WHERE role = 'parent' "
								+ "AND user_id NOT IN "
								+ "( SELECT parent_id FROM parent_student WHERE student_id = 11)";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlStudent2Parent, studentId);
		return mapResultsToUsers(results);
	}
	
	private List<User> mapResultsToUsers(SqlRowSet results) {
		List<User> users = new ArrayList<User>();
		
		while(results.next()) {
			users.add(mapRowToUser(results));
		}
		
		return users;
	}
	
	
	
	
	private User mapRowToUser(SqlRowSet results) {
	    User theUser;
	    theUser = new User();
	    theUser.setFirstName(results.getString("first_name"));
	    theUser.setEmail(results.getString("email"));
	    theUser.setLastName(results.getString("last_name"));
	    theUser.setRole(results.getString("role"));
	    theUser.setId(results.getLong("user_id"));
	    theUser.setPassword(results.getString("password"));

	    return theUser;
	}



}
