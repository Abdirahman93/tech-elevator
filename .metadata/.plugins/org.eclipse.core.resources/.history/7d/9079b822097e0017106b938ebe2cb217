package com.techelevator.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.UserDao;
import com.techelevator.Assessment;
import com.techelevator.AssessmentDao;
import com.techelevator.BehaviorReport;
import com.techelevator.BehaviorReportDao;
import com.techelevator.User;


@Controller
@SessionAttributes("currentUser")
public class UserController {
private UserDao userDao;
private AssessmentDao assessmentDao;
private BehaviorReportDao behaviorReportDao;

	
	@Autowired
	public UserController(UserDao userDao, BehaviorReportDao behaviorReportDao) {
		this.userDao = userDao;
		this.behaviorReportDao = behaviorReportDao;
	}
		
	@RequestMapping(path="/user/userDashboard", method=RequestMethod.GET)
	public String showUserDashboard(HttpServletRequest request, ModelMap model) {
		User user = (User)model.get("currentUser");
		if (user.getRole().equals("teacher")) {
			List<User> students = userDao.getStudentsByTeacherId(user.getId());
			request.setAttribute("students", students);
		} else if(user.getRole().equals("parent")) {
			List<User> students = userDao.getStudentsByParentId(user.getId());
			request.setAttribute("students", students);
		}
		return "user/userDashboard";
	}
	
	@RequestMapping(path="/user/createAccount")
	public String inputNewUser(ModelMap model) {
		if (((User)model.get("currentUser")).getRole().equals("teacher")) { 
		return "user/createAccount";
		}
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/createAccount", method=RequestMethod.POST)
    public String createNewUser(@RequestParam String firstName, 
    							@RequestParam String lastName, 
    							@RequestParam String role, 
    							@RequestParam String email, 
    							@RequestParam String password, 
    							ModelMap model) {
		
    	User user = new User();
    	user.setFirstName(firstName);
    	user.setLastName(lastName);
    	user.setRole(role);
    	user.setEmail(email);
    	user.setPassword(password);
    	userDao.saveUser(user);
    
    	return "redirect:/user/bindAccounts";
    }
	
	@RequestMapping(path="/user/viewStudentProgress", method=RequestMethod.GET)
	public String viewStudentProgress(@RequestParam (value="student_id", required=false) Long student_id,
			                          ModelMap model) {
		
		if (((User) model.get("currentUser")).getRole().equals("student")){
			student_id = ((User) model.get("currentUser")).getId();
		}
		List<BehaviorReport> behavior = new ArrayList<BehaviorReport>();
		behavior = behaviorReportDao.getBehaviorReport(student_id);
		model.addAttribute("behaviorReports", behavior);
		return "/user/viewStudentProgress";
	}
	
	@RequestMapping(path ="/user/bindAccounts", method = RequestMethod.GET)
	public String bindStudent(HttpServletRequest request, ModelMap model) {
		User user = (User) model.get("currentUser");

		if (user.getRole().equals("teacher")) {
			List<User> studentList = userDao.getAllStudents();
			request.setAttribute("studentList", studentList);
			return "user/listStudents";
		}
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path ="/user/bindAccounts", method = RequestMethod.GET)
	public String bindAccounts(HttpServletRequest request, ModelMap model) {
		User user = (User) model.get("currentUser");

		if (user.getRole().equals("teacher")) {
			List<User> teacherList = new ArrayList<User>();
			List<User> studentList = new ArrayList<User>();
			List<User> parentList = new ArrayList<User>();
			List<User> users = userDao.getAllUsers();
			
			for (User thisUser : users) {
				if (thisUser.getRole().equals("student")) {
					studentList.add(thisUser);
				} else if (thisUser.getRole().equals("teacher")) {
					teacherList.add(thisUser);
				} else if (thisUser.getRole().equals("parent")) {
					parentList.add(thisUser);
				}
			}
			request.setAttribute("studentList", studentList);
			request.setAttribute("teacherList", teacherList);
			request.setAttribute("parentList", parentList);
			return "user/bindAccounts";
		}
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/bindAccounts", method=RequestMethod.POST)
	public String bindUserAccounts(@RequestParam (value="teacherId",required=false) Long teacherId,
								   @RequestParam (value="studentId", required=false) Long studentId,
								   @RequestParam (value="parentId", required=false) Long parentId,
								   ModelMap model) {
		

		if (studentId != null) {
			if (teacherId != null) {
				userDao.bindStudentToTeacher(studentId, teacherId);
			}
			if (parentId != null) {
				userDao.bindStudentToParents(parentId, studentId);
			}
		}	
			return "redirect:/user/userDashboard";
		}
	
	

	
	
	
}
