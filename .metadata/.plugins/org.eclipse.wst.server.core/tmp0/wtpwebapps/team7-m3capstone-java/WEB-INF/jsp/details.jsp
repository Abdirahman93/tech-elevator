<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>



<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div class=main-section>

	<div class="park-list">


		<div id="detailImage">
			<c:set var="parkCode" value="${park.parkCode}" />
			<c:set var="parkCodeLower" value="${fn:toLowerCase(parkCode)}" />
			<img src="img/parks/${parkCodeLower}.jpg" />
		</div>

		<!-- file:///Users/amohamed/Development/workspaces/team7-m3capstone-java/src/main/webapp/img/parks/${parkCodeLower}.jpg" -->

		<h1>
			<c:out value="${park.parkName}" />
		</h1>
		<c:out value="${park.description}" />
		<br> <br>
		<c:out value="State: ${park.state}" />
		<br>
		<c:out value="Acreage: ${park.acreage}" />
		<br>
		<c:out value="Elevation: ${park.elevation}" />
		<br>
		<c:out value="Miles of trail: ${park.milesOfTrail}" />
		<br>
		<c:out value="Number of campsites: ${park.numOfCampSites}" />
		<br>
		<c:out value="Climate type: ${park.climate}" />
		<br>
		<c:out value="Year founded: ${park.yearFounded}" />
		<br>
		<c:out value="Annual visitors: ${park.visitors}" />
		<br>
		<c:out value="Entry fee: ${park.entryFee}" />
		<br>
		<c:out value="Number of species: ${park.numOfSpecies}" />
		<br> <br>
		<c:out value="&quot;${park.quote}&quot;" />
		<br>
		<c:out value="- ${park.quoteSource}" />
		<br>

		<p>Choose Weather in:</p>
		<c:url var="formAction" value="/details"/>
		<form action="${formAction}" method="POST">
			<input type="radio" name="tempFormat" value="Celsius">Celsius
			<input type="radio" name="tempFormat" value="Fahrenheit">Fahrenheit<br> <br>
			<input type="hidden" name="parkCode" value="${park.parkCode}">
			<input type="submit" value="Submit">
		</form>
	</div>
	<div class="weather-info">
		<c:forEach var="weather" items="${weather}">
			<c:choose>
				<c:when test="${weather.fiveDayValue == 1}">
				<div id="f">
					<div id="weatherImage">
						<h4>Today</h4>
						<img src="img/weather/${weather.forecast}.png" />

					</div>
					
					<c:choose>
						<c:when test="${isFahrenheit}">

						<c:out value="High: ${weather.highTemp } F" />
						<c:out value="Low: ${weather.lowTemp} F" />
						</c:when>
						<c:otherwise>
						<c:out value="High: ${Math.round((weather.highTemp - 32) * 5/9)} C" />
						<c:out value="Low: ${Math.round((weather.lowTemp - 32) * 5/9)} C" />
						</c:otherwise>
					</c:choose>
					<p>
					<c:choose>
					<c:when test = "${weather.forecast eq 'sunny' }"> Pack sunscreen. <br> </c:when>
					<c:when test = "${weather.forecast eq 'snow' }"> Pack snowshoes. <br> </c:when>
					<c:when test = "${weather.forecast eq 'rain' }"> Pack rain gear and wear waterproof shoes. <br> </c:when>
					<c:when test = "${weather.forecast eq 'thunderstorm' }"> seek shelter and avoid hiking on exposed ridges.<br> </c:when>
					</c:choose>
					<c:if test = "${weather.highTemp > 75 }"> Bring an extra gallon of water<br></c:if>
					<c:if test = "${weather.lowTemp < 20 }"> Exposure to frigid temperatures<br></c:if>
					<c:if test = "${weather.highTemp - weather.lowTemp > 20 }"> Wear breatheable layers<br></c:if>
					</p>
					<br>
					</div>

				</c:when>
				<c:when test="${weather.fiveDayValue > 1}">
					<div id="weatherImage1">
						<img src="img/weather/${weather.forecast}.png" />
					</div>
					<c:choose>
						<c:when test="${isFahrenheit}">
						<c:out value="High: ${weather.highTemp} F" /><br>
						<c:out value="Low: ${weather.lowTemp} F" /><br>
						</c:when>
						<c:otherwise>
						<c:out value="High: ${Math.round((weather.highTemp - 32) * 5/9)} C" />
						<c:out value="Low: ${Math.round((weather.lowTemp - 32) * 5/9)} C" />
						</c:otherwise>
					</c:choose>
				</c:when>
			</c:choose>
		</c:forEach>
		
	</div>

</div>

</body>
</html>