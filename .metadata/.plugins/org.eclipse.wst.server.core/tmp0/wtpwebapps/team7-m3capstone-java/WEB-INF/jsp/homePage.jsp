<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div class=main-section>

	<c:forEach var="park" items="${parkList}">
		<br>
		<br>
		<div class="park-list">
			<div id="detailImage">
			
			<c:set var="parkCode" value="${park.parkCode}"/>
			<c:set var="parkCodeLower" value="${fn:toLowerCase(parkCode)}"/>
			<a href="details?parkCode=${park.parkCode}">
			<img src="img/parks/${parkCodeLower}.jpg" >
			<!--file:///Users/amohamed/Development/workspaces/team7-m3capstone-java/src/main/webapp/img/parks/${parkCodeLower}.jpg  -->
			</a>
		</div>
		<div id="lol">
		<h3><c:out value="${park.parkName}" /></h3>
		<br>
		<c:out value="${park.state}" />
		<br>
		<br>
		<c:out value="${park.description}" />
		<br>
		</div>
</div>
</c:forEach>
</div>