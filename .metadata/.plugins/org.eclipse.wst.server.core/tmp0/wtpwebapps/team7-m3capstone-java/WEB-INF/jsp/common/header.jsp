<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>National Park Geek</title>
    <c:url value="/css/nationalgeek.css" var="cssHref" />
    <link rel="stylesheet" href="${cssHref}">
</head>

<body>
	<div id="banner">
    <header>
    		<c:url value="/" var="homePage" />
    		<c:url value="/img/logo.png" var="logoSrc" />
        <a href="${homePageHref}">
        		<img id="toplogo" src="${logoSrc}" alt="National Park Geek logo" />
        </a>
    </header>
    <nav>
        
        <ul>

            <li><a href="${homePage}">Home</a></li>
            <c:url value="/surveyInput" var="surveyInputURL"/>
            <li><a href="${surveyInputURL}">Survey</a></li>
                          
        </ul>
    </nav>
    </div>