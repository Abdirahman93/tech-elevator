

function passwordGenerator() {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < 10; i++)
	    text += possible.charAt(Math.floor(Math.random() * possible.length));

	  return text;
	}

$(document).ready(function() {
	$('#generate_password').on('click', function(event) {
		var password = passwordGenerator();
		$('#password').val(password);
		return false;
	});

});