<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<c:choose><c:when test="${currentUser.role == 'teacher'}">
<div id="Account">
<h2>Create a new Account</h2>
<c:url var="formAction" value="/user/createAccount" />
<form id="form0" method="POST" action="${formAction}">
	<div>
		<input type="text" name="firstName" id="firstName" value="" placeholder="First Name" /><br/>
	</div>
	<div>
		<input type="text" name="lastName" id="lastName" value="" placeholder="Last Name" /><br/>
	</div>
	<div>
		<input type="text" name="email" id="email" value="" placeholder="student@email.com" /><br/>
	</div>
	<div>
		<input type="text" name="password" id="password" placeholder="password" />
	</div>
	<br>
	<button id="generate_password">Generate Password</button><br><br>
	
	 <input type="radio" name="role" value ="teacher">Teacher<br> 
	<input type="radio" name="role" value = "student">Student<br> 
	<input type="radio" name="role" value = "parent">Parent<br>
	<br>
	<button>Create Account</button>
</form>
</div>
</c:when></c:choose>
<div class="ocean">
  <div class="wave"></div>
  <div class="wave"></div>
</div>
<c:url var="passwordGenerator" value="/js/passwordGenerator.js"/>
<script src="${passwordGenerator}"></script>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />
