<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<c:choose><c:when test="${currentUser.role == 'teacher'}">

<div id="Account">
<h2>User Relations</h2><br>
<c:url var="formAction" value="/user/bindAccounts/${studentId}" />
<form id="form0" method="POST" action="${formAction}">
	<h4>Student</h4>

		<input type="hidden" name="studentId" value="${student.id}"/>
		<h2>${student.firstName} ${student.lastName}</h2><br>
	<br>
	<h4>Teacher</h4>
	<c:forEach var="teacher" items="${teacherList}">
		<input type=radio name="teacherId" value="${teacher.id}"/>${teacher.firstName} ${teacher.lastName}<br>
	</c:forEach><br>
	<h4>Parent</h4>
	<c:forEach var="parent" items="${parentList}">
		<input type=radio name="parentId" value="${parent.id}"/>${parent.firstName} ${parent.lastName}<br>
	</c:forEach><br>
	
	<button>Bind User Accounts</button>
		</form>
	</div>
</c:when></c:choose>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />