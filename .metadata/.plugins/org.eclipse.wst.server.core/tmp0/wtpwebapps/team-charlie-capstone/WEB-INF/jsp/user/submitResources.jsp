<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<c:choose><c:when test="${currentUser.role == 'teacher'}">
<h2>Submit Resources</h2>

<c:url var="formAction" value="/user/${currentUser.id}/submitResources" />

<div class="Account">
	<div class="col-md-5">
		<form action="${formAction}" method="POST">
			
			<div class="form-group">
				<label for="LinkResource">Link </label>
				<input type="text" id="link" name="linkContent" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="resourceText">Text Materials </label>
				<textarea id="resourceText" name="textContent" class="form-control" rows="3" cols="50" placeholder="Your message goes here (140 character max)" maxlength="1000"></textarea>	
			</div>
			<div class="form-group">
				<label for="file">File </label>
				<input type="file" name="fileContent" id="file">	
			</div>
			<button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
	</div>

	<div class="col-md-9"></div>

</c:when></c:choose>


<c:import url="/WEB-INF/jsp/common/footer.jsp" />