<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div id="Account">
	<h2>Student Progress</h2>
	<br>

	<h4>Homework</h4>
	<c:forEach var="homeworks" items="${homework}">
		<div>
			<c:set var="qCount" value="0" />
			<c:set var="cCount" value="0" />
			<strong>Homework Assignment <c:out
					value="${homeworks.assignmentId}" /></strong><br>
			<br>
			<c:forEach var="questions" items="${homeworkQuestions}">
				<c:if test="${questions.id == homeworks.assignmentId }">
					<c:out value="${questions.text1}" />
					<br>
					<c:out value="${questions.text2}" />
					<br>
					<c:set var="qCount" value="${qCount + 1}" />
				</c:if>
				<c:forEach var="responses" items="${homeworkResponses}">
					<c:if test="${responses.qNum == questions.questionNumber}">
						<c:choose>
							<c:when test="${not empty responses.response }">
						Student Response: <c:out value="${responses.response}"></c:out>
								<c:choose>
									<c:when test="${empty responses.grade}">
										<strong>-*UNGRADED*-</strong>
									</c:when>
									<c:when test="${responses.grade == 'correct'}">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										<c:set var="cCount" value="${cCount + 1}" />
									</c:when>
									<c:when test="${responses.grade != 'correct'}">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</c:when>
								</c:choose>
								<br>
								<br>
							</c:when>
							<c:when test="${empty responses.response}">
								<strong>NOT ANSWERED</strong>
								<br>
								<br>
							</c:when>
						</c:choose>
					</c:if>
				</c:forEach>
			</c:forEach>
			<br> <strong>Score for this assignment:</strong>
			${cCount}/${qCount} --
			<fmt:formatNumber type="number" minFractionDigits="2"
				maxFractionDigits="2" value="${(cCount/qCount) * 100}" />
			%
		</div>
		<hr>
	</c:forEach>

	<h4>Quizzes</h4>
	<c:forEach var="quiz" items="${quiz}">
		<div>
			<c:set var="qCount" value="0" />
			<c:set var="cCount" value="0" />
			<strong>Quiz Assignment <c:out value="${quiz.quizId}" /></strong><br>
			<br>
			<c:forEach var="qQuestions" items="${quizQuestions}">
				<c:if test="${qQuestions.id == quiz.quizId }">
					<c:out value="${qQuestions.text1}" />
					<br>
					<c:out value="${qQuestions.text2}" />
					<br>
					<c:set var="qCount" value="${qCount + 1}" />
				</c:if>
				<c:forEach var="qResponses" items="${quizResponses}">
					<c:if test="${qResponses.qNum == qQuestions.questionNumber}">
						<c:choose>
							<c:when test="${not empty qResponses.response }">
						Student Response: <c:out value="${qResponses.response}"></c:out>
								<c:choose>
									<c:when test="${empty qResponses.grade}">
										<strong>-*UNGRADED*-</strong>
									</c:when>
									<c:when test="${qResponses.grade == 'correct'}">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										<c:set var="cCount" value="${cCount + 1}" />
									</c:when>
									<c:when test="${qResponses.grade != 'correct'}">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</c:when>
								</c:choose>
								<br>
								<br>
							</c:when>
							<c:when test="${empty qResponses.response}">
								<strong>NOT ANSWERED</strong>
								<br>
								<br>
							</c:when>
						</c:choose>
					</c:if>
				</c:forEach>
			</c:forEach>
			<br> <strong>Score for this assignment:</strong>
			${cCount}/${qCount} --
			<fmt:formatNumber type="number" minFractionDigits="2"
				maxFractionDigits="2" value="${(cCount/qCount) * 100}" />
			%
		</div>
		<hr>
	</c:forEach>

	<h4>Behavior Reports</h4>
	<c:forEach var="behaviorReport" items="${behaviorReports}">
		<div>
			<p>
				<c:out value="${behaviorReport.issued}" />
			</p>
			<p>
				<c:out value="${behaviorReport.reportText}" />
			</p>
			<c:if test="${behaviorReport.positive == 'true'}">
				<span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span>
			</c:if>
			<c:if test="${behaviorReport.positive == 'false'}">
				<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
			</c:if>
		</div>
		<hr>
	</c:forEach>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />