<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<c:choose>
	<c:when test="${currentUser.role == 'teacher'}">

		
		<div class="AccountOne">
			<c:if test="${currentUser.role == 'teacher'}">


				<h2>Submit Behavior Report</h2>

				<c:url var="formAction1" value="/user/submitBehaviorReport" />

				<div class="row">
					<div class="col-md-5">
						<form action="${formAction1}" method="POST">
							<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
							<div class="form-group">
								<div class="form-group">
									<select name="studentId">
										<!--  For Each Student -->
										<c:forEach var="student" items="${students}">
											<option value="${student.id}">${student.firstName}
												${student.lastName}</option>
										</c:forEach>
									</select>
									<!-- <label for="student_id">Student ID</label>
								<input type="text" id="student_id" name="studentId" class="form-control" /> -->
								</div>
								<div class="form-group">
									<label for="issued">Issued Date </label> <input type="text"
										id="issued" name="issued" class="form-control" />
								</div>
								<div class="form-group">
									<label for="report_text">Report Text </label> <input
										type="text" id="report_text" name="reportText"
										class="form-control" />
								</div>
								<div class="form-group">
										<input type=radio name="positive" value="false"/>Positive <br>
										<input type=radio name="positive" value="true"/>Negative
								</div>



								<button type="submit" class="btn btn-default">Submit</button>

							</div>

						</form>
					</div>
				</div>

			</c:if>
		</div>
	</c:when>
</c:choose>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />