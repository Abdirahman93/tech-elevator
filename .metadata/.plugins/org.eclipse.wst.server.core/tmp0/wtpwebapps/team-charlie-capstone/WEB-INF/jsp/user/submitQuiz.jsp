<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div class="Account">
<c:url var="formAction0" value="${contextPath}/user/submitQuiz/${quizId}" />
	<form action="${formAction0}" method="POST">
<c:choose>
	<c:when test="${currentUser.role == 'student'}">
		<h2>Submit Quiz</h2>
		<c:forEach var="question" items="${questions}">

			<input type="hidden" name="qNum" value="${question.questionNumber}"/>
			<c:if test="${question.questionType == 'tf'}">
				
				<c:out value="${question.text1}" />
				<br>
				<c:out value="${question.text2 }" />
				<br>
				<div class="form-group">
		
					<label for="LinkResource">Answer </label> <br>
					<input type=radio name="response" value="true"/>True <br>
					<input type=radio name="response" value="false"/>False
				</div>
			</c:if>

			<c:if test="${question.questionType == 'mcma'}">

				<input type="hidden" name="response" id="chkresponse" value="" />
				<c:out value="${question.text1}" />
				<br>
				<c:out value="${question.text2}" />
				<br>
				<div class="form-group">
					<label for="LinkResource">Answer </label> <br>
					<input type=checkbox id="mcma" name="mcma" value="a"/> A<br>
					<input type=checkbox id="mcma" name="mcma" value="b"/> B<br>
					<input type=checkbox id="mcma" name="mcma" value="c"/> C<br> 
					<input type=checkbox id="mcma" name="mcma" value="d"/> D<br>
				</div>
			</c:if>

			<c:if test="${question.questionType == 'mcoa'}">

				<c:out value="${question.text1}" />
				<br>
				<c:out value="${question.text2}" />
				<br>
				<div class="form-group">
					<label for="LinkResource">Answer </label> <br>
					<input type=radio name="response" value="a"/> A<br>
					<input type=radio name="response" value="b"/> B<br> 
					<input type=radio name="response" value="c"/> C<br> 
					<input type=radio name="response" value="d"/> D 
				</div>
			</c:if>


			<c:if test="${question.questionType == 'essay'}">

				<c:out value="${question.text1}" />
				<br>

				<div class="form-group">
					<label for="answerText">Answer </label>
					<textarea id="answerText" name="response" class="form-control"
						rows="3" cols="50"
						placeholder="Type your answer here"
						maxlength="1000"></textarea>
				</div>
			</c:if>
		</c:forEach>
	</c:when>
</c:choose>

		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />