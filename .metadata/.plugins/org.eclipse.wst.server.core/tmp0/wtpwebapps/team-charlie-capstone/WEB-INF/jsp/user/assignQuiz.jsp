<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />



	<c:if test="${currentUser.role == 'teacher'}">
	<div class="Account">
		<h2>True or False</h2>
		<c:url var="formActionTf"
			value="${contextPath}/user/assignQuiz/TrueFalse" />
		<div class="row">
			<div class="col-md-5">
				<form action="${formActionTf}" method="POST">
					<div class="form-group">
						<label for="resourceText">Question </label>
						<textarea id="resourceText" name="text1" class="form-control"
							rows="3" cols="50"
							placeholder="Type your question here "
							maxlength="1000"></textarea>
					</div>
					Quiz Assignment Id <br> <input type="text" name="text3"
						value="${assignId}"><br>
					<button type="submit" class="btn btn-default">Add To Quiz</button>
				</form>
			</div>
		</div>
		<h2>Multiple Choice</h2>
		<c:url var="formActionMcoa"
			value="${contextPath}/user/assignQuiz/MCOA" />
		<div class="row">
			<div class="col-md-5">
				<form action="${formActionMcoa}" method="POST">
					<div class="form-group">
						<label for="resourceText">Question </label>
						<textarea id="resourceText" name="text1" class="form-control"
							rows="3" cols="50"
							placeholder="Type your question here"
							maxlength="1000"></textarea>
					</div>
					<div class="form-group">
						<label for="resourceText">Answer Options</label>
						<textarea id="resourceText" name="text2" class="form-control"
							rows="3" cols="50"
							placeholder="Type your answer options here"
							maxlength="1000"></textarea>
					</div>
					<div class="form-group">
						Quiz Assignment Id <br> <input type="text" name="text3"
							value="${assignId}"><br>
						<button type="submit" class="btn btn-default">Add To Quiz</button>
					</div>
				</form>
			</div>
		</div>
		<h2>Multiple Choice, Multiple Answers</h2>
		<c:url var="formActionMcma"
			value="${contextPath}/user/assignQuiz/MCMA" />
		<div class="row">
			<div class="col-md-5">
				<form action="${formActionMcma}" method="POST">
					<div class="form-group">
						<label for="resourceText">Question </label>
						<textarea id="resourceText" name="text1" class="form-control"
							rows="3" cols="50"
							placeholder="Type your question here"
							maxlength="1000"></textarea>
					</div>
					<div class="form-group">
						<label for="resourceText">Answer Options </label>
						<textarea id="resourceText" name="text2" class="form-control"
							rows="3" cols="50"
							placeholder="Type your answer options here"
							maxlength="1000"></textarea>
					</div>
					<div class="form-group">
						Quiz Assignment Id <br> <input type="text" name="text3"
							value="${assignId}"><br>
						<button type="submit" class="btn btn-default">Add To Quiz</button>
					</div>
				</form>
			</div>
		</div>
		<h2>Essay</h2>
		<c:url var="formActionEssay"
			value="${contextPath}/user/assignQuiz/Essay" />
		<div class="row">
			<div class="col-md-5">
				<form action="${formActionEssay}" method="POST">
					<div class="form-group">
						<label for="resourceText">Question</label>
						<textarea id="resourceText" name="text1" class="form-control"
							rows="3" cols="50"
							placeholder="Type your question here"
							maxlength="1000"></textarea>
					</div>
					<div class="form-group">
						Quiz Assignment Id <br> <input type="text" name="text3"
							value="${assignId}"><br>
						<button type="submit" class="btn btn-default">Add To Quiz</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</c:if>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />