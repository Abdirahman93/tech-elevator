<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div id="user-dashboard">
<h1>
<c:set var="Account" value = "${currentUser.role}"/>
<c:out value ="${fn:toUpperCase(dashBoard) }" />
</h1>

<h1>Welcome back, ${currentUser.firstName} ${currentUser.lastName}</h1>
<c:choose>
	<c:when test="${currentUser.role == 'teacher'}">
		<h1>You are logged in as a teacher</h1>
		<ul>
			<c:forEach var="student" items="${students}">
				<li><a href="viewStudentProgress?student_id=${student.id}">${student.firstName} ${student.lastName}</a></li>
			</c:forEach>
		</ul>
	</c:when>
	
	<c:when test="${currentUser.role == 'parent'}">
		<h1>You are a logged in as a parent</h1>
		<h3>Students</h3>
		<ul>
			<c:forEach var="student" items="${students}">
				<li><a href="viewStudentProgress?student_id=${student.id}">${student.firstName} ${student.lastName}</a></li>
			</c:forEach>
		</ul>
	</c:when>
	
	<c:when test="${currentUser.role == 'student'}">
		<h1>You are a logged in as a student</h1>
	</c:when>

</c:choose>


</div>
<div class="ocean">
  <div class="wave"></div>
  <div class="wave"></div>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />