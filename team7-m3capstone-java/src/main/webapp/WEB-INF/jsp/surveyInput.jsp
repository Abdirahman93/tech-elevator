<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div class=main-section>
<h1>Pick your favorite National Park!</h1>
<p>Love visiting our National Parks? Take our survey to select your favorite park. We value your feedback.</p>
<br>

<c:url var="formAction" value="/surveyInput"/>
	<form:form method="POST" action="${formAction}" modelAttribute="survey">
	<!-- modelAttribute="survey" -->
		  
		<label for="parkCode">Choose your favorite National Park</label>
		<form:select path="parkCode">
    		<c:forEach var="park" items="${parkList}">
    		<option value="${park.parkCode}"> ${park.parkName}</option>
    		</c:forEach>
  		</form:select><br>
  		<form:errors path="parkCode" cssClass="error"/>
  		
  		
  		<br>
  		
  		<label for="email">Your email: </label>
		<form:input type="text" path="email" id="emailAddress" />
		<br>
		<form:errors path="email" cssClass="error"/>
		<br>
		
		<label for="state">State of residence:</label> 
		<form:select path="state" id="stateOfBirth">
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="DC">District Of Columbia</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Louisiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersey</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>
			</form:select>
			<br>
			<form:errors path="state" cssClass="error"/>
			<br>
			
		<label for="activityLevel">Activity level:</label> 
		<form:radiobutton path="activityLevel" value="Inactive"/>Inactive
		<form:radiobutton path="activityLevel" value="Sedentary"/>Sedentary
		<form:radiobutton path="activityLevel" value="Active"/>Active
		<form:radiobutton path="activityLevel" value="Extremely"/>Extremely active<br>
		<form:errors path="activityLevel" cssClass="error"/><br>
		<input type="submit" value="Submit">

	</form:form>
</div>
</body>
</html>