<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div class=main-section>
<h1>Survey results!</h1>
<p>Thank you for taking our survey. Here are the most popular National Parks according to our survey results! Click on the park's image to find out more!</p>

	<div class="surveyResults">
	<c:forEach var="park" items="${list}">
	
			<h3><c:out value="${park.parkName}"></c:out></h3>
		
			<c:set var="parkCode" value="${park.parkCode}" />
			<c:set var="parkCodeLower" value="${fn:toLowerCase(parkCode)}" />
			<a id="surveyImage" href="details?parkCode=${park.parkCode}">
			<img src="img/parks/${parkCodeLower}.jpg" />
			</a>	

</c:forEach>
</div>

</div>
</body>
</html>