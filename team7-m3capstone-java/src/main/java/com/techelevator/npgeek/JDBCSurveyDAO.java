package com.techelevator.npgeek;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;



@Component
public class JDBCSurveyDAO implements SurveyDAO {

private JdbcTemplate jdbcTemplate;


@Autowired
public JDBCSurveyDAO(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
}



@Override
public Survey saveSurvey(Survey survey) {
	String sqlCreateSurveyByCode = "INSERT INTO survey_result (parkcode, emailaddress, state, activitylevel) VALUES (? ,?, ?, ?)";
	jdbcTemplate.update(sqlCreateSurveyByCode, survey.getParkCode(), survey.getEmail(), survey.getState(), survey.getActivityLevel());
	return survey;
}

private long getNextSurveyId(){
	SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('survey_id_seq')");
	if(nextIdResult.next()) {
		return nextIdResult.getLong(1);
	}else{
		throw new RuntimeException("Something went wrong");
	}
}




private Survey mapRowToSurvey(SqlRowSet results) {
    Survey theSurvey;
    theSurvey = new Survey();
    theSurvey.setParkCode(results.getString("parkcode"));
    theSurvey.setEmail(results.getString("emailaddress"));
    theSurvey.setState(results.getString("state"));
    theSurvey.setActivityLevel(results.getString("activitylevel"));
    theSurvey.setSurveyId(results.getInt("surveyid"));
    
    return theSurvey;
}
}