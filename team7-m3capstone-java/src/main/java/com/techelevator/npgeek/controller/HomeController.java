package com.techelevator.npgeek.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.npgeek.Park;
import com.techelevator.npgeek.ParkDAO;
import com.techelevator.npgeek.Survey;
import com.techelevator.npgeek.SurveyDAO;
import com.techelevator.npgeek.Weather;
import com.techelevator.npgeek.WeatherDAO;


@Controller
@SessionAttributes({"surveyResults", "isFahrenheit", "list"})
public class HomeController {
	@Autowired
	private ParkDAO parkDao;
	@Autowired
	private WeatherDAO weatherDao;
	@Autowired
	private SurveyDAO surveyDAO;
	
	@RequestMapping(path="/", method=RequestMethod.GET)
	public String displayHomePage(HttpServletRequest request) {
		request.setAttribute("parkList", parkDao.getAllParkInfo());

		return "homePage";
	}
	
	
	@RequestMapping(path={"/details"}, method=RequestMethod.GET)
	public String showWeatherAndParkDetails(HttpServletRequest request, @ModelAttribute("parkCode") String parkCodeAttr, ModelMap model) {
		String parkCode = request.getParameter("parkCode");
		if(parkCode == null) {
			parkCode = parkCodeAttr;
		}
		
		if(!model.containsAttribute("isFahrenheit")) {
			model.addAttribute("isFahrenheit", true);
		}

		List<Weather> weather = weatherDao.getWeatherByCode2(parkCode);
		Park park = parkDao.getParkByParkCode(parkCode);
		request.setAttribute("park", park);
		request.setAttribute("weather", weather);		
		return "details";
	}
	
	@RequestMapping(path="/details", method=RequestMethod.POST)
	public String setWeatherFormat(@RequestParam String tempFormat, @RequestParam String parkCode, ModelMap model, RedirectAttributes redirectAttrs) {
		if(tempFormat.equals("Celsius")) {
			model.addAttribute("isFahrenheit", false);
		} else {
			model.addAttribute("isFahrenheit", true);
		}
		
		redirectAttrs.addFlashAttribute("parkCode", parkCode);
		
		return "redirect:/details";
	}
	
	@RequestMapping(path="/surveyInput", method=RequestMethod.GET)
	public String displaySurveyInput(HttpServletRequest request, Model modelHolder) {
		if(!modelHolder.containsAttribute("survey")) {
			modelHolder.addAttribute("survey", new Survey());
		}

		request.setAttribute("parkList", parkDao.getAllParkInfo());
		return "surveyInput";
	}
	
	@RequestMapping(path="/surveyInput", method=RequestMethod.POST)
	public String processSurvey(@Valid @ModelAttribute("survey") Survey survey, BindingResult result, RedirectAttributes attr){
		attr.addFlashAttribute("survey", survey);
		
		if(result.hasErrors()) {
			attr.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "survey", result);
			return "redirect:/surveyInput";
		}
		
		surveyDAO.saveSurvey(survey); 
		return "redirect:/surveyOutput";
		
	}
	
	@RequestMapping(path="/surveyOutput", method=RequestMethod.GET)
	public String displaySurveyOutput(HttpServletRequest request) {
		
		List<Park> parks = parkDao.getTopParks();
		request.setAttribute("list", parks);
		return "surveyOutput";
	}

//	private List<Park> getParks(String parkCode) {
//		List<Park> parks = parkDao.getAllParksByCode(parkCode);
//		return parks;
//		}
//
//		private List<Weather> getWeather() {
//		    List<Weather> weather = weatherDao.getAllWeatherInfo();
//		    return weather;
//		}
	
}
