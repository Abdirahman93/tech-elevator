package com.techelevator.npgeek;

import java.util.List;

public interface ParkDAO {

	public List<Park> getAllParkInfo();
	public Park getParkByParkCode(String parkCode);
	public List<Park> getAllParksByCode();
	public List<Park> getTopParks();
	
}
