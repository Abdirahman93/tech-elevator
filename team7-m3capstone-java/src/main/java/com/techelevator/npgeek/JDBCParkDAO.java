package com.techelevator.npgeek;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCParkDAO implements ParkDAO {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JDBCParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> getAllParkInfo() {
		Park thePark = null;
		List<Park> park = new ArrayList<Park>();
		String sqlGetAllActivePark = "SELECT * " + "FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActivePark);
		while (results.next()) {
			thePark = mapRowToPark(results);
			park.add(thePark);

		}

		return park;
	}

	@Override
	public Park getParkByParkCode(String parkCode) {
		Park park = null;
		String sqlSelectParkByCode = "SELECT *  FROM park WHERE parkcode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectParkByCode, parkCode);
		if (results.next()) {
			return mapRowToPark(results);
		} else {
			return null;
		}
	}

	@Override
	public List<Park> getAllParksByCode() {
		Park thePark = null;
		List<Park> park = new ArrayList<Park>();
		String sqlSelectParkByCode = "SELECT parkname  FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectParkByCode);
		while (results.next()) {
			thePark = mapRowToPark(results);
			park.add(thePark);
		}
		return park;
	}

	@Override
	public List<Park> getTopParks() {
		Park thePark = null;
		List<Park> park = new ArrayList<Park>();
		String sqlGetAllActivePark = "SELECT COUNT(survey_result.parkcode), park.parkcode, parkname, park.state, acreage, elevationinfeet, milesoftrail, numberofcampsites, climate, yearfounded,"
				+ " annualvisitorcount, inspirationalquote, inspirationalquotesource, parkdescription, entryfee, numberofanimalspecies "
				+ "FROM park JOIN survey_result ON park.parkcode = survey_result.parkcode GROUP BY park.parkcode ORDER BY COUNT(*) DESC LIMIT 5";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActivePark);
		while (results.next()) {
			thePark = mapRowToPark(results);
			park.add(thePark);

		}

		return park;

	}

	private Park mapRowToPark(SqlRowSet results) {
		Park thePark;
		thePark = new Park();
		thePark.setParkCode(results.getString("parkcode"));
		thePark.setParkName(results.getString("parkname"));
		thePark.setState(results.getString("state"));
		thePark.setAcreage(results.getInt("acreage"));
		thePark.setElevation(results.getInt("elevationinfeet"));
		thePark.setMilesOfTrail(results.getDouble("milesoftrail"));
		thePark.setNumOfCampSites(results.getInt("numberofcampsites"));
		thePark.setClimate(results.getString("climate"));
		thePark.setYearFounded(results.getInt("yearfounded"));
		thePark.setVisitors(results.getInt("annualvisitorcount"));
		thePark.setQuote(results.getString("inspirationalquote"));
		thePark.setQuoteSource(results.getString("inspirationalquotesource"));
		thePark.setDescription(results.getString("parkdescription"));
		thePark.setEntryFee(results.getInt("entryfee"));
		thePark.setNumOfSpecies(results.getInt("numberofanimalspecies"));

		return thePark;
	}

}
