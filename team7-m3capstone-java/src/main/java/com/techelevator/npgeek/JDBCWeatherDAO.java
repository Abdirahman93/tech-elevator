package com.techelevator.npgeek;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;


@Component
public class JDBCWeatherDAO implements WeatherDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCWeatherDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Weather> getAllWeatherInfo() {
		Weather theWeather = null;
		List<Weather> weather = new ArrayList<Weather>();
		String sqlGetAllActiveWeather = "SELECT * " + "FROM weather ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveWeather);
		while(results.next()){
			 theWeather = mapRowToWeather(results);
			 weather.add(theWeather);
			
		}
		
		return weather;
	}
	

	
	@Override
	public List<Weather> getWeatherByCode2(String parkCode) {
		Weather weather = null;
		List<Weather> theWeather = new ArrayList<Weather>();
		String sqlSelectWeatherByCode = "SELECT *  FROM weather WHERE parkcode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectWeatherByCode, parkCode);
		while(results.next()) {
			weather = mapRowToWeather(results);
			theWeather.add(weather);
		}
		return theWeather;
	}
	
	
	private Weather mapRowToWeather(SqlRowSet results) {
		Weather theWeather;
		theWeather = new Weather();
		theWeather.setParkCode(results.getString("parkcode"));
		theWeather.setFiveDayValue(results.getInt("fivedayforecastvalue"));
		theWeather.setLowTemp(results.getInt("low"));
		theWeather.setHighTemp(results.getInt("high"));
		theWeather.setForecast(results.getString("forecast"));
		
		
		return theWeather;
	}
	
}
