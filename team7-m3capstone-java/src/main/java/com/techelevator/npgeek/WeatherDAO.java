package com.techelevator.npgeek;

import java.util.List;

public interface WeatherDAO {

	public List<Weather> getAllWeatherInfo();

	public List<Weather> getWeatherByCode2(String parkCode);
	
//	public Weather getWeatherByCode(String parkCode);

}
