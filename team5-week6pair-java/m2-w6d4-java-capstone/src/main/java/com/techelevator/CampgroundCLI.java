package com.techelevator;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.Menu;

public class CampgroundCLI {

	private Menu menu;
	private JdbcTemplate jdbcTemplate;
	private static final String MAIN_MENU_OPTION_SELECT = "Select a park";
	private static final String MAIN_MENU_OPTION_QUIT = "Quit";
	private static final String[] MAIN_MENU = { MAIN_MENU_OPTION_SELECT, MAIN_MENU_OPTION_QUIT };
	private static final String PARK_MENU_OPTION_VIEW = "View Campgrounds";
	private static final String PARK_MENU_OPTION_SEARCH = "Search for Reservation";
	private static final String PARK_MENU_OPTION_RETURN = "Return to Prevous Screen";
	private static final String[] PARK_MENU_OPTION = new String[] { PARK_MENU_OPTION_VIEW, PARK_MENU_OPTION_SEARCH,
			PARK_MENU_OPTION_RETURN };
	private static final String LAST_MENU_SEARCH = "Search for Available Reservation";
	private static final String LAST_MENU_RETURN = "Return to Previous Screen";
	private static final String[] LAST_MENU = { LAST_MENU_SEARCH, LAST_MENU_RETURN };

	private campgroundDAO campgroundDAO;
	private siteDAO siteDAO;
	private parkDAO parkDAO;
	private reservationDAO reservationDAO;
	private String input;
	private String input2;
	private String input3;
	private String input4;
	private String input5;


	public CampgroundCLI(DataSource datasource) {
		this.menu = new Menu(System.in, System.out);

		// create your DAOs here
		campgroundDAO = new JDBCCampgroundDAO(datasource);
		siteDAO = new JDBCSiteDAO(datasource);
		parkDAO = new JDBCParkDAO(datasource);
		reservationDAO = new JDBCReservationDAO(datasource);
	}

	public void run() {

		while (true) {

			printHeading("Main Menu");
			String choice = (String) menu.getChoiceFromOptions(MAIN_MENU);
			if (choice.equals(MAIN_MENU_OPTION_QUIT)) {
				System.exit(0);
			} else if (choice.equals(MAIN_MENU_OPTION_SELECT)) {

				handlePark();
			}

		}
	}

	private void handlePark() {

		printHeading("Select a Park for Further Details");
		String choice = (String) menu.getChoiceFromOptions(parkDAO.getAllActiveParkNames());
		handleParkSearch(choice);
		String choice2 = (String) menu.getChoiceFromOptions(PARK_MENU_OPTION);
		if (choice2.equals(PARK_MENU_OPTION_VIEW)) {
			printHeading(choice + " National Park Campgrounds");
			handleCampSearch(choice);
			String choice3 = (String) menu.getChoiceFromOptions(LAST_MENU);
			if (choice3.equals(LAST_MENU_SEARCH)) {
				handleCampSearch(choice);
//				handleReservation();
			} else if (choice3.equals(LAST_MENU_RETURN)) {
				handleParkSearch(choice);
			}
		} else if (choice2.equals(PARK_MENU_OPTION_RETURN)) {
			handlePark();
		}

	}

	private void handleParkSearch(String parkName) {
		Park par = parkDAO.getAllParkInfo(parkName);
		printHeading("Park Information");

		System.out.println(par.getName() + " National Park");
		System.out.println("");
		System.out.println("Location: " + par.getLocation());
		System.out.println("Established: " + par.getDate());
		System.out.println("Area: " + par.getArea());
		System.out.println("Annual Visitors: " + par.getVisitors());
		System.out.println(par.getDescription());
	}

	private void handleCampSearch(String campName) {

		List<Campground> car = campgroundDAO.getAllCampInfo(campName);
		System.out.println(String.format("%-2s %-35s %-10s %-10s %s", "", "Name", "Open", "Close", "Fee"));
		for (Campground name : car) {
			System.out.println(String.format("%-2s %-35s %-10s %-10s $%s", name.getCampgroundId(), name.getName(),
					convertMonth(name.getOpenFrom()), convertMonth(name.getOpenTo()), name.getDailyFee()));
		}
		 
	}

	

//	private void handleReservation() {
//		 input = menu.getChoiceFromOption("Which campground (enter 0 to cancel)? ");
//		 input2 = menu.getChoiceFromOption("What is the arrival date? ");
//		 input3 = menu.getChoiceFromOption("What is the departure date? ");
//		List<Site> site = siteDAO.getAllSiteInfo(Long.parseLong(input));
//		System.out.println(String.format("%-20s %-15s %-15s %-20s %s", "Site Number()", "Max Occupancy",
//				"Accessible", "Max Rv Length", "Utilities"));
//			for (Site name : site) {
//				
////				System.out.println(String.format("%-20s %-15s %-15s %-20s %s", name.getSite_Number(), name.getMaxOccupancy(),
////						handleAccessible(), name.getMaxRvLength(), handleUtilities()));
//
//			}
//			handleAddReservation();
//	}
//	private String handleAccessible(){
//		List<Site> site = siteDAO.getAllSiteInfo(Long.parseLong(input));
//		for(Site name : site){
//			boolean acc = name.getAccessible();
//			if(acc == true){
//				return "Yes";
//			}else{
//			return "No";
//			}
//		}
//		return "";
//	}
//	
//	private String handleUtilities(){
//		List<Site> site = siteDAO.getAllSiteInfo(Long.parseLong(input));
//		for(Site name : site){
//			boolean acc = name.getUtilities();
//			if(acc == true){
//				return "Yes";
//			}else{
//			return "No";
//			}
//		}
//		return "";
//	}
//	
	private void handleAddReservation() {
		input4 = menu.getChoiceFromOption("Which site should be reserved (enter 0 to cancel)? ");	
		input5 = menu.getChoiceFromOption("What name should the reservation be made under? ");
		if(handleNotAddReservation() == false){
			System.out.println("The reservation can't be made. Please choose a different date.");

		}else{
		
		Reservation makeRes = reservationDAO.createReservation(Integer.parseInt(input4), input5, LocalDate.parse(input2), LocalDate.parse(input3));
		System.out.println("The reservation has been made and the confirmation id is " + makeRes.getReservationId());
		}
	}

	private boolean handleNotAddReservation() {
		List<Reservation> searchRes = reservationDAO.searchReservation(LocalDate.parse(input2), LocalDate.parse(input3));
			
		return true;
	}

	private String convertMonth(String month) {
		if (month.equals("01")) {
			return "January";
		} else if (month.equals("02")) {
			return "February";
		} else if (month.equals("03")) {
			return "March";
		} else if (month.equals("04")) {
			return "April";
		} else if (month.equals("05")) {
			return "May";
		} else if (month.equals("06")) {
			return "June";
		} else if (month.equals("07")) {
			return "July";
		} else if (month.equals("08")) {
			return "August";
		} else if (month.equals("09")) {
			return "September";
		} else if (month.equals("10")) {
			return "October";
		} else if (month.equals("11")) {
			return "November";
		} else if (month.equals("12")) {
			return "December";
		}
		return month;

	}
	
	private void printHeading(String headingText) {
		System.out.println("\n" + headingText);
		for (int i = 0; i < headingText.length(); i++) {
			System.out.print("-");
		}
		System.out.println();
	}

	private String getUserInput(String prompt) {
		System.out.print(prompt + " >>> ");
		return new Scanner(System.in).nextLine();
	}
}
