package com.techelevator;

import java.time.LocalDate;
import java.util.List;

public interface reservationDAO {

	public List<Reservation> getAllReservations();

	public Reservation createReservation(int siteId, String name, LocalDate fDate, LocalDate tDate);


	public List<Reservation> searchReservation(LocalDate fDate, LocalDate tDate);
}
