package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCCampgroundDAO implements campgroundDAO {
	
	private JdbcTemplate jdbcTemplate;

	public JDBCCampgroundDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Campground> getAllCampInfo(String nameSearch) {
		List<Campground> camps = new ArrayList<Campground>();
		
		String sqlGetAllActiveCamp = "SELECT *  FROM campground Join park ON park.park_id = campground.park_id WHERE park.name = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveCamp, nameSearch);
		while(results.next()){
			Campground theCamp = mapRowToCampground(results);
			camps.add(theCamp);
		}
		
		return camps;
	}
	
	
	
	
	private Campground mapRowToCampground(SqlRowSet results) {
		Campground theCamp;
		theCamp = new Campground();
		theCamp.setName(results.getString("name"));
		theCamp.setCampgroundId(results.getLong("campground_Id"));
		theCamp.setDailyFee(results.getDouble("daily_fee"));
		theCamp.setOpenFrom(results.getString("open_from_mm"));
		theCamp.setOpenTo(results.getString("open_to_mm"));
		theCamp.setParkId(results.getLong("park_id"));
		return theCamp;
	}
}
