package com.techelevator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCReservationDAO implements reservationDAO{

	private JdbcTemplate jdbcTemplate;

	public JDBCReservationDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Reservation> getAllReservations() {
		List<Reservation> res =  new ArrayList<Reservation>();
		String sqlGetAllActiveCamps = "SELECT * " + "FROM reservation";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveCamps);
		while(results.next()){
			Reservation theRes = mapRowToReservation(results);
			res.add(theRes);
		}
		
		return res;
	}
	
	@Override
	public Reservation  createReservation(int siteId, String name, LocalDate fDate, LocalDate tDate){
		Reservation reserve = new Reservation();
		reserve.setReservationId(getNextReservationId());
		reserve.setName(name);
		reserve.setFromDate(fDate);
		reserve.setToDate(tDate);
		String sqlCreateReservation = "INSERT INTO reservation(reservation_id, site_id, name, from_date, to_date) VALUES(?, ?, ?, ?, ?) ";
		jdbcTemplate.update(sqlCreateReservation, reserve.getReservationId(), siteId, name, fDate, tDate);
		return reserve;
		
	}
	
	@Override
	public List<Reservation> searchReservation (LocalDate fDate, LocalDate tDate){
		List<Reservation> res =  new ArrayList<Reservation>();
		String sqlGetAllActiveCamps = "SELECT * " +
				"FROM reservation " +
				"WHERE ? <= to_date AND ? >= from_date";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveCamps, fDate, tDate);
		while(results.next()){
			Reservation theRes = mapRowToReservation(results);
			res.add(theRes);
		}
		
		return res;
	}

	
	private Reservation mapRowToReservation(SqlRowSet results) {
		Reservation theRes;
		theRes = new Reservation();
		theRes.setFromDate(results.getDate("from_date").toLocalDate());
		theRes.setSiteId(results.getInt("site_id"));
		theRes.setToDate(results.getDate("to_date").toLocalDate());
		theRes.setCreateDate(results.getDate("create_date").toLocalDate());
		theRes.setReservationId(results.getLong("reservation_id"));
		theRes.setName(results.getString("name"));
		
		return theRes;
	}

	
	private long getNextReservationId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('reservation_reservation_id_seq')");
		if(nextIdResult.next()) {
			return nextIdResult.getLong(1);
		} else {
			throw new RuntimeException("Something went wrong while getting an id for the new reservation");
		}
	}


}
