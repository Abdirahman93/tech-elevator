package com.techelevator;

import java.time.LocalDate;

public class Reservation {
		private long reservationId;
	    private String name;
	    private LocalDate fromDate;
	    private int siteId;
	    private LocalDate toDate;
	    private LocalDate createDate;
	    
	    public long getReservationId(){
	        return reservationId;
	    }
	    public void setReservationId(long reservationId){
	        this.reservationId = reservationId;
	    }

	    public String getName(){
	        return name;
	    }
	    public void setName(String name){
	        this.name = name;
	    }
	    
	    public LocalDate getFromDate(){
	        return fromDate;
	    }
	    public void setFromDate(LocalDate fromDate){
	        this.fromDate = fromDate;
	    }
	    
	    public LocalDate getToDate(){
	        return toDate;
	    }
	    public void setToDate(LocalDate toDate){
	        this.toDate = toDate;
	    }
	    
	    public int getSiteId(){
	        return siteId;
	    }
	    public void setSiteId(int siteId){
	        this.siteId = siteId;
	    }
	    
	    public LocalDate getCreateDate(){
	        return createDate;
	    }
	    public void setCreateDate(LocalDate createDate){
	        this.createDate = createDate;
	    }
}
