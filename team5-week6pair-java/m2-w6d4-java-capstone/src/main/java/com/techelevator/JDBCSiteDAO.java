package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCSiteDAO implements siteDAO {
	private JdbcTemplate jdbcTemplate;

	public JDBCSiteDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Site> getAllSiteInfo(long id) {
		List<Site> sites = new ArrayList<Site>();
		
		String sqlGetAllActiveSites = "SELECT *  FROM site   WHERE campground_id = ? LIMIT 5";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveSites, id);
		while(results.next()){
			Site theSite = mapRowToSite(results);
			sites.add(theSite);
		}
		
		return sites;
	}
	
//	@Override
//	pubilc List<Site> getAllActiveSites(long id) {
//		List<Site> site = new ArrayList<Site>();
//		
//		String getAllActiveSite = "SELECT * FROM site JOIN reservation ON site.site_id= reservation.site_id WHERE campground_id = ? AND site_id NOT IN(SELECT site_id FROM reservation WHERE ? >= to_date AND ? <= from_date"
//	}
	
	private Site mapRowToSite(SqlRowSet results) {
		Site theSite;
		theSite = new Site();
		theSite.setSiteId(results.getLong("site_id"));
		theSite.setCampgroundId(results.getLong("campground_id"));
		theSite.setSiteNumber(results.getInt("site_number"));
		theSite.setMaxOccupancy(results.getInt("max_occupancy"));
		theSite.setAccessible(results.getBoolean("accessible"));
		theSite.setMaxRvLength(results.getInt("max_rv_length"));
		theSite.setUtilities(results.getBoolean("utilities"));
		
		return theSite;
	}
}
