package com.techelevator;


public class Campground {
	private Long campgroundId;
	private Long parkId;
	private String name;
	private String openFrom;
	private String openTo;
	private double dailyFee;
	
	public Long getCampgroundId() {
		return campgroundId;
	}
	public void setCampgroundId(Long campgroundId) {
		this.campgroundId = campgroundId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOpenFrom() {
		return openFrom;
	}
	public void setOpenFrom(String openFrom) {
		this.openFrom = openFrom;
	}
	public String getOpenTo() {
		return openTo;
	}
	public void setOpenTo(String openTo) {
		this.openTo = openTo;
	}
	
	public double getDailyFee() {
		return dailyFee;
	}
	public void setDailyFee(double d) {
		this.dailyFee = d;
	}
	
	public String toString() {
		return name;
	}
	
	public long getParkId(){
		return parkId;
	}
	
	public void setParkId(long parkId){
		this.parkId = parkId;
	}
}
