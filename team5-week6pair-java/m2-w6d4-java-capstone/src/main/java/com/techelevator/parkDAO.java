package com.techelevator;

import java.util.List;

public interface parkDAO {

	public Park getAllParkInfo(String nameSearch);
	public String[] getAllActiveParkNames();

}
