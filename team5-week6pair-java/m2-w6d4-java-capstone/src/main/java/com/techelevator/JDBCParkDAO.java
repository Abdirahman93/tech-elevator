package com.techelevator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;



public class JDBCParkDAO implements parkDAO{
	
	private JdbcTemplate jdbcTemplate;

	public JDBCParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Park getAllParkInfo(String nameSearch) {
		Park thePark = null;
		String sqlGetAllActivePark = "SELECT * " + "FROM park " + "WHERE parkname = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActivePark, nameSearch);
		if(results.next()){
			 thePark = mapRowToPark(results);
			
		}
		
		return thePark;
	}
	
	
	
	@Override
	public String[] getAllActiveParkNames() {
		List<String> parks = new ArrayList<String>();
		String sqlGetAllActivePark = "SELECT parkname " + "FROM park " + "ORDER BY parkname DESC";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActivePark);
		while(results.next()){
			
			parks.add(results.getString("parkname"));
		}
		
		return (String[]) parks.toArray(new String[parks.size()]);
	}
	
	
	private Park mapRowToPark(SqlRowSet results) {
		Park thePark;
		thePark = new Park();
		thePark.setName(results.getString("name"));
		thePark.setParkId(results.getLong("park_id"));
		thePark.setLocation(results.getString("location"));
		thePark.setDate(results.getDate("establish_date"));
		thePark.setArea(results.getString("area"));
		thePark.setVisitors(results.getInt("visitors"));
		thePark.setDescription(results.getString("description"));
		
		return thePark;
	}

	

}
