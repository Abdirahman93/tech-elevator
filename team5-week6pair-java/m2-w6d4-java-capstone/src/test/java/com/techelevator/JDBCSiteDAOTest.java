package com.techelevator;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;



public class JDBCSiteDAOTest {
	private static SingleConnectionDataSource dataSource;
	private JDBCSiteDAO site;
	private JdbcTemplate jdbcTemplate;

	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	@Before
	public void setup() {
		String sqlInsertReservation = "INSERT INTO site (site_id, campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities) VALUES(900, 1, 1, 6, 'false', 0, 'false')";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertReservation);
		site = new JDBCSiteDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}

	@Test
	public void get_all_site_info() {
		List<Site> results = site.getAllSiteInfo(1);

		Assert.assertNotNull(results);
		Assert.assertNotEquals(0,results.size());
	}
	
	private Site getSite( long SiteId, int campground_id, int site_number, int max_occupancy, boolean accessible, int max_rv_length, boolean utilities) {
		Site theSite = new Site();
		
		theSite.setMaxOccupancy(max_occupancy);
		theSite.setAccessible(accessible);
		theSite.setCampgroundId(campground_id);
		theSite.setMaxRvLength(max_rv_length);
		theSite.setSiteNumber(site_number);
		theSite.setSiteId(SiteId);
		theSite.setUtilities(utilities);
		
		return theSite;
	}
	
	private void assertProjectsAreEqual(Site expected, Site actual) {
		assertEquals(expected.getAccessible(), actual.getAccessible());
		assertEquals(expected.getCampgroundId(), actual.getCampgroundId());
		assertEquals(expected.getMaxOccupancy(), actual.getMaxOccupancy());
		assertEquals(expected.getMaxRvLength(), actual.getMaxRvLength());
		assertEquals(expected.getUtilities(), actual.getUtilities());
		assertEquals(expected.getSiteId(), actual.getSiteId());
		assertEquals(expected.getSite_Number(), actual.getSite_Number());
	}

	
}
