package com.techelevator;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCReservationDAOTest {
	private static SingleConnectionDataSource dataSource;
	private JDBCReservationDAO res;
	private JdbcTemplate jdbcTemplate;

	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	@Before
	public void setup() {
		String sqlInsertReservation = "INSERT INTO reservation (reservation_id, site_id, name, from_date, to_date, create_date) VALUES(51, 1, 'Smith Family Reservation', '2017-06-13', '2017-06-17', '2017-06-15')";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertReservation);
		res = new JDBCReservationDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}

	@Test
	public void search_reservation() {
		
		List<Reservation> results = res.searchReservation(LocalDate.parse("2017-06-13"), LocalDate.parse("2017-06-17"));

		Assert.assertNotNull(results);
	}
	
	@Test
	public void get_all_reservations() {
		List<Reservation> results = res.getAllReservations();

		Assert.assertNotNull(results);
		Assert.assertNotEquals(0, results.size());
	}
	
	@Test
	public void create_reservation(){
		res.createReservation(3, "new", LocalDate.parse("2017-06-13"), LocalDate.parse("2017-06-17"));
		String sqlselect = "SELECT name FROM reservation WHERE name = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect, "new");

		results.next();
		
		Assert.assertEquals("new", results.getString("name"));
	}
	
}
