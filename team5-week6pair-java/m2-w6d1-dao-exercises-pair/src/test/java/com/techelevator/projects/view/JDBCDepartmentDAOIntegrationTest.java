package com.techelevator.projects.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.jdbc.JDBCDepartmentDAO;


public class JDBCDepartmentDAOIntegrationTest {
	private static final String TEST_PROJECTEMPLOYEE = "BOB";
	private static final String TEST_DEPARTMENT= "Dep";

	private static SingleConnectionDataSource dataSource;
	private JDBCDepartmentDAO dao;
	private JdbcTemplate jdbcTemplate;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	@Before
	public void setup() {
		String sqlInsertEmployee = "INSERT INTO department (department_id, name) VALUES(5, 'hello')";
		String sqlInsertProjectEmployee = "INSERT INTO project_employee (project_id, employee_id) VALUES (1,2)";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertProjectEmployee);
		jdbcTemplate.update(sqlInsertEmployee);
		dao = new JDBCDepartmentDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	@Test
	public void get_all_departments(){
		List<Department> results = dao.getAllDepartments();
		
		Assert.assertNotNull(results);
		Assert.assertNotEquals(0, results.size());
	}
	
	@Test
	public void get_departments_by_name(){
		List<Department> results = dao.searchDepartmentsByName("hello");
		
		assertNotNull(results);
		assertEquals(1, results.size());
		
	}
	
	@Test
	public void update_department_name(){
		dao.updateDepartmentName(5L, "hi");
		String sqlselect = "SELECT name FROM department WHERE department_id = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect, 5L);
		
		results.next();
		
		Assert.assertEquals("hi", results.getString("name"));
	}
	
	@Test
	public void create_department(){
		dao.createDepartment("newDep");
		String sqlselect = "SELECT name FROM department WHERE name = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect, "newDep");

		results.next();
		
		Assert.assertEquals("newDep", results.getString("name"));
	}
	
	@Test
	public void get_department_by_id(){
		dao.getDepartmentById(5L);
		String sqlselect = "SELECT name FROM department WHERE department_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect, 5L);
		
		results.next();
		
		Assert.assertEquals("hello", results.getString("name"));
		
	}
	
	private Department getDepartment( long id, String name) {
		Department theDep = new Department();
		theDep.setId(id);
		theDep.getName();
		
		return theDep;
	}
	
	private void assertDepartmentsAreEqual(Department expected, Department actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getName(), actual.getName());
		
	}
}
