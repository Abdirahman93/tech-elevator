package com.techelevator.projects.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.jdbc.JDBCEmployeeDAO;

public class JDBCEmployeeDAOIntegrationTest {

	private static final String TEST_PROJECTEMPLOYEE = "BOB";
	private static final String TEST_EMPLOYEE = "Emp";

	private static SingleConnectionDataSource dataSource;
	private JDBCEmployeeDAO dao;
	private JdbcTemplate jdbcTemplate;

	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	@Before
	public void setup() {
		String sqlInsertEmployee = "INSERT INTO employee (employee_id, department_id, first_name, last_name, birth_date, gender, hire_date) VALUES(13, 1, 'Mat', 'Be', '1968-04-05', 'M', '2016-02-16')";
		String sqlInsertProjectEmployee = "INSERT INTO project_employee (project_id, employee_id) VALUES (2,13)";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertEmployee);
		jdbcTemplate.update(sqlInsertProjectEmployee);
		dao = new JDBCEmployeeDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}

	@Test
	public void get_all_employees() throws SQLException{
			List<Employee> results = dao.getAllEmployees();
			
			assertEquals(13, results.size());
		}
	
	@Test
	public void serach_employee_by_name(){
		List<Employee> results = dao.searchEmployeesByName("Mat", "Be");
		
		assertNotNull(results);
		assertEquals(1, results.size());
	}
	
	@Test
	public void get_Employees_By_Department_Id(){
		List<Employee> results = dao.getEmployeesByDepartmentId(2);
		assertNotNull(results);
		assertEquals(3, results.size());
	}
	
	@Test
	public void get_employee_without_projects(){
		List<Employee> results = dao.getEmployeesWithoutProjects();
		assertNotNull(results);
		Assert.assertNotEquals(1, results.size());
	}
	
	@Test
	public void get_Employees_by_project_id(){
		List<Employee> results = dao.getEmployeesByProjectId((long) 2);
		assertNotNull(results);
		assertEquals(1, results.size());
		Assert.assertEquals("Mat", results.get(0).getFirstName());
		Assert.assertEquals("Be", results.get(0).getLastName());

	}
	
	@Test
	public void change_employee_department(){
		dao.changeEmployeeDepartment((long)2, (long)3);
		String sqlselect = "SELECT department_id FROM employee WHERE employee_id = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect, 2L);
		
		results.next();
		
		Assert.assertEquals(3L, results.getLong("department_id"));
	}
	
	private Employee getEmployee(String firstName, String lastName, LocalDate birthDay, char gender, LocalDate hireDate) {
		Employee theEmployee = new Employee();
		theEmployee.setFirstName(firstName);
		theEmployee.setLastName(lastName);
		theEmployee.setBirthDay(birthDay);
		theEmployee.setGender(gender);
		theEmployee.setHireDate(hireDate);
		
		return theEmployee;
	}
	
	private void assertEmployeesAreEqual(Employee expected, Employee actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getFirstName(), actual.getFirstName());
		assertEquals(expected.getLastName(), actual.getLastName());
		assertEquals(expected.getDepartmentId(), actual.getDepartmentId());
	}
}
