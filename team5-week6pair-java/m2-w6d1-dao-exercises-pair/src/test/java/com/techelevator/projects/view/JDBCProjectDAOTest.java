package com.techelevator.projects.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import javax.sql.rowset.serial.SerialArray;
import javax.sql.rowset.serial.SerialClob;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Project;
import com.techelevator.projects.model.jdbc.JDBCProjectDAO;

public class JDBCProjectDAOTest {
	private static final String TEST_PROJECTEMPLOYEE = "BOB";
	private static final String TEST_PROJECT= "Pro";

	private static SingleConnectionDataSource dataSource;
	private JDBCProjectDAO dao;
	private JdbcTemplate jdbcTemplate;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	@Before
	public void setup() {
		String sqlInsertEmployee = "INSERT INTO project (project_id, name, from_date, to_date) VALUES(11, 'Project XYX'	, '961-03-01', '2002-08-31')";
		String sqlInsertProjectEmployee = "INSERT INTO project_employee (project_id, employee_id) VALUES (1,2)";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertProjectEmployee);
		jdbcTemplate.update(sqlInsertEmployee);
		dao = new JDBCProjectDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	@Test
	public void get_all_projects(){
		List<Project> results = dao.getAllActiveProjects();
		
		Assert.assertNotNull(results);
		Assert.assertNotEquals(0, results.size());
		
	}
	
	@Test
	public void remove_employeee_from_project(){
		
		dao.removeEmployeeFromProject((long) 1, (long)2);
		String sqlselect = "SELECT * FROM project_employee WHERE employee_id = 2 AND project_id = 1";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect);

		Assert.assertFalse(results.next());
	}
	
	@Test
	public void add_employee_to_project(){
		dao.addEmployeeToProject((long) 3, (long) 2);
		String sqlselect = "SELECT project_id FROM project_employee WHERE employee_id = 2 AND project_id = 3 ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlselect);

		Assert.assertTrue(results.next());
	}
	

	private Project getProject( long id, String name, LocalDate fromDate, LocalDate toDate) {
		Project thePro = new Project();
		thePro.setId(id);
		thePro.setName(name);
		thePro.setStartDate(fromDate);
		thePro.setEndDate(toDate);
		
		return thePro;
	}
	
	private void assertProjectsAreEqual(Project expected, Project actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getStartDate(), actual.getStartDate());
		assertEquals(expected.getEndDate(), actual.getEndDate());
	}
}

