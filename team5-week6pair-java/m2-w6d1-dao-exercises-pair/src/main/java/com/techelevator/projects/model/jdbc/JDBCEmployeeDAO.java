package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.EmployeeDAO;

public class JDBCEmployeeDAO implements EmployeeDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCEmployeeDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Employee> getAllEmployees() {
		ArrayList<Employee> employees = new ArrayList<>();
		String sqlGetAllEmployees = "SELECT * " + "FROM employee";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllEmployees);
		while (results.next()) {
			Employee theEmployee = mapRowToEmployee(results);
			employees.add(theEmployee);
		}
		return employees;
	}

	@Override
	public List<Employee> searchEmployeesByName(String firstNameSearch, String lastNameSearch) {
		List<Employee> emp = new ArrayList<Employee>();
		Employee theEmployee;
		String sqlSearchEmployeesByName = "SELECT * " + "FROM employee " + "WHERE first_name = ? AND last_name = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchEmployeesByName, firstNameSearch, lastNameSearch);
		while(results.next()){
			theEmployee = mapRowToEmployee(results);
			emp.add(theEmployee);
		}
		return emp;
	}

	@Override
	public List<Employee> getEmployeesByDepartmentId(long id) {
		List<Employee> emp1 =  new ArrayList<Employee>();
		Employee theEmployee;
		String sqlGetEmployeeByDepartmentId = "SELECT * " + "FROM department " + "JOIN employee ON department.department_id = employee.department_id  "+ "WHERE department.department_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeeByDepartmentId, id);
		while(results.next()){
			theEmployee = mapRowToEmployee(results);
			emp1.add(theEmployee);
		}
		return emp1;
	}

	@Override
	public List<Employee> getEmployeesWithoutProjects() {
		List<Employee> emp2 =  new ArrayList<Employee>();
		Employee theEmployee;
		String sqlGetEmployeesWithoutProjects = "SELECT * " + "FROM project_employee " + "RIGHT OUTER JOIN employee ON project_employee.employee_id = employee.employee_id";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeesWithoutProjects);
		while(results.next()){
			theEmployee = mapRowToEmployee(results);
			emp2.add(theEmployee);
		}
		return emp2;
	}

	@Override
	public List<Employee> getEmployeesByProjectId(Long projectId) {
		List<Employee> emp3 =  new ArrayList<Employee>();
		Employee theEmployee;
		String sqlGetEmployeesByProjectId = "SELECT * " + "FROM employee " + "JOIN project_employee ON employee.employee_id = project_employee.employee_id  "+ "WHERE project_id = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeesByProjectId, projectId);
		while(results.next()){
			theEmployee = mapRowToEmployee(results);
			emp3.add(theEmployee);
		}
		return emp3;
	}

	@Override
	public void changeEmployeeDepartment(Long employeeId, Long departmentId) {
		Employee theEmployee;
		String changeEmployeeDepartmentName = "UPDATE employee " + "SET department_id = ? " + "WHERE employee_id = ?";
		jdbcTemplate.update(changeEmployeeDepartmentName, departmentId, employeeId);
		
	}
	
	private Employee mapRowToEmployee(SqlRowSet results) {
		Employee theEmployee;
		theEmployee = new Employee();
		theEmployee.setId(results.getLong("employee_id"));
		theEmployee.setFirstName(results.getString("first_name"));
		theEmployee.setLastName(results.getString("last_Name"));
		theEmployee.setDepartmentId(results.getLong("department_Id"));
		
		return theEmployee;
	}
	


}
