package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.Project;
import com.techelevator.projects.model.ProjectDAO;

public class JDBCProjectDAO implements ProjectDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCProjectDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Project> getAllActiveProjects() {
		List<Project> projects =  new ArrayList<Project>();
		String sqlGetAllActiveProjects = "SELECT * " + "FROM project";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveProjects);
		while(results.next()){
			Project theProject = mapRowToProject(results);
			projects.add(theProject);
		}
		
		return projects;
	}

	@Override
	public void removeEmployeeFromProject(Long projectId, Long employeeId) {
		Project theProject;
		String sqlRemoveEmployeeFromProject = "DELETE FROM project_employee " + "WHERE employee_id = ? AND project_id = ?";
		jdbcTemplate.update(sqlRemoveEmployeeFromProject, employeeId, projectId);
	}

	@Override
	public void addEmployeeToProject(Long projectId, Long employeeId) {
		Project theProject;
		String sqlAddEmployeeToProject = "INSERT INTO project_employee(employee_id, project_id) " + "VALUES ( ?, ? )";
		jdbcTemplate.update(sqlAddEmployeeToProject, employeeId, projectId);
	}
	
	private Project mapRowToProject(SqlRowSet results) {
		Project theProject;
		theProject = new Project();
		theProject.setName(results.getString("name"));
		theProject.setId(results.getLong("project_id"));
		
		return theProject;
	}
	
	private long getNextEmployeeId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_project_id')");
		if(nextIdResult.next()) {
			return nextIdResult.getLong(1);
		} else {
			throw new RuntimeException("Something went wrong while getting an id for the new project");
		}
	}
}
