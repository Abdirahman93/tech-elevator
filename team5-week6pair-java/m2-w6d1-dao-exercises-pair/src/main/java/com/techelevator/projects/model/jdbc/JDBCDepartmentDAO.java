package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;

public class JDBCDepartmentDAO implements DepartmentDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCDepartmentDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Department> getAllDepartments() {
		ArrayList<Department> departments = new ArrayList<>();
		String sqlgetAllDepartments = "SELECT * " + "FROM department";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlgetAllDepartments);
		while (results.next()) {
			Department theDepartment = mapRowToDepartment(results);
			departments.add(theDepartment);
		}
		return departments;
	}

	@Override
	public List<Department> searchDepartmentsByName(String nameSearch) {
		List<Department> dep = new ArrayList<Department>();
		Department theDepartment;
		String searchDepartmentsByName = "SELECT name, department_id " + "FROM department " + " WHERE name = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(searchDepartmentsByName, nameSearch);
		while(results.next()) {
			theDepartment = mapRowToDepartment(results);
			dep.add(theDepartment);
		}
		return dep;
	}

	@Override
	public void updateDepartmentName(Long departmentId, String departmentName) {
		Department theDepartment;
		String updateDepartmentName = "UPDATE department " + " SET name = ? " + " WHERE department_id = ?";
		jdbcTemplate.update(updateDepartmentName, departmentName, departmentId);
		
	}

	@Override
	public Department createDepartment(String departmentName) {
		Department theDepartment = new Department();
		theDepartment.setName(departmentName);
		theDepartment.setId(getNextDepartmentId());
		String createDepartment = "INSERT INTO department (name, department_Id)" + "Values ( ? , ? ) ";
		jdbcTemplate.update(createDepartment, theDepartment.getName(), theDepartment.getId());
		
		
		return theDepartment;
	}

	@Override
	public Department getDepartmentById(Long id) {
		Department theDepartment = null;
		String getDepartmentById = "SELECT name, department_id " + " FROM department" + " WHERE department_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(getDepartmentById, id);
		if (results.next()) {
			theDepartment = mapRowToDepartment(results);
		}
		return theDepartment;
	}

	private Department mapRowToDepartment(SqlRowSet results) {
		Department theDepartment;
		theDepartment = new Department();
		theDepartment.setId(results.getLong("department_id"));
		theDepartment.setName(results.getString("name"));
		return theDepartment;
	}
	
	private long getNextDepartmentId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_department_id')");
		if(nextIdResult.next()) {
			return nextIdResult.getLong(1);
		} else {
			throw new RuntimeException("Something went wrong while getting an id for the new department");
		}
	}
}
