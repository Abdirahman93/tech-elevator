<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Recipe List View</title>
    <link rel="stylesheet" href="css/recipeTiles.css" />
</head>
<body>
    <header>
        <h1>MVC Exercises - Views Part 2: Models</h1>        
    </header>
    <nav>
        <ul>
            <li><a href="recipeTiles">Tile Layout</a></li>
            <li><a href="recipeTable">Table Layout</a></li>
        </ul>
        
    </nav>
    
    <section id="main-content">
   	<h1>Recipes</h1>
		
		<c:forEach var="recipe" items="${recipes}">
			<div id="body">
			<a href= "recipeDetails?recipeId=${recipe.recipeId}">
			<img
				src= "img/recipe${recipe.recipeId}.jpg" alt="${recipe.name}"></a>
			<div id="des">
				<div id="n">
				<c:out value="${recipe.name}"/>
				</div>
				<div id="r">
				<img src="img/${Math.round(recipe.averageRating)}-star.png" style="width: 5em; height: 0.9em;" />
				</div>
				<div id = "ing">
				<c:out value="${recipe.numberOfIngredients} ingredients"/>
				</div>
			</div>
			</div>
		</c:forEach>
	
    </section>
</body>
</html>