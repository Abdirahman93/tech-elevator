<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Recipe Table View</title>
    <link rel="stylesheet" href="css/recipeTable.css" />
</head>
<body>
    <header>
        <h1>MVC Exercises - Views Part 2: Models</h1>        
    </header>
    <nav>
        <ul>
            <li><a href="recipeTiles">Tile Layout</a></li>
            <li><a href="recipeTable">Table Layout</a></li>
        </ul>
        
    </nav>
    <section id="main-content">

<table>
			<tr>
    <td id="title"></td>
    <c:forEach var="recipe" items="${recipes}">
        <td> <a href= "recipeDetails?recipeId=${recipe.recipeId}">
			<img
				src= "img/recipe${recipe.recipeId}.jpg" alt="${recipe.name}"></a></td>
    </c:forEach>
    </tr>
    
    <tr>
    <td id="title"> Name </td>
    <c:forEach var="recipe" items="${recipes}">
    <td class="even">${recipe.name}</td>
    </c:forEach>
    </tr>
    
     <tr>
    <td id="title"> Type </td>
    <c:forEach var="recipe" items="${recipes}">
    <td>${recipe.recipeType}</td>
    </c:forEach>
    </tr>
    
     <tr>
    <td id="title"> Cook Time </td>
    <c:forEach var="recipe" items="${recipes}">
    <td class="even">${recipe.cookTimeInMinutes} min</td>
    </c:forEach>
    </tr>
    
     <tr>
    <td id="title"> Ingredients </td>
    <c:forEach var="recipe" items="${recipes}">
    <td>${recipe.numberOfIngredients} ingredients</td>
    </c:forEach>
    </tr>
    
    <tr>
    <td id="title"> Rating </td>
    <c:forEach var="recipe" items="${recipes}">
    <td  class="even"><img src="img/${Math.round(recipe.averageRating)}-star.png" style="width: 5em; height: 0.9em;" /></td>
    </c:forEach>
    </tr>
    
	</table>
    </section>
</body>
</html>