<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>Recipe List View</title>
<link rel="stylesheet" href="css/detail.css" />
</head>
<body>
	<header>
		<h1>MVC Exercises - Views Part 2: Models</h1>
	</header>
	<nav>
		<ul>
			<li><a href="recipeTiles">Tile Layout</a></li>
			<li><a href="recipeTable">Table Layout</a></li>
		</ul>

	</nav>

	<section id="main-content">

		<div id="body">
			<img src="img/recipe${recipe.recipeId}.jpg" alt="${recipe.name}">
			<div id="body1">
				<div id="name">
					<c:out value="${recipe.name}" />
				</div>
				<div id="type">
					<c:out value="${recipe.recipeType}" />
				</div>
				<div id="cook">
					<c:out value="Cook Time ${recipe.cookTimeInMinutes}" />
				</div>
				<div id="description">
					<c:out value="${recipe.description}" />
				</div>
			</div>
		</div>
		
			<h4>Ingredients</h4>
			<div id="body2">
			<h4>Ingredients</h4>
			<div id="ingredients">
				<ul>
					<c:forEach var="recipe" items="${recipe.ingredients}">
					<li>
					<c:out value="${recipe.quantity}" />, <c:out value="${recipe.name}"></c:out></li>
					</c:forEach>
				</ul>
			</div>

		</div>
		<div id="body3">

			<div id="prepration">
				<ol>
				<c:forEach var="recipe" items="${recipe.preparationSteps}">
					
					<li><c:out value="${recipe}" /></li>
					
					</c:forEach>
			</ol>
			</div>

		</div>

	</section>
</body>
</html>