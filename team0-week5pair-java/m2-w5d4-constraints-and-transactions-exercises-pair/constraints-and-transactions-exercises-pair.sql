-- Write queries to return the following:
-- Make the following changes in the "world" database.

-- 1. Add Superman's hometown, Smallville, Kansas to the city table. The 
-- countrycode is 'USA', and population of 45001. (Yes, I looked it up on 
-- Wikipedia.)
INSERT INTO city(id, name, countrycode, district, population)
VALUES (0 ,'Smallville', 'USA', 'Kansas', 45001);
SELECT *
FROM city
WHERE id = 0;
-- 2. Add Kryptonese to the countrylanguage table. Kryptonese is spoken by 0.0001
-- percentage of the 'USA' population.
INSERT INTO countrylanguage (countrycode, language, isofficial, percentage)
VALUES ('USA', 'Kryptonese', false, 0.0001);
-- 3. After heated debate, "Kryptonese" was renamed to "Krypto-babble", change 
-- the appropriate record accordingly.
UPDATE countrylanguage
SET language = 'Krypto-babble'
WHERE language = 'Kryptonese';
-- 4. Set the US captial to Smallville, Kansas in the country table.
UPDATE country
SET capital = 0
WHERE code = 'USA';
-- 5. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)
DELETE FROM city
WHERE name = 'Smallville';
--it did not work. Because it is the capital in country.

-- 6. Return the US captial to Washington.
UPDATE country
SET capital = 3813
WHERE code = 'USA';
-- 7. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)
DELETE FROM city
WHERE name = 'Smallville';
--It worked because it is no longer the capital of the US.

-- 8. Reverse the "is the official language" setting for all languages where the
-- country's year of independence is within the range of 1800 and 1972 
-- (exclusive). 
-- (590 rows affected)


BEGIN TRANSACTION;

UPDATE countrylanguage
SET isofficial= NOT isofficial
WHERE countrylanguage.countrycode IN (
SELECT country.code
FROM country
JOIN countrylanguage ON country.code = countrylanguage.countrycode
WHERE indepyear > 1800 AND indepyear < 1972
ORDER BY indepyear ASC
)
 
 COMMIT;
-- 9. Convert popuation so it is expressed in 1,000s for all cities. (Round to
-- the nearest integer value greater than 0.)
-- (4079 rows affected)
BEGIN TRANSACTION;

UPDATE city
SET population = round(population/1000);

ROLLBACK;
-- 10. Assuming a country's surfacearea is expressed in miles, convert it to 
-- meters for all countries where French is spoken by more than 20% of the 
-- population.
-- (7 rows affected)

UPDATE country
SET surfacearea = surfacearea/1609.34
WHERE country.code IN (
SELECT country.code
FROM countrylanguage
JOIN country ON countrylanguage.countrycode = country.code
WHERE language = 'French' AND percentage > 20
)

ROLLBACK;

