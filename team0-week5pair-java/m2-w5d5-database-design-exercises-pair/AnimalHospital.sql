Create Table pet(
pet_id serial not null,
name varchar(30) not null,
age int not null,

constraint pk_pet_id primary key (pet_id)
)

Create Table owner(
owner_id serial not null,
first_name varchar(30),
last_name varchar(30),
pet_id int not null,

constraint pk_owner_id primary key (owner_id),
constraint fk_pet_id foreign key (pet_id) references pet(pet_id)
)

Create Table pet_type(
pet_type_id serial not null,
type varchar(16) not null,
pet_id int not null,

constraint pk_pet_type_id primary key (pet_type_id),
constraint fk_pet_id foreign key (pet_id) references pet(pet_id)
)

Create Table procedure(
procedure_id serial not null,
type varchar(100) not null,
pet_id int not null,
cost decimal(6, 2) not null,

constraint pk_procedure_id primary key (procedure_id),
constraint fk_pet_id foreign key (pet_id) references pet(pet_id)
)

Create Table visit(
visit_id serial not null,
visit_date Date not null,

constraint pk_visit_id primary key (visit_id)
)

Create Table visit_procedure(
visit_id int not null,
procedure_id int not null,

constraint pk_visit_procedure_visit_id_procedure_id primary key (visit_id, procedure_id),
constraint fk_visit_id foreign key (visit_id) references visit(visit_id),
constraint fk_procedure_id foreign key (procedure_id) references procedure(procedure_id)
)

Create Table invoice(

invoice_id serial not null,
tax decimal(4,2) not null,
invoice_date Date not null,

constraint pk_invoice_id primary key (invoice_id)
)

Create Table visit_invoice(
visit_id int not null,
invoice_id int not null,

constraint pk_visit_invoice_visit_id_invoice_id primary key (visit_id, invoice_id),
constraint fk_visit_id foreign key (visit_id) references visit(visit_id),
constraint fk_invoice_id foreign key (invoice_id) references invoice(invoice_id)
 )
 
 Drop Table department, employee, project;