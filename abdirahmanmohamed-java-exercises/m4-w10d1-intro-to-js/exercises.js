﻿function sumDouble(num1, num2) { 
	var sum = num1 + num2;
	if(num1 === num2){
		return sum *2;
	}else{
		return sum;
	}
}

function hasTeen(num1, num2, num3) {
	if(num1>=13 & num1<= 19){
		return true;
	}else if(num2>=13 & num2<= 19){
		return true;
	}else if(num3>=13 & num3<= 19){
		return true;
	}else{
		return false
	}
}

function lastDigit(num1, num2){
	var str1 = num1.toString();
	var str2 = num2.toString();
	if(str1.charAt(str1.length-1) === str2.charAt(str2.length-1)){
		return true;
	}else{
		return false;
	}
}

function seeColor(str){
	
	if(str.search("red") == 0){
		return "red";
	}else if(str.search("blue") == 0){
		return "blue";
	}else{
		return "";
	}
}

function middleThree(str){
    	var position;

        position = str.length / 2;
        
       return str.substring(position - 1, position + 2);

}

function frontAgain(str){
	return str.substring(0,2) == str.substring(str.length-2, str.length)
}

function alarmClock(day, vacation){
	
	
	if(vacation == true){
		if(day>0 && day<6){
			return "10:00";
		}else{
			return "off";
		}
	}else{
		if(day>0 && day < 6){
			return "7:00";
		}else{
			return "10:00";
		}
	}	
}

function makeMiddle(nums){
	if (nums.length %2 == 0 && nums.length >= 2) {
	    // even-length array (two middle elements)
	    return [nums[(nums.length/2) - 1], nums[nums.length/2]];
		return avg;
	}else{
		return [];
	}
}

function oddOnly(nums) {
	// for
	var result = [];
	for(var i = 0; i < nums.length; i++) {
		if(nums[i] %2 !== 0){
			result.push(nums[i]);
		}
	}
	
	return result;
}

function weave(nums1, nums2){
	var nums3 = [];
	if(nums1.length == nums2.length){
		for(var i = 0; i < nums1.length; i++){
			nums3.push(nums1[i]);
			nums3.push(nums2[i]);
			
		}
	} else if(nums1.length < nums2.length){
		for(var i = 0; i < nums1.length; i++){
			nums3.push(nums1[i]);
			nums3.push(nums2[i]);
			
		}
		for(var i = nums1.length; i < nums2.length; i++){
			
			nums3.push(nums2[i]);
			
		}
	} else{
		for(var i = 0; i < nums2.length; i++){
			nums3.push(nums1[i]);
			nums3.push(nums2[i]);
			
		}
		for(var i = nums2.length; i < nums1.length; i++){
			
			nums3.push(nums1[i]);
			
		}
	}
	return nums3;
}

function cigarParty(num, weekend){
	
	if(weekend == true && num > 40){
		return true
	}else if(weekend == false && num > 40 && num < 60){
		return true;
	}else{
		return false;
	}
}

function stringSplosion(str){
	var str1 = [];
	  for(var i = 1; i <= str.length; i++){
	    var str2 = str.substr(0, i);
	    str1.push(str2);
	  }
	return str1.join('');
}

function fizzBuzz(num){
	if(num % 3 == 0 && num % 5 == 0){
		return "FizzBuzz"
	}else if(num % 5 == 0){
		return "Buzz";
	}else if(num % 3 == 0){
		return "Fizz"
	}else{
		return num;
	}
}

function reverseArray(numArray){
	var result = [];
	for(var i = 0; i < numArray.length; i++){
		var currentNum = numArray[i];
		result[numArray.length - (i+1)] = currentNum;
	}
	return result;
}

function countValues(numArray){
	var count = {};
	for(var i = 0; i < numArray.length; i++){
		var current = numArray[i];
		var currentCount = count[current];
		if(currentCount === undefined){
			currentCount = 1;
		}else{
			currentCount++;
		}
		count[current]= currentCount;
	}
	return count;
}

