<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>Product Tiles View</title>
<link rel="stylesheet" href="css/site.css" />
</head>
<body>
	<header>
		<h1>MVC Exercises - Views Part 2: Models</h1>
	</header>
	<nav>
		<ul>
	  		<li><a href="productList">List Layout</a></li>
            <li><a href="productTiles">Tile Layout</a></li>
            <li><a href="productTable">Table Layout</a></li>
		</ul>

	</nav>
	<section id="main-content">
		<h1>Toy Department</h1>

		<c:forEach var="product" items="${productList}">
			<div id="body1">
				<div>
				<a href= "productDetail?productId=${product.productId }"><img id="image" src = "img/${product.imageName}"
					alt="${product.name}"></a>
					</div>
				<div id="view">
					<div id="name">
						<c:out value="${product.name}" />
						<div id="seller">
						<c:if test="${product.topSeller}">
						<c:out  value= "BEST SELLER!" />
						</c:if>
						<c:if test="${not product.topSeller}">
						<c:out value= ""/>
						</c:if>
						</div>
			
					</div>
					<div id="manu">
						<c:out value="${product.manufacturer}" />
					</div>
					<div id="price">
						<c:out value="$${product.price}" />
					</div>
					<div id="weight">
						<c:out value="Weight ${product.weightInLbs}" />
					</div>
					<div id="rating">
						<img src="img/${Math.round(product.averageRating)}-star.png"
							style="width: 5em; height: 0.9em;" />
					</div>
				</div>

			</div>

		</c:forEach>

	</section>
</body>
</html>