<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Product Table View</title>
    <link rel="stylesheet" href="css/site.css" />
</head>
<body>
    <header>
        <h1>MVC Exercises - Views Part 2: Models</h1>        
    </header>
    <nav>
        <ul>
            <li><a href="productList">List Layout</a></li>
            <li><a href="productTiles">Tile Layout</a></li>
            <li><a href="productTable">Table Layout</a></li>
        </ul>
        
    </nav>
    <section id="main-content">
    	<h1>Toy Department</h1>
		<table>
			<tr>
				<td id="title"></td>
				<c:forEach var="product" items="${productList}">
				<td><a href= "productDetail?productId=${product.productId }"><img id="tableImage" src = "img/${product.imageName}"
					alt="${product.name}"></a>
					<div id="seller">
						<c:if test="${product.topSeller}">
						<c:out  value= "BEST SELLER!" />
						</c:if>
						<c:if test="${not product.topSeller}">
						<c:out value= ""/>
						</c:if>
						</div></td>
					</c:forEach>
					</tr>
					
			<tr>
				<td id="name">Name</td>
				<c:forEach var="product" items="${productList}">
					<td class="even">${product.name }</td>
				</c:forEach>
			</tr>
			
			<tr>
				<td id="rating">Rating</td>
				<c:forEach var="product" items="${productList}">
					<td><img src="img/${Math.round(product.averageRating)}-star.png"
							style="width: 5em; height: 0.9em;" /></td>
				</c:forEach>
			</tr>
			
			<tr>
				<td id="mfr">Mfr</td>
				<c:forEach var="product" items="${productList}">
					<td class="even">${product.manufacturer }</td>
				</c:forEach>
			</tr>
			
			<tr>
				<td id="price">Price</td>
				<c:forEach var="product" items="${productList}">
					<td >$${product.price }</td>
				</c:forEach>
			</tr>
			
			<tr>
				<td id="wt">wt. (in lbs)</td>
				<c:forEach var="product" items="${productList}">
					<td class="even">${product.weightInLbs }</td>
				</c:forEach>
			</tr>
		</table>
       

    </section>
</body>
</html>