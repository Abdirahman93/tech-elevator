package com.techelevator;

public class Exercises {

	public static void main(String[] args) {
        
        /* 
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch? 
        */
		int birdsOnBranch = 4;
		int birdsFlewAway = 1;
		int birdsLeft = birdsOnBranch - birdsFlewAway;
		System.out.println("Number of birds left: " + birdsLeft);
        /* 
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests? 
        */
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int moreBirdsThanNests = numberOfBirds - numberOfNests;
		System.out.println("Number of more birds than nests: " + moreBirdsThanNests);
        /* 
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods? 
        */
		int racInWoods = 3;
		int racHome = 2;
		int leftWoods = racInWoods - racHome;
		System.out.println("Raccoons left in the woods: " + leftWoods);
        /* 
        4. There are 5 flowers and 3 bees. How many less bees than flowers? 
        */
		int flowers = 5;
		int bees = 3;
		int beesLessThanFlowers = flowers - bees;
		System.out.println("Number of less bees than flowers: " + beesLessThanFlowers);

        /* 
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now? 
        */
		int firstPigeon = 1;
		int secondPigeon = 1;
		int totalPigeons = firstPigeon + secondPigeon;
		System.out.println("Pigeons eating breadcrumbs: " + totalPigeons);

        /* 
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now? 
        */
		int owlsOnFence = 3;
		int newOwls = 2;
		int totalOwls = owlsOnFence + newOwls;
		System.out.println("Owls on fence now: " + totalOwls);

        /* 
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home? 
        */
		int beaversHome = 2;
		int beaversSwim = 1;
		int beaversStillHome = beaversHome - beaversSwim;
		System.out.println("Beavers still working home: " + beaversStillHome);
		

        /* 
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all? 
        */
		int toucansOnLimb = 2;
		int toucansJoin = 1;
		int toucansTotal = toucansOnLimb + toucansJoin;
		System.out.println("Total number of toucans: " + toucansTotal);

        /* 
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts? 
        */
		int squirrels = 4;
		int nuts = 2;
		int moreSquirrels = squirrels - nuts;
		System.out.println("Number of more squirrels than nuts: " + moreSquirrels);

        /* 
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find? 
        */
		double quarter = .25;
		double dime = .10;
		double nickel = .05;
		double totalNickels = nickel + nickel;
		double totalMoney = quarter + dime + totalNickels;
		System.out.println("Total money Mrs. Hilt found : " + totalMoney);

        /* 
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all? 
        */
		int briers = 18;
		int macAdams = 20;
		int flannery = 17;
		int totalMuffins = briers + macAdams + flannery;
		System.out.println("Total number of muffins : " + totalMuffins);

        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		double yoyo = .24;
		double whistle = .14;
		double spentOnToys = yoyo + whistle;
		System.out.println("Total money spent on two toys : " + spentOnToys);
        
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
		int miniMarshmallows = 10;
		int largeMarshmallows = 8;
		int totalMarshmallows = miniMarshmallows + largeMarshmallows;
		System.out.println("Toal number of marshmallows used : " + totalMarshmallows);
        
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
		int hiltSnow = 29;
		int brecknockSnow = 17;
		int moreHilt = hiltSnow - brecknockSnow;
		System.out.println("Number of more inches of snow Mrs. Hilt's house had : " + moreHilt);
        
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
		int hiltTotalMoney = 10;
		int spentToyTruck = 3;
		int spentPencil = 2;
		int moneyLeft = hiltTotalMoney - (spentToyTruck + spentPencil);
		System.out.println("Money left : " + moneyLeft);
        
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
		int totalMarbles = 16;
		int lostMarbles = 7;
		int marblesLeft = totalMarbles - lostMarbles;
		System.out.println("Number of marbles left : " + marblesLeft);
        
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int hasSeaShells = 19;
		int toHaveSeaShells = 25;
		int needsSeaShells = toHaveSeaShells - hasSeaShells;
		System.out.println("Seashells needed to have 25 seashells : " + needsSeaShells);
        
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int totalBalloons = 17;
		int redBalloons = 8;
		int greenBalloons = totalBalloons - redBalloons;
		System.out.println("Number of green balloons : " + greenBalloons);
            
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
		int firstBooksOnShelf = 38;
		int newBooksOnShelf = 10;
		int totalBooksOnShelf = firstBooksOnShelf + newBooksOnShelf;
		System.out.println("Total books on shelf : " + totalBooksOnShelf);
        
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int oneBee = 6;
		int eightBees = oneBee * 8;
		System.out.println("Number of legs 8 bees have : " + eightBees);
        
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
		double oneCone = .99;
		double twoCones = oneCone * 2;
		System.out.println("Two ice cream cones cost : " + twoCones);
        
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int hasRocks = 64;
		int totalRocks = 125;
		int needsRocks = totalRocks - hasRocks;
		System.out.println("Rocks she needs to complete : " + needsRocks);
        
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
		int hadMarbles = 38;
		int hiltLostMarbles = 15;
		int hiltMarblesLeft = hadMarbles - hiltLostMarbles;
		System.out.println("Marbles she has left : " + hiltMarblesLeft);
		
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
		int totalMiles = 78;
		int milesDriven = 32;
		int milesLeft = totalMiles - milesDriven;
		System.out.println("Miles left : " + milesLeft);
        
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
		double morningSnow = 1.5;
		double eveningSnow = .75;
		double totalTimeSnow = morningSnow + eveningSnow;
		System.out.println("Total time shovelling snow : " + totalTimeSnow);
        
        /*    
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
		double oneHotDogCost = .50;
		double sixHotDogsCost = oneHotDogCost * 6;
		System.out.println("Cost of all hot dogs : " + sixHotDogsCost);
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
		double totalHiltMoney = .50;
		double pencilCost = .07;
		double totalPencils = totalHiltMoney / pencilCost;
		System.out.println("Pencils she can buy with 50 cents : " + totalPencils);
        
        /*    
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
		int hiltButterflies = 33;
		int orangeButterflies = 20;
		int redButterflies = hiltButterflies - orangeButterflies;
		System.out.println("Number of red butterflies : " + redButterflies);
        
        /*    
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
		double gaveClerk = 1.00;
		double candyCost = .54;
		double kateChange = gaveClerk - candyCost;
		System.out.println("Change Kate got back : " + kateChange);
        
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
		int treeNow = 13;
		int plantTrees = 12;
		int totalTrees = treeNow + plantTrees;
		System.out.println("Number of trees he will have : " + totalTrees);
        
        /*    
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
		int oneDay = 24;
		int twoDays = oneDay * 2;
		int seeGrandma = twoDays;
		System.out.println("Number of hours until she sees her grandma : " + seeGrandma);
        
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
		int cousins = 4;
		int oneCousinGum = 5;
		int totalGum = cousins * oneCousinGum;
		System.out.println("Total gum she will need : " + totalGum);
        
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
		double danHas = 3.00;
		double candyBarCost = 1.00;
		double danMoneyLeft = danHas - candyBarCost;
		System.out.println("Money Dan has left : " + danMoneyLeft);
        
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
        int numBoats = 5;
        int peoplePerBoat = 3;
        int totalPeopleOnBoats = numBoats * peoplePerBoat;
        System.out.println("Total people on boats : " + totalPeopleOnBoats);
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
        int totalLegos = 380;
        int lostLegos = 57;
        int legosLeft = totalLegos - lostLegos;
        System.out.println("Number of legos left : " + legosLeft);
            
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
        int muffinsNow = 35;
        int totalArthurMuffins = 83;
        int muffinsArthurNeeds = totalArthurMuffins - muffinsNow;
        System.out.println("Muffins Arthur has left to bake : " + muffinsArthurNeeds);
        
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
        int willyCrayons = 1400;
        int lucyCrayons = 290;
        int moreCrayons = willyCrayons - lucyCrayons;
        System.out.println("How many more crayons Willy has than Lucy : " + moreCrayons);
        
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
        int onePageStickers = 10;
        int twentyTwoPageStickers = onePageStickers * 22;
        System.out.println("Number of stickers on 22 pages : " + twentyTwoPageStickers);
        
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
        int cupcakes = 96;
        int children = 8;
        int cupcakesPerChild = cupcakes / children;
        System.out.println("Cupcakes per child : " + cupcakesPerChild);
        
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
        double totalGingerCookies = 47;
        double cookiesPerJar = 6;
        double cookiesLeft = totalGingerCookies % cookiesPerJar;
        System.out.println("Cookies not placed in a jar : " + cookiesLeft);
        
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
        int croissants = 59;
        int neighbors = 8;
        int croissantPerNeighbor = croissants % neighbors;
        System.out.println("Croissants left : " + croissantPerNeighbor);
        
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
        double totalMarianCookies = 276;
        double cookiesPerTray = 12;
        double traysNeeded = totalMarianCookies / cookiesPerTray;
        System.out.println("Number of trays needed : " + traysNeeded);
            
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
        double totalPretzels = 480;
        double pretzelsPerServing = 12;
        double totalPretzelsServings = totalPretzels / pretzelsPerServing;
        System.out.println("Pretzels servings needed to prepare : " + totalPretzelsServings);
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
        double totalLemonCC = 53;
        double lemonCCHome = 2;
        double lemonCCSheHasNow = totalLemonCC - lemonCCHome;
        double lemonCCPerBox = 3;
        double lemonCCBoxesGivenAway = lemonCCSheHasNow / lemonCCPerBox;
        System.out.println("Boxes with 3 lemon cupcakes given away : " + lemonCCBoxesGivenAway);
            
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
        double totalCarrots = 74;
        double numPeople = 12;
        double carrotsLeft = totalCarrots % numPeople;
        System.out.println("Number of carrots left uneaten : " + carrotsLeft);
        
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
        int teddyBears = 98;
        int teddyBearsPerShelf = 7;
        int shelves = teddyBears / teddyBearsPerShelf;
        System.out.println("Number of shelves filled : " + shelves);
        
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
        int oneAlbum = 20;
        int numPics = 480;
        int numAlbums = numPics / oneAlbum;
        System.out.println("Number of albums needed : " + numAlbums);
        
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
        double totalTCards = 94;
        double cardsPerBox = 8;
        double numBoxesFilled = totalTCards / cardsPerBox;
        System.out.println("Number of boxes filled : " + numBoxesFilled);
        double numTCardsLeft = totalTCards % cardsPerBox;
        System.out.println("Number of cards left : " + numTCardsLeft);
            
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
        int numBooks = 210;
        int numRepairedShelves = 10;
        int booksPerShelf = numBooks / numRepairedShelves;
        System.out.println("How many books each shelf contains : " + booksPerShelf);

        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
        double numCroissants = 17;
        double guests = 7;
        double croissantsToGuestsEqually = numCroissants / guests;
        System.out.println("How many croissants each guest will have : " + croissantsToGuestsEqually);
        int x = (5 / 2) * 4;
        System.out.println("answer : " + x); 
        double xx = 4.9;
        int y = (int)xx;
        System.out.println("A"+y) ;  
	}

}
