package com.techelevator;

public class KataRomanNumerals {
	public int nums(String roms){
		if(roms.equals("")){
			return 0;
		}
		
		int roman = 0;
		
		while(roms.contains("CM")){
			roman += 900;
			roms = roms.replaceFirst("CM", "");
		}
		
		while(roms.contains("CD")){
			roman += 400;
			roms = roms.replaceFirst("CD", "");
		}
		
		while(roms.contains("XC")){
			roman += 90;
			roms = roms.replaceFirst("XC", "");
		}
		
		while(roms.contains("XL")){
			roman += 40;
			roms = roms.replaceFirst("XL", "");
		}
		
		while(roms.contains("IX")){
			roman += 9;
			roms = roms.replaceFirst("IX", "");
		}
		
		while(roms.contains("IV")){
			roman += 4;
			roms = roms.replaceFirst("IV", "");
		}
		
		while(roms.contains("M")){
			roman += 1000;
			roms = roms.replaceFirst("M", "");
		}
		
		
		while(roms.contains("D")){
			roman += 500;
			roms = roms.replaceFirst("D", "");
		}
		
		while(roms.contains("C")){
			roman += 100;
			roms = roms.replaceFirst("C", "");
		}
		
		while(roms.contains("L")){
			roman += 50;
			roms = roms.replaceFirst("L", "");
		}
		
		while(roms.contains("X")){
			roman += 10;
			roms = roms.replaceFirst("X", "");
		}
		
		while(roms.contains("V")){
			roman += 5;
			roms = roms.replaceFirst("V", "");
		}
		
		while(roms.contains("I")){
			roman += 1;
			roms = roms.replaceFirst("I", "");
		}
	
		
		return roman;
	}
	
	public String rom(int num){
		if(num < 1 || num > 3000){
			return " ";
		}
		
		String numeral = "";
		
		while(num >= 1000){
			numeral += "M";
			num -= 1000;
		}
		
		while(num >= 900){
			numeral += "CM";
			num -= 900;
		}
		
		while(num >= 500){
			numeral += "D";
			num -= 500;
		}
		
		while(num >= 400){
			numeral += "CD";
			num -= 400;
		}
		
		while(num >= 100){
			numeral += "C";
			num -= 100;
		}
		
		while(num >= 90){
			numeral += "XC";
			num -= 90;
		}
		
		while(num >= 50){
			numeral += "L";
			num -= 50;
		}
		
		while(num >= 40){
			numeral += "XL";
			num -= 40;
		}
		
		while(num >= 10){
			numeral += "X";
			num -= 10;
		}
		
		while(num >= 9){
			numeral += "IX";
			num -= 9;
		}
		
		while(num >= 5){
			numeral += "V";
			num -= 5;
		}
		
		while(num >= 4){
			numeral += "IV";
			num -= 4;
		}
		
		while(num >= 1){
			numeral += "I";
			num -= 1;
		}
		
		return numeral;
	}
}
