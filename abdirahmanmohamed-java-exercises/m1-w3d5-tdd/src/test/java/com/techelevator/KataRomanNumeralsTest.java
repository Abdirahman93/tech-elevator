package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataRomanNumeralsTest {
	private KataRomanNumerals rom;
	private KataRomanNumerals num;
	@Before
	public void setup(){
		rom = new KataRomanNumerals();
		num = new KataRomanNumerals();
	}
	
	@Test
	public void more_than_3000(){
		String result = rom.rom(3001);
		Assert.assertEquals(" ", result);
	}
	
	@Test
	public void zero_or_less(){
		String result = rom.rom(0);
		Assert.assertEquals(" ", result);
	}
	
	@Test
	public void two_thousand(){
		String result = rom.rom(2000);
		Assert.assertEquals("MM", result);
	}
	
	@Test
	public void one_thousand(){
		String result = rom.rom(1000);
		Assert.assertEquals("M", result);
	}
	
	@Test
	public void nine_hundred(){
		String result = rom.rom(900);
		Assert.assertEquals("CM", result);
	}
	
	@Test
	public void five_hundred(){
		String result = rom.rom(500);
		Assert.assertEquals("D", result);
	}
	
	@Test
	public void four_hundred(){
		String result = rom.rom(400);
		Assert.assertEquals("CD", result);
	}
	
	@Test
	public void one_hundred(){
		String result = rom.rom(100);
		Assert.assertEquals("C", result);
	}
	
	@Test
	public void ninety(){
		String result = rom.rom(90);
		Assert.assertEquals("XC", result);
	}
	
	@Test
	public void fifty(){
		String result = rom.rom(50);
		Assert.assertEquals("L", result);
	}
	
	@Test
	public void forty(){
		String result = rom.rom(40);
		Assert.assertEquals("XL", result);
	}
	
	@Test
	public void ten(){
		String result = rom.rom(10);
		Assert.assertEquals("X", result);
	}
	
	@Test
	public void nine(){
		String result = rom.rom(9);
		Assert.assertEquals("IX", result);
	}
	
	@Test
	public void five(){
		String result = rom.rom(5);
		Assert.assertEquals("V", result);
	}
	
	@Test
	public void four(){
		String result = rom.rom(4);
		Assert.assertEquals("IV", result);
	}
	
	@Test
	public void two(){
		String result = rom.rom(2);
		Assert.assertEquals("II", result);
	}
	
	@Test
	public void one(){
		String result = rom.rom(1);
		Assert.assertEquals("I", result);
	}
	
	@Test
	public void rom_to_1(){
		int result = num.nums("I");
		Assert.assertEquals(1, result);
	}
	
	@Test
	public void rom_to_0(){
		int result = num.nums("");
		Assert.assertEquals(0, result);
	}
	
	@Test
	public void rom_to_4(){
		int result = num.nums("IV");
		Assert.assertEquals(4, result);
	}
	
	@Test
	public void rom_to_20(){
		int result = num.nums("XX");
		Assert.assertEquals(20, result);
	}
	
	@Test
	public void rom_to_100(){
		int result = num.nums("C");
		Assert.assertEquals(100, result);
	}
	
	@Test
	public void rom_to_900(){
		int result = num.nums("CM");
		Assert.assertEquals(900, result);
	}
	
	@Test
	public void rom_to_400(){
		int result = num.nums("CD");
		Assert.assertEquals(400, result);
	}
	
	@Test
	public void rom_to_500(){
		int result = num.nums("D");
		Assert.assertEquals(500, result);
	}
	
	@Test
	public void rom_to_554(){
		int result = num.nums("DLIV");
		Assert.assertEquals(554, result);
	}
	
	@Test
	public void rom_to_90(){
		int result = num.nums("XC");
		Assert.assertEquals(90, result);
	}
	
	@Test
	public void rom_to_50(){
		int result = num.nums("L");
		Assert.assertEquals(50, result);
	}
	
	@Test
	public void rom_to_40(){
		int result = num.nums("XL");
		Assert.assertEquals(40, result);
	}
}
