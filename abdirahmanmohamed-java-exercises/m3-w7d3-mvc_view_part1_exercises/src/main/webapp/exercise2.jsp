<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<title>Exercise 2 - Fibonacci 25</title>
<style>
li {
list-style-type: none;
}
</style>
</head>
<body>
<h1>Exercise 2 - Fibonacci 25</h1>
<ul>
<c:set var= "startingNumber" value="0" />
<c:set var = "nextNumber" value="1"/>
<c:forEach begin = "0" end ="26" var="count">
<c:set var="fibonacci" value="${nextNumber + startingNumber}"/>
<c:set var= "startingNumber" value="${nextNumber}" />
<c:set var = "nextNumber" value="${fibonacci}"/>
<li>${fibonacci}</li>
</c:forEach>
<%--
Add a list item (i.e. <li>) for each of the first 25 numbers in the Fibonacci sequence

            See exercise2-fibonacci.png for example output
         --%>
    </ul>
</body>
</html>