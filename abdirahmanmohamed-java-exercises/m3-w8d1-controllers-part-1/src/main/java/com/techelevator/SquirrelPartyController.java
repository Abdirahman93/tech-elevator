package com.techelevator;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SquirrelPartyController {
	@RequestMapping("/squirrelPartyInput")
	public String showSquirrelPartyInput() {
		return "squirrelPartyInput";
	}

	@RequestMapping("/squirrelPartyResult")
	public String showSquirrelPartyResult(HttpServletRequest res) {
		
		return "squirrelPartyResult";
	}

}
