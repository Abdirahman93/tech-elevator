package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LastTwoController {
	
	@RequestMapping("/lastTwoInput")
	public String showLastTwoInput(){
		return "lastTwoInput";
	}
	
	@RequestMapping("/lastTwoResult")
	public String showLastTwoResult(HttpServletRequest res){
		List<String> list = new ArrayList();
		list.add((res.getParameter("wOne")));
		list.add((res.getParameter("wTwo")));
		list.add((res.getParameter("wThree")));
		list.add((res.getParameter("wFour")));
		list.add((res.getParameter("wFive")));
		list.add((res.getParameter("wSix")));
		list.add((res.getParameter("wSeven")));
		list.add((res.getParameter("wEight")));
		list.add((res.getParameter("wNine")));
		list.add((res.getParameter("wTen")));
		
		
		for(int i = 0; i < list.size(); i++){
			String lastTwo = list.get(i);
			lastTwo= lastTwo.substring(0, lastTwo.length() - 2)
				    + lastTwo.charAt(lastTwo.length() - 1)
				    + lastTwo.charAt(lastTwo.length() - 2);
			list.set(i, lastTwo);	
		}
		
		res.setAttribute("lastTwoList", list);
		return "lastTwoResult";

	}
}
