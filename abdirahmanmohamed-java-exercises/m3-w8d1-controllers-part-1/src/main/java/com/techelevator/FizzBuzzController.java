package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FizzBuzzController {
	
	@RequestMapping("/fizzBuzzInput")
	public String showFizzBuzzInput(){
		return "fizzBuzzInput";
	}
	
	@RequestMapping("/fizzBuzzResult")
	public String showFizzBuzzResultTwo(HttpServletRequest res){
		List<Integer> fNum = new ArrayList();
		String firstNum = res.getParameter("firstNum");
		String secNum = res.getParameter("secNum");
		String altFizz = res.getParameter("altFizz");
		String altBuzz = res.getParameter("altBuzz");
	
		
		fNum.add(Integer.parseInt(res.getParameter("nOne")));
		fNum.add(Integer.parseInt(res.getParameter("nTwo")));
		fNum.add(Integer.parseInt(res.getParameter("nThree")));
		fNum.add(Integer.parseInt(res.getParameter("nFour")));
		fNum.add(Integer.parseInt(res.getParameter("nFive")));
		
		res.setAttribute("fizzList", fNum);

		return "fizzBuzzResult";
		
	
	}
	
	

}
