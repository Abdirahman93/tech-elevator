package com.techelevator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BabyLottoController {

	@RequestMapping("/babyLottoInput")
	public String showColorizedNameInput() {
		return "babyLottoInput";
	}

	@RequestMapping("/babyLottoResult")
	public String showColorizedNameResult(HttpServletRequest res) {
		List<lotto> ticket = new ArrayList<lotto>();
		List<String> list = new ArrayList();
		list.add((res.getParameter("t1")));
		list.add((res.getParameter("t2")));
		list.add((res.getParameter("t3")));
		list.add((res.getParameter("t4")));
		list.add((res.getParameter("t5")));
		for(String str : list){
		lotto lo = new lotto();
		lo.setTickets(str);
		String [] t1List = str.split(",");
		String t11 = t1List[0];
		String t12 = t1List[1];
		String t13 = t1List[2];
		if(t11.equals(t12) && t11.equals(t13)){
			lo.setColor("green");
			
		}else if(t11.equals(t12) || t11.equals(t13)|| t13.equals(t12)){
			lo.setColor("silver");
		}else{
			lo.setColor("black");
		}
		ticket.add(lo);
		}
		
		res.setAttribute("lottoList", ticket);
		return "babyLottoResult";
	}

}