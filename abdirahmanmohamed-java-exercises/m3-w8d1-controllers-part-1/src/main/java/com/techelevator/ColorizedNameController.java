package com.techelevator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ColorizedNameController {

	@RequestMapping("/colorizedNameInput")
	public String showColorizedNameInput() {
		return "colorizedNameInput";
	}

	@RequestMapping("/colorizedNameResult")
	public String showColorizedNameResult(HttpServletRequest res) {

		return "colorizedNameResult";
	}

}
