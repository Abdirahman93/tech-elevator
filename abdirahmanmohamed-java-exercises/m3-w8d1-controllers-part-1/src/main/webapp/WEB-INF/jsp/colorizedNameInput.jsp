<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<h1>Colorized Name Input</h1>
	<c:url var="formAction" value="/colorizedNameResult" />
	<form method="GET" action="${formAction}">
		<label for="firstName">First Name</label><br> <input type="text"
			name="firstName" /><br> <label for="lastName">Last Name</label><br>
		<input type="text" name="lastName" /><br> <br> <input
			type="checkBox" name="red">Red<br> <input
			type="checkBox" name="blue">Blue<br> <input
			type="checkBox" name="green">Green<br> <br>

		<input type="submit" value="Send It!">

	</form>
</body>

</html>