<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<h1>Baby Lotto</h1>
	<h2>Enter three comma delimited numbers for each ticket</h2>
	<c:url var="formAction" value="/babyLottoResult" />
	<form method="GET" action="${formAction}">
		<label for="t1">Ticket 1</label><br> 
		<input type="text" name="t1" /><br>
		
		<label for="t2">Ticket 2</label><br>
		<input type="text" name="t2" /><br> 
		
		<label for="t3">Ticket 3</label><br> 
		<input type="text" name="t3" /><br>
		
		<label for="t4">Ticket 4</label><br>
		<input type="text" name="t4" /><br>
		
		<label for="t5">Ticket 5</label><br>
		<input type="text" name="t5" /><br>
		
		<input type="submit" value="Send It!">

	</form>
</body>

</html>