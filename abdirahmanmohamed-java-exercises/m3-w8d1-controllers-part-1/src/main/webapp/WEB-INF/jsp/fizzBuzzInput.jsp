<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<h1>FizzBuzz Revisited</h1>
	<c:url var="formAction" value="/fizzBuzzResult" />
	<form method="GET" action="${formAction}">
		<label for="firstNum">Divisible By</label><br> 
		<input type="text" name="firstNum" /><br>
		
		<label for="secNum">Divisible By</label><br>
		<input type="text" name="secNum" /><br> 
		
		<label for="altFizz">Alternate Fizz</label><br> 
		<input type="text" name="altFizz" /><br>
		
		<label for="altBuzz">Alternate Buzz</label><br>
		<input type="text" name="altBuzz" /><br>
		
		<br></br>
		<label for="nOne">Number 1</label> <input type="text" name="nOne" /><br></br>
		<br><label for="nTwo">Number 2</label> <input type="text" name="nTwo" /><br></br>
		<br><label for="nThree">Number 3</label> <input type="text" name="nThree" /><br></br>
		<br><label for="nFour">Number 4</label> <input type="text" name="nFour" /><br></br>
		<br><label for="nFive">Number 5</label> <input type="text" name="nFive" /><br></br>
	
		

		<input type="submit" value="Send It!">

	</form>
</body>

</html>