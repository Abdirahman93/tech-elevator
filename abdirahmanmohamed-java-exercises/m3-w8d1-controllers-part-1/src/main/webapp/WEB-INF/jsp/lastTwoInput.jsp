<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<h1>Reverse Last Two Letters</h1>
	<c:url var="formAction" value="/lastTwoResult" />
	<form method="GET" action="${formAction}">

		<label for="wOne">Word 1</label> <input type="text" name="wOne" /><br></br>
		<br><label for="wTwo">Word 2</label> <input type="text" name="wTwo" /><br></br>
		<br><label for="wThree">Word 3</label> <input type="text" name="wThree" /><br></br>
		<br><label for="wFour">Word 4</label> <input type="text" name="wFour" /><br></br>
		<br><label for="wFive">Word 5</label> <input type="text" name="wFive" /><br></br>
		<br><label for="wSix">Word 6</label> <input type="text" name="wSix" /><br></br>
		<br><label for="wSeven">Word 7</label> <input type="text" name="wSeven" /><br></br>
		<br><label for="wEight">Word 8</label> <input type="text" name="wEight" /><br></br>
		<br><label for="wNine">Word 9</label> <input type="text" name="wNine" /><br></br>
		<br><label for="wTen">Word 10</label> <input type="text" name="wTen" /><br></br>
	
		

		<input type="submit" value="Send It!">

	</form>
</body>

</html>