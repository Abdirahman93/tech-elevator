<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<c:choose>
	<c:when test="${not empty param.weekend}">
	<h1>Great Party!!</h1>
	<img src = "img/happy-squirrel.png" alt = "Happy Squirrel">
	
	</c:when>
	<c:when test="${param.sNum > 39 and param.sNum < 61}">
	<h1>Great Party!!</h1>
	<img src = "img/happy-squirrel.png" alt = "Happy Squirrel">
	</c:when>
	<c:when test="${param.sNum < 40 or param.sNum > 60}">
	<h1>Ugh!!</h1>
	<img src = "img/sad-squirrel.png" alt = "Sad Squirrel">
	</c:when>
	</c:choose>
</body>
</html>