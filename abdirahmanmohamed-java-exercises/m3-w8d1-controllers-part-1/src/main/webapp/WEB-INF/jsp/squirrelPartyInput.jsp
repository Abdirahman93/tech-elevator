<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<h1>Squirrel Party</h1>
	<img src = "img/smoking-squirrel.png" alt= "Smoking Squirrel">
	<c:url var="formAction" value="/squirrelPartyResult" />
	<form method="GET" action="${formAction}">
		<label for="sNum">Number Of Squirrels</label><br> 
		<input type="text" name="sNum" /><br>
		
		<input type="checkBox" name="weekend">Is this the weekend?<br>
		
		<input type="submit" value="Send It!">

	</form>
</body>

</html>