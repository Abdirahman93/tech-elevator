<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<h1>FizzBuzz Revisited</h1>
	<p>Divisible By: ${param.firstNum }</p>
	<p>Divisible By: ${param.secNum }</p>
	<p>Divisible By Both: ${param.firstNum } and ${param.secNum }</p>
	
	<br>
	<p>Alternate Fizz: ${param.altFizz }</p>
	<p>Alternate Buzz: ${param.altBuzz }</p>
	
	<br>
	<c:forEach var="num" items="${fizzList}">
		<c:choose>
			<c:when test="${num % param.firstNum == 0 and num % param.secNum == 0 }">
				<p>${num} ${param.altFizz}${param.altBuzz }</p>
			</c:when>
			<c:when test="${num % param.firstNum == 0 }">
				<p>${num} ${param.altFizz}</p>
			</c:when>
			<c:when test="${num % param.secNum == 0 }">
				<p>${num} ${param.altBuzz}</p>
			</c:when>
			<c:when test="${num % param.firstNum != 0 and num % param.secNum != 0 }">
				<p>${num}</p>
				</c:when>
		</c:choose>
	</c:forEach>
	
	
</body>
</html>