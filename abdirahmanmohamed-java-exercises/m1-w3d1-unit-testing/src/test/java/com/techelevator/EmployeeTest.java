package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeTest {
	private Employee em;
	
	@Before
	public void setup(){
		em = new Employee(0, "Ab", "M", 100);
	}
	
	@Test
	public void employee_id(){
		//Act
		em.getEmployeeId();
		
		//Assert
		Assert.assertEquals(0,em.getEmployeeId());
	}
	
	@Test
	public void full_name(){
		//Act
		em.fullName();
		
		//Assert
		Assert.assertEquals("M, Ab", em.fullName());
	}
	
	@Test
	public void first_name(){
		//Act
		em.firstName();
		
		//Assert
		Assert.assertEquals("Ab", em.firstName());
	}
	
	@Test
	public void last_name(){
		//Act
		em.lastName();
		
		//Assert
		Assert.assertEquals("M", em.lastName());
	}
	
	@Test
	public void department(){
		//Act
		em.setDepartment("Oh");
		
		//Assert
		Assert.assertEquals("Oh",em.getDepartment());
	}
	
	@Test
	public void annual_salary(){
		//Act
		em.getAnnualSalary();
		
		//Assert
		Assert.assertEquals(100, em.getAnnualSalary(), 0.01);
	}
	
	@Test
	public void raise_salary(){
		em.RaiseSalary(5);
		
		Assert.assertEquals(105, em.getAnnualSalary(), 0.01);
	}
	
	
}
