package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SmartPhoneTest {
	private SmartPhone phone;
	
	@Before
	public void setup(){
		phone = new SmartPhone("1234567890","Verizon");
	}
	
	@Test
	public void smart_phone_initializes(){
		Assert.assertEquals("1234567890",phone.getPhoneNumber());
		Assert.assertEquals("Verizon", phone.getCarrier());
		Assert.assertEquals(100,phone.getBatteryCharge());
		Assert.assertFalse(phone.isOnCall());
	}
	
	@Test
	public void set_operating_system(){
		//Act
		phone.setOperatingSystem("ios");
		
		//Assert
		Assert.assertEquals("ios", phone.getOperatingSystem());
	}
	
	@Test
	public void call(){
		//Act
		boolean call = phone.Call("phoneNumberToCall", 1);
		
		//Assert
		Assert.assertTrue(phone.isOnCall());
		
	}
	
	@Test
	public void answer_phone(){
		//Act
		 phone.answerPhone();
		 
		 //Assert
		 Assert.assertTrue(phone.isOnCall());
	}
	
	@Test
	public void hang_up(){
		//Act
		phone.HangUp();
		
		//Assert
		Assert.assertFalse(phone.isOnCall());
	}
	
	@Test
	public void recharge_battery(){
		//Act
		phone.RechargeBattery();
		
		//Assert
		Assert.assertEquals(phone.getBatteryCharge(),95);
	}
}
