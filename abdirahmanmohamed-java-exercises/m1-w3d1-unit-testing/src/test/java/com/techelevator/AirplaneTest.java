package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class AirplaneTest {
	private Airplane myPlane;
	
	@Before
	public void setup(){
		myPlane = new Airplane(10, 100, "123456");
	}
	
	@Test
	public void number_of_booked_seats_to_with(){
		//Assert
		Assert.assertEquals(0, myPlane.getBookedCoachSeats());
		Assert.assertEquals(0, myPlane.getBookedFirstClassSeats());
		Assert.assertEquals(10, myPlane.getTotalFirstClassSeats());
		Assert.assertEquals(6, myPlane.getPlaneNumber().length());
		Assert.assertNotNull(myPlane.getPlaneNumber());
		Assert.assertEquals(100, myPlane.getTotalCoachSeats());
	}
	
	@Test
	public void can_reserve_first_class(){
		//Act
		boolean reserve = myPlane.reserve(true, 7);
		
		//Assert
		Assert.assertTrue("True", reserve);
		Assert.assertEquals(3, myPlane.getAvailableFirstClassSeats());
	}
	
	@Test
	public void can_reserve_coach(){
		//Act
		boolean reserve = myPlane.reserve(false, 99);
		
		//Assert
		Assert.assertTrue("Reserve", reserve);
		Assert.assertEquals(1, myPlane.getAvailableCoachSeats());
		
		
	}
	
	@Test
	public void cannot_reserve_at_all(){
		//Act
		boolean reserve = myPlane.reserve(false, 110);
		
		//Assert
		Assert.assertFalse(reserve);
		Assert.assertEquals(100, myPlane.getAvailableCoachSeats());
	}

	@Test
	public void number_of_seats_over_first_class(){
		//Act
		boolean reserve = myPlane.reserve(true, 50);
		
		//Assert
		Assert.assertFalse(reserve);
		Assert.assertEquals(10, myPlane.getAvailableFirstClassSeats());
	}
	
}
