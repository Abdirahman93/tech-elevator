package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ElevatorTest {
	private Elevator myEle;
    
    @Before
    public void setup(){
		myEle = new Elevator(0,10);
	}
    
    @Test
    public void elavator_initializes(){
    	Assert.assertEquals(0,myEle.getShaftNumber());
    	Assert.assertEquals(10,myEle.getNumberOfLevels());
    	Assert.assertEquals(1,myEle.getCurrentLevel());
    	Assert.assertTrue("Door is Open",myEle.isDoorOpen());
    	Assert.assertFalse("Elevator is moving", myEle.isMoving()); 	
    }
    
    @Test
    public void opens_door_when_elevator_is_not_moving(){
    	//Act
    	myEle.OpenDoor();
    	
    	//Assert
    	Assert.assertTrue("Door is open when elevator is not moving",true );
    	
    }
    
    @Test
    
    public void close_door_when_elevator_is_moving(){
    	//Act
    	myEle.CloseDoor();
    	
    	//Assert
    	Assert.assertFalse("Door is open when elevator is not moving",myEle.isDoorOpen() );
    	
    }
    
    @Test
    
    public void elevator_going_up(){
    	// Arrange
    	myEle.CloseDoor();
    	
    	//Act
    	boolean goUp = myEle.goUp(7);
    	
    	//Assert
    	Assert.assertTrue("Should be able to go up", goUp);
    	Assert.assertEquals(7,myEle.getCurrentLevel());
    	Assert.assertFalse("Door closed", myEle.isDoorOpen());
    	
    }
    
    @Test
    public void cannot_move_if_it_is_floor_10(){
    	myEle.goUp(10);
    	//Act
    	boolean goUp = myEle.goUp(11);
    	
    	//Asserts
    	Assert.assertFalse(goUp);
    }
   
    @Test
    public void elevator_cannot_go_higher(){
    	//Act
    	boolean goUp = myEle.goUp(11);
    	
    	//Assert
    	Assert.assertEquals(10, myEle.getNumberOfLevels());
    }
    
    @Test
    
    public void elevator_going_down(){
		myEle.CloseDoor();
		myEle.goUp(5);
	
    	//Act
    	boolean goDown = myEle.goDown(2);
    	
    	//Assert
    	Assert.assertTrue("should be able to go down", goDown);
    	Assert.assertEquals(3, myEle.getCurrentLevel());
    	Assert.assertFalse("Door closed", myEle.isDoorOpen());
    }

    @Test
    public void elevator_cannot_go_lower(){
    	//Act
    	boolean goDown = myEle.goDown(0);
	
    	//Assert
    	Assert.assertEquals(1, myEle.getCurrentLevel());
}

    @Test
    public void elevator_cannot_go_lower_than_1(){
    	myEle.goDown(1);
    	//Act
    	boolean goDown = myEle.goDown(12);
	
    	//Assert
    	Assert.assertFalse(goDown);
}
    
}
