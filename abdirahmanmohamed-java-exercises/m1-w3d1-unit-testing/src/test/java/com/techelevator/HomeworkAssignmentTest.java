package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HomeworkAssignmentTest {
	private HomeworkAssignment hw;
	
	@Before
	public void setup(){
		hw = new HomeworkAssignment(100);
	}
	
	@Test
	public void total_marks(){
		hw.setTotalMarks(80);
		//Assert
		Assert.assertEquals(80,hw.getTotalMarks());
	}
	
	@Test
	public void possible_marks(){
		Assert.assertEquals(100,hw.getPossibleMarks());
	}
	
	@Test
	public void submitter_name(){
		//Act
		hw.setSubmitterName("abdi");
		
		//Assert
		Assert.assertEquals("abdi", hw.getSubmitterName());
	}
	
	@Test
	public void get_a(){
		hw.setTotalMarks(91);
		
		
		//Act
		String grade = hw.getLetterGrade();
		
		// Assert
		Assert.assertEquals("A", grade);
		
	}
	
	@Test
	public void get_b(){
		hw.setTotalMarks(84);
		
		
		//Act
		String grade = hw.getLetterGrade();
		
		// Assert
		Assert.assertEquals("B", grade);
		
	}@Test
	public void get_c(){
		hw.setTotalMarks(77);
		
		
		//Act
		String grade = hw.getLetterGrade();
		
		// Assert
		Assert.assertEquals("C", grade);
		
	}
	
	@Test
	public void get_d(){
		hw.setTotalMarks(66);
		
		
		//Act
		String grade = hw.getLetterGrade();
		
		// Assert
		Assert.assertEquals("D", grade);
		
	}
	
	@Test
	public void get_f(){
		hw.setTotalMarks(55);
		
		
		//Act
		String grade = hw.getLetterGrade();
		
		// Assert
		Assert.assertEquals("F", grade);
		
	}
}
