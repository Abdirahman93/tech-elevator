package com.techelevator;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;



@Controller 
@SessionAttributes("favorites")
public class HelloController {
	
	@RequestMapping(path ="/", method=RequestMethod.GET)
	public String displayPage1(){
		
		return "page1";
	}
	
	
	
	@RequestMapping(path="/page1", method=RequestMethod.POST)
	public String page1(
												 @RequestParam String color, 
												 ModelMap model) {
		
		favorites fav = new favorites();
		fav.setColor(color);
		
		model.addAttribute("favorites", fav);
		
		return "redirect:page2";
	}
	
	@RequestMapping(path="/page2", method=RequestMethod.GET)
	public String displaypage2() {
		return "page2";
	}
	
	
	@RequestMapping(path="/page2", method=RequestMethod.POST)
	public String page2( @RequestParam String food, 
												 ModelMap model) {
		
		favorites fav =(favorites)model.get("favorites");
		fav.setFood(food);
		
		return "redirect:/page3";
	}
	
	@RequestMapping(path="/page3", method=RequestMethod.GET)
	public String displaypage3() {
		return "page3";
	}
	
	@RequestMapping(path="/page3", method=RequestMethod.POST)
	public String page3(
												 @RequestParam String season, 
												 ModelMap model) {
		
		favorites fav =(favorites)model.get("favorites");
		fav.setSeason(season);
		
		return "redirect:/summary";
	}
	
	@RequestMapping(path="/summary", method=RequestMethod.GET)
	public String displaySummary() {
		return "summary";
	}
	
}
