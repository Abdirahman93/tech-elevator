<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<h1>Favorite Things Exercise</h1>
<table>
	<tr>
		<th>Favorite Color:</th>
		<td><c:out value="${favorites.color}" /></td>
	</tr>
	<tr>
		<th>Favorite Food:</th>
		<td><c:out value="${favorites.food}" /></td>
	</tr>
	<tr>
		<th>Favorite Season:</th>
		<td><c:out value="${favorites.season}" /></td>
	</tr>
</table>