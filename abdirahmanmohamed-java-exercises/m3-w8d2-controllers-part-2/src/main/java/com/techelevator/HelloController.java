package com.techelevator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;

@Controller 
public class HelloController {
	@Autowired
	private ReviewDao reviewDao;
	private Review re;
	@RequestMapping(path = "/", method=RequestMethod.GET)
	public String showHomePage(HttpServletRequest request) {
		// Get Reviews from Database
		List<Review> reviewList = reviewDao.getAllReviews();
		request.setAttribute("reviewList", reviewList);
		
		return "homePage";
	}
	
	@RequestMapping("/submitReview")
	public String showSubmitReview(HttpServletRequest request){
		return "submitReview";
	}
	
	@RequestMapping(path= "/submitReview", method=RequestMethod.POST)
	public String showSubmitReviewPartOne(HttpServletRequest request){
		Review rev = new Review();
		String userName= request.getParameter("userName");
		int rating = Integer.parseInt(request.getParameter("rating"));
		String title =request.getParameter("reviewTitle");
		String text = request.getParameter("reviewText");
		
		
		rev.setUsername(userName);
		rev.setRating(rating);
		rev.setText(text);
		rev.setTitle(title);
		rev.setDateSubmitted(LocalDateTime.now());
		
		reviewDao.save(rev);
		
		return "redirect:/";
	}
	
	
}
