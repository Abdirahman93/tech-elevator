<!DOCTYPE html>
<html>
	<head>
		<title>Create Review</title>
	</head>
	<body>
		<div id="main_content">
		
			<form action="submitReview" method="POST">
				<div class="formGroup">
					<label for="userName">User Name: </label>
					<input type="text" name="userName" id="userName" />
				</div>
				<div class="formGroup">
					<label for="rating"> Rating: </label>
					<input type="text" name="rating" id="rating" />
				</div>	
				<div class="formGroup">		
					<label for="reviewTitle">Review Title</label>
					<input type="text" name="reviewTitle" id="reviewTitle" />
				</div>	
				<div class="formGroup">		
					<label for="reviewText">Review Text: </label>
					<input type="text" name="reviewText" id="reviewText" />
				</div>
				
				<div class="formGroup">
					<input type="submit" value="Submit" />
				</div>		
			</form>
		</div>
	</body>
</html>