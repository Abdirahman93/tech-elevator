<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Squirrel Parties for Dummies</title>
</head>

<body>
	<h1>Squirrel Parties for Dummies!</h1>
	<img
		src="file:///Users/amohamed/Development/workspaces/abdirahmanmohamed-java-exercises/m3-w8d2-controllers-part-2/etc/forDummies.png"
		alt="pic">
	<p>Marketing Copy</p>
	<nav>
	<ul>
	<li><a href="submitReview">Create New Review</a></li>
	</ul>
	</nav>
	<c:forEach var="review" items="${reviewList }">
		<h4>${review.title}(${review.username})</h4>
		<p>${review.text}</p>
		<img
			src="file:///Users/amohamed/Development/workspaces/abdirahmanmohamed-java-exercises/m3-w8d2-controllers-part-2/etc/img/${review.rating}-star.png"
			style="width: 5em; height: 0.9em;">

		<hr>
		
	</c:forEach>
</body>
</html>