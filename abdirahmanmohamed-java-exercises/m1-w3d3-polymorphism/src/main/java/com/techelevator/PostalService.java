package com.techelevator;

public class PostalService implements DeliveryDriver{
	private double firstClass;
	private double secondClass;
	private double thirdClass;
	public int ouncesPerPound = 16;

	private int postalClass;
	
	public PostalService(int postalClass){
		this.postalClass = postalClass;
	}
	
	@Override
	public String getName() {
		if(postalClass == 1) {
			return "Postal Service (1st Class)";
		} else if(postalClass == 2) {
			return "Postal Service (2nd Class)";
		} else {
			return "Postal Service (3rd Class)";
		}
	}
	
	@Override
	public double calculateRate(int distance, int weight) {
		if(weight <= 48 ){
			firstClass = 0.195*distance;
			secondClass = 0.0195*distance;
			thirdClass = 0.0150*distance;
		} else if(weight >= 64 && weight <= 128){
			firstClass = 0.250*distance;
			secondClass = 0.0450*distance;
			thirdClass = 0.0160*distance;
		} else if(weight >= 144){
			firstClass = 0.500*distance;
			secondClass = 0.0500*distance;
			thirdClass = 0.0170*distance;
		} else if(weight >=0 && weight <=2 ){
			firstClass = 0.035*distance;
			secondClass = 0.0035*distance;
			thirdClass = 0.0020*distance;
		} else if(weight >=3 && weight <=8 ){
			firstClass = 0.040*distance;
			secondClass = 0.0040*distance;
			thirdClass = 0.0024*distance;
		} else if(weight >=9 && weight <=15 ){
			firstClass = 0.047*distance;
			secondClass = 0.0047*distance;
			thirdClass = 0.0024*distance;
		}
			
		if(postalClass == 1) {
			return firstClass;
		} else if(postalClass == 2) {
			return secondClass;
		} else {
			return thirdClass;
		}
	}
	

}
