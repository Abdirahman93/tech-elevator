package com.techelevator;

public class Car implements Vehicle {
	
	@Override
	public double calculateToll(int distance) {
		double toll = distance * 0.020;
		return toll;
	}

	@Override
	public String getName() {
		return "Car";
	}
	
}
