package com.techelevator;

public class SPU implements DeliveryDriver {
	public enum Method {
		FOUR_DAY_GROUND,
		TWO_DAY_GROUND,
		NEXT_DAY
	}
	
	private Method method;
	
	public SPU(Method method) {
		this.method = method;
	}
	
	@Override
	public double calculateRate(int distance, int weight) {
		if(method == Method.FOUR_DAY_GROUND){
			return (weight * 0.0050) * distance;
		} else if(method == Method.TWO_DAY_GROUND){
			return (weight * 0.050) * distance;
		} else {
			return (weight * 0.075) * distance;
		}
	}

	@Override
	public String getName() {
		if(method == Method.FOUR_DAY_GROUND){
			return "SPU (4-day ground)";
		} else if(method == Method.TWO_DAY_GROUND){
			return "SPU (2-day business)";
		} else {
			return "SPU (next-day)";
		}
	}

}
