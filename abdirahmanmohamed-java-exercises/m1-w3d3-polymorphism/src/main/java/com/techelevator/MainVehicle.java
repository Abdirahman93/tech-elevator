package com.techelevator;

import java.util.Scanner;

import com.techelevator.Truck.Method;

public class MainVehicle {
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
		
		Vehicle[] vehicles = new Vehicle[]{
				new Car(),
				new CarWithTrailer(),
				new Tank(),
				new Truck(Method.Four),
				new Truck(Method.Six),
				new Truck(Method.Eightplus)
		};

		System.out.println();
		System.out.println("Distance       	car        		      $ cost");
		System.out.println("--------------------------------------------------------");
		for(Vehicle car: vehicles){
			int distance = (int)(Math.random()*230) + 10;
			System.out.print(Integer.toString(distance) + "\t"+"\t");
			System.out.println(String.format("%1$-30s$%2$s", car.getName(),car.calculateToll(distance)));
		}
	}
}
