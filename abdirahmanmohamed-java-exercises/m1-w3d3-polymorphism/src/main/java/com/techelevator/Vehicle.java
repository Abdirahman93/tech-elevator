package com.techelevator;

public interface Vehicle {
	public String getName();
	public double calculateToll(int distance);
}
