package com.techelevator;

public class CarWithTrailer implements Vehicle{

	@Override
	public double calculateToll(int distance) {
		double toll = distance * 0.020 +1.00;
		return toll;
	
	}

	@Override
	public String getName() {
		return "Car with trailer";
	}
	
}
