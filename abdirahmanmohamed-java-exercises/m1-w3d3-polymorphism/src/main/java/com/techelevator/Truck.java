package com.techelevator;

public class Truck implements Vehicle {
	private Method method;
	public enum Method {
		Four,
		Six,
		Eightplus	
	}
	
	public Truck(Method method){
		this.method = method;
	}
	
	@Override
	public double calculateToll(int distance) {
		if(method == Method.Four){
			double toll = 0.040 * distance;
			return toll;
		}else if(method == Method.Six){
			double toll = 0.045 *distance;
			return toll;
		}else{
			double toll = 0.048* distance;
			return toll;
		}
	}

	@Override
	public String getName() {
		if(method == Method.Four){
			return "Truck (4 Axles)";
		}else if(method == Method.Six){
			return "Truck (6 Axles)";
		}else{
			return "Truck (8+ Axles)";
		}
	}
	
	
}
