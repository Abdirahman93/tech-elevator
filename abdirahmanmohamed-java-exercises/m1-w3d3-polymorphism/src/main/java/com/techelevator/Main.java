package com.techelevator;

import java.util.Scanner;

import com.techelevator.SPU.Method;

public class Main {
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
		
		DeliveryDriver[] drivers = new DeliveryDriver[] {
				new PostalService(1),
				new PostalService(2),
				new PostalService(3),
				new Fexed(),
				new SPU(Method.FOUR_DAY_GROUND),
				new SPU(Method.TWO_DAY_GROUND),
				new SPU(Method.NEXT_DAY)
			};
		
		System.out.println("Please enter the weight of the package: ");
		
		int weight = input.nextInt();
		input.nextLine();
		System.out.println("Is the measurement in ounces or pounds?");
		
		String weightFormat = input.nextLine();
		
		if(weightFormat.equals("ounces")){
			weight =weight ;
			
		}else{
			weight = weight*16;
		}
		System.out.println("What distance will it be traveling to? ");
		
		int distance = input.nextInt();
		input.nextLine();
		
		System.out.println();
		System.out.println("Delivery Method               $ cost");
		System.out.println("------------------------------------");
		for(DeliveryDriver eachDriver: drivers){
			System.out.println(String.format("%1$-30s$%2$s", eachDriver.getName(), eachDriver.calculateRate(distance, weight)));
		}
			
	}
}
