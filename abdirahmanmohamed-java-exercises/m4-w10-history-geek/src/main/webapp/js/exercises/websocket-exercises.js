﻿$(document).ready(function () {

var socket = new SockJS("ws-connect");
var connection = Stomp.over(socket);


var errorCallback = function(error) {
      // display the error's message header:
      console.log(error);
 };
 
 
 var successCallback = function(frame) {
        console.log("Connected: "+ frame); //notifies that you are connected to chat
        
        connection.subscribe('/topic/members', function(members) {
            members = JSON.parse(members.body);
            $('#members ul').empty();
            console.log(members);
            for(var i=0; i < members.length; i++) {
                var item = $('<li>').text(members[i]);
            $('#members ul').append(item);
        }
    });
        connection.subscribe('/topic/chat', function(data) { //subscribes you to chat window
            var dataObject = JSON.parse(data.body); //shows message body
            console.log(dataObject);
     var userName = dataObject.userName;
     var message = dataObject.message;
     var am = true;
     var hours = dataObject.sentDate.hour;
     if(hours > 12) {
         hours = hours - 12;
         am = false;
     }
     var minutes = dataObject.sentDate.minute;
     if(minutes < 10) {
         minutes = '0' + minutes;
     }
     var time = " "+ hours + ':' + minutes + ' ' + (am ? 'AM' : 'PM');
     var sentMessage = $('<p class = "message"></p>').text(message).html('<span class = "username">' + userName + '</span>' + "" +'<span class = "time">' + time + '</span><br>' +message);
     $('#history').append(sentMessage);
        });
 };
    
    connection.connect({}, successCallback, errorCallback);
     $("#chatForm button").on('click', function() {  //when 'send' button is clicked...
        var messageContents = $('#chatForm textArea').val(); //...tell what to send...
            $('#chatForm textarea').val("");
    
            var chatMessage = {
                    message: messageContents
            };
        connection.send("/app/chat", {}, JSON.stringify(chatMessage)); //...send message to server
        return false;
    });
});