﻿/// <reference path="../jquery-3.1.1.js" />
/// <reference path="../jquery.validate.js" />

$(document).ready(function () {

    // Email address - required
    // Email address - .gov only (BONUS)
    // Password - required, length 8 or more
    // ConfirmPassword
	
	$(".login").validate({
		
		debug: false,
		rules: {
			email: {
				email: true,
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			email: {
				email: "Please enter a valid email",
				required: "Email is required!"
			
			}
		},
		errorClass: "field-validation-error",
		validClass : "valid"
	})
	
	$(".registration").validate({
		
		debug: false,
		rules: {
			email: {
				email: true,
				required: true,
				govEmail: true,
			},
			password: {
				required: true,         
                minlength: 8,           
              
			},
			confirmPassword: {
				required: true,
				equalTo:"#password"
			}
		},
		messages: {
			email: {
				email: "Please enter a valid email",
				required: "Email is required!"
			
			},
			password: {
				required: "Please enter a password!",
				minLength: "Password must be eight characters",
			
			},
				
			confirmPassword: {
				required:"Re-enter your password",
				equalTo:"Passwords do not match!"
			}
		},
		errorClass: "field-validation-error",
		validClass : "valid"
	})
	
	$("#checkout").validate({
		
		debug: false,
		rules: {
			BillingAddress1: {
				required: true
			},
			BillingCity: {
				required: true
			},
			BillingState: {
				required: true
			},
			BillingPostalCode: {
				required: true
			},
			ShippingAddress1: {
				required: true
			},
			ShippingCity: {
				required: true
			},
			ShippingState: {
				required: true
			},
			ShippingPostalCode: {
				required: true
			},
			ShippingType: {
				required:true
			},
			NameOnCard: {
				required: true
			},
			CreditCardNumber: {
				required: true,
				minLength: 10
			},
			ExpirationDate: {
				required: true,
				
			}
		},
		messages: {
			BillingAddress1: {
				required: "Billing Address is required!"
			},
			BillingCity: {
				required: "Billing City is required!"
			},
			BillingState: {
				required: "Billing State is required!"
			},
			BillingPostalCode: {
				required: "Billing Zipcode is required!"
			},
			ShippingAddress1: {
				required: "Shipping Address is required!"
			},
			ShippingCity: {
				required: "Shipping City is required!"
			},
			ShippingState: {
				required: "Shipping State is required!"
			},
			ShippingPostalCode: {
				required: "Shipping Zipcode is required!"
			},
			ShippingType: {
				required: "Shipping info is required"
			},
			NameOnCard: {
				required: "Name is required"
			},
			CreditCardNumber: {
				required: "Credit card number is required",
				minLength: "Valid credit card is required"
			},
			ExpirationDate: {
				required: "Expiration date is required"
			}
		},
		errorClass: "field-validation-error",
		validClass : "valid"
	})
	
	
	$.validator.addMethod("govEmail", function (value, index) {
	    return value.toLowerCase().endsWith("@gov.com"); 
	}, "Please enter a gov.com email");
});