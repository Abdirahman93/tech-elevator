﻿/// <reference path="../jquery-3.1.1.js" />

$(document).ready(
		function() {

			var sameShipping = $("#SameShipping");
			sameShipping.on('change', function(event) {
				if (sameShipping.is(':checked')) {
					$('#ShippingAddress1').val($('#BillingAddress1').val());
					$('#ShippingAddress2').val($('#BillingAddress2').val());
					$('#ShippingCity').val($('#BillingCity').val());
					$('#ShippingState').val($('#BillingState').val());
					$('#ShippingPostalCode').val($('#BillingPostalCode').val())
				}
			});

			var cost = $('input[name="ShippingType"]');
			cost.on('change', function(event) {

				var data = parseFloat(($(this).attr('data-cost')));
				var total = parseFloat(($('#grandtotal .price').html()
						.substring(1)));
				$('#shipping .price').html(data);
				$('#grandtotal .price').html("$" + (total + data));

			});
			
			$(document).keydown(function (e) {


			switch (e.which) {
			
			case 37: //left
				$('.ship').prev().addClass("ship");
				$('.ship').next().removeClass("ship");
				if ($(".ship").hasClass("treasure")) {
					alert("You have won!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("pirate")) {
					alert("Pirates took your ship!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("iceberg")) {
					alert("You sunk like the titanic!");
					$(".ship").removeClass("ship");
				}
				break;
			
			case 38: //up
				$('.ship').parent().prev().children().eq($('.ship').index())
						.addClass("ship");
				$('.ship').parent().next().children().eq($('.ship').index())
						.removeClass("ship");
				if ($(".ship").hasClass("treasure")) {
					alert("You have won!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("pirate")) {
					alert("Pirates took your ship!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("iceberg")) {
					alert("You sunk like the titanic!");
					$(".ship").removeClass("ship");
				}
				break;
			case 39: //right 
				$('.ship').next().addClass("ship");
				$('.ship').prev().removeClass("ship");
				if ($(".ship").hasClass("treasure")) {
					alert("You have won!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("pirate")) {
					alert("Pirates took your ship!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("iceberg")) {
					alert("You sunk like the titanic!");
					$(".ship").removeClass("ship");
				}
				break;
			case 40: //down 
				var ship = $('.ship');
				$('.ship').parent().next().children().eq($('.ship').index())
				
						.addClass("ship");
				ship.removeClass("ship");
				if ($(".ship").hasClass("treasure")) {
					alert("You have won!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("pirate")) {
					alert("Pirates took your ship!");
					$(".ship").removeClass("ship");
				} else if ($(".ship").hasClass("iceberg")) {
					alert("You sunk like the titanic!");
					$(".ship").removeClass("ship");
				}
				break;
			}
			var gameReset = $("#btnRestart");
			gameReset.on("click", function(event) {
				$("h2").text("Start Over!");
				$(".ship").removeClass("ship");
				$("td#row_0_column_0.gamecell").addClass("ship");
			});

		});
});