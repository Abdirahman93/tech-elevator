package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuizMaker {

	public static void main(String[] args) {
		File file = getFileFromUser();
		List<Question> questions = new ArrayList<Question>();

		try (Scanner quiz = new Scanner(file)) {
			while (quiz.hasNextLine()) {
				String word = quiz.nextLine();
				String[] parts = word.split("\\|");
				String prompt = parts[0];
				String correctAnswer = "";
				List<String> answers = new ArrayList<String>();

				for (int i = 1; i < parts.length; i++) {
					String answer = parts[i];
					if (answer.endsWith("*")) {
						answer = answer.substring(0, answer.length() - 1);
						correctAnswer = answer;
					}

					answers.add(answer);
				}

				Question question = new Question(prompt, answers, correctAnswer);
				questions.add(question);
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Show the questions

		int count = 0;
		for (Question question : questions) {
			boolean isValid = false;
			while (!isValid) {
				System.out.println(question.getPrompt());

				for (int i = 0; i < question.getAnswers().size(); i++) {
					System.out.println(Integer.toString(i + 1) + ") " + question.getAnswers().get(i));
				}

				// Take user input and tell them if they are correct
				System.out.println("Please choose answer value: ");
				Scanner input = new Scanner(System.in);
				try {
					int answer = input.nextInt();
					input.nextLine();
					if (answer <= 0 || answer > 4) {
						System.out.println("Your answer: " + answer);
						System.out.println("Invalid answer!");
					} else if (question.getAnswers().get(answer - 1).equals(question.getCorrectAnswer())) {
						System.out.println("Your answer: " + answer);
						System.out.println("Correct answer!");
						isValid = true;
						count++;
					} else {
						System.out.println("Your answer: " + answer);
						System.out.println("Wrong answer!");
						isValid = true;
					}
				} catch (Exception e) {
					System.out.println("You did it wrong...");
				}
			}
			System.out.println("You got " + count + " answer(s) correct out of the total " + questions.size() + " questions asked");

			System.out.println();
		}

	}

	private static File getFileFromUser() {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the path to the file: ");
		String path = input.nextLine();

		File file = new File(path);
		if (!file.exists()) {
			System.out.print("That file doesn't exist");
			System.exit(1);
		} else if (!file.isFile()) {
			System.out.println(path + " is a directory.");
			System.exit(1);
		}
		return file;

	}

}
