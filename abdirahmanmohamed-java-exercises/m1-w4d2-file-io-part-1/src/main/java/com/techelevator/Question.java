package com.techelevator;

import java.util.List;

public class Question {
	private String prompt;
	private List<String> answers;
	private String correctAnswer;
	
	public Question(String prompt, List<String> answers, String correctAnswer) {
		this.prompt = prompt;
		this.answers = answers;
		this.correctAnswer = correctAnswer;
	}
	
	public String getPrompt() {
		return prompt;
	}
	
	public List<String> getAnswers() {
		return answers;
	}
	
	public String getCorrectAnswer() {
		return correctAnswer;
	}
}
