package com.techelevator;

public class Television {
	private boolean isOn;
	private int currentChannel = 3;
	private int currentVolume = 2;
	
	public boolean isOn(){
		return isOn;
	}
	
	public int getCurrentChannel(){
		return currentChannel;
	}
	
	public int getCurrentVolume(){
		return currentVolume;
	}
	
	public void turnOff(){
		this.isOn = false;
	}
	
	public void turnOn(){
		this.isOn = true;
	} 
	
	public void changeChannel(int newChannel){
		if(this.isOn && newChannel >= 3 && newChannel <= 18){
			this.currentChannel = newChannel;
		}
	}
	
	public void channelUp(){
		if(this.isOn){
			this.currentChannel++;
			if(currentChannel > 18){
				currentChannel = 3;
			}
		}
	}
	
	public void channelDown(){
		if(isOn){
			currentChannel -= 1;
			if(currentChannel < 3) {
				currentChannel = 18;
			}
		}
	}
	
	public void raiseVolume(){
		if(this.isOn){
			this.currentVolume++;
		}
	}
	
	public void lowerVolume(){
		if(this.isOn && currentVolume > 0){
			this.currentVolume--;
		}
	}
}
