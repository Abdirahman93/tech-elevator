package com.techelevator;

public class Elevator {
	private int currentFloor;
	private int numberOfFloors;
	private boolean isDoorOpen;
	
	public int getCurrentFloor(){
		return currentFloor;
	}
	
	public int getNumberOfFloors(){
		return numberOfFloors;
	}
	
	public boolean isDoorOpen(){
		return isDoorOpen;
	}
	
	public Elevator(int totalNumberOfFloors){
		this.numberOfFloors = totalNumberOfFloors;
		this.currentFloor = 1;
	}
	
	public void openDoor(){
		this.isDoorOpen = true;
	}
	
	public void closeDoor(){
		this.isDoorOpen = false;
	}
	
	public void goUp(int desiredFloor){
		if(!isDoorOpen && desiredFloor <= this.numberOfFloors && desiredFloor > currentFloor){
			this.currentFloor = desiredFloor;
		}
	}
	
	public void goDown(int desiredFloor){
		if(!isDoorOpen && desiredFloor >= 1 && desiredFloor < currentFloor){
			this.currentFloor = desiredFloor;
		}
	}
}
