package com.techelevator;

public class HomeworkAssignment {
	private int totalMarks;
	private int possibleMarks;
	private String submitterName;
	
	public int getTotalMarks(){
		return totalMarks;
	}
	public void setTotalMarks(int totalMarks){
		this.totalMarks = totalMarks;
	}
	
	public int getPossibleMarks(){
		return possibleMarks;
	}
	
	public String getSubmitterName(){
		return submitterName;
	}
	public void setSubmitterName(String submitterName){
		this.submitterName = submitterName;
	}
	
	public HomeworkAssignment(int possibleMarks){
		this.possibleMarks = possibleMarks;
	}
	
	public String getLetterGrade(){
		double percentage = (double) totalMarks / (double) possibleMarks *100;
		if(percentage >= 90){
			return "A";
		}else if(percentage >= 80 && percentage < 90){
			return "B";
		}else if(percentage >= 70 && percentage < 80 ){
			return "C";
		}else if( percentage >= 60 && percentage < 70){
			return "D";
		}
		return "F";
	}

}
