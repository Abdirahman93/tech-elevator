package com.techelevator;

import java.util.Scanner;

/*
Write a command line program which prompts the user for a series of decimal integer values  
and displays each decimal value as itself and its binary equivalent

$ DecimalToBinary 

Please enter in a decimal number: 460

460 in binary is 111001100
*/
public class DecimalToBinary {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter a decimal number:");
		
		int decimalNum = input.nextInt();
		input.nextLine();
			int powerOf2 = 1;
			while(powerOf2 < decimalNum){
				powerOf2 *= 2;
			}
			
			while(powerOf2 > 0) {
				// Decide if we print 1 or 0
				if(powerOf2 > decimalNum){
					System.out.print(0);
				}else{
					System.out.print(1);
					decimalNum -= powerOf2;
				}
				powerOf2 /= 2;
			}
//		System.out.println(Integer.toBinaryString(decimalNum));
	}

}


	


