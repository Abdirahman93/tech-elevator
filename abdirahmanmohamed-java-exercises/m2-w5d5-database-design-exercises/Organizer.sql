CREATE TABLE employee (
	employeeID serial,
	projectID int,
	departmentID int,
	jobTitle varchar(64) NOT NULL,
	firstName varchar(64) NOT NULL,
	lastName varchar(64) NOT NULL,
	gender varchar(6),
	birthdate date,
	hireDate date NOT NULL,
	CONSTRAINT pk_employee_employeeID PRIMARY KEY (employeeID)
	CONSTRAINT fk_employee_departmentID FOREIGN KEY (departmentID) REFERENCES department(departmentID)
	CONSTRAINT fk_employee_projectID FOREIGN KEY (projectID) REFERENCES project(projectID)

);

CREATE TABLE project(
	projectID serial,
	projectName varchar(64) NOT NULL,
	startDate date NOT NULL,,
	CONSTRAINT pk_project_projectID PRIMARY KEY (projectID),
);

CREATE TABLE department(
	departmentID serial,
	departmentName varchar(64) NOT NULL,
	CONSTRAINT pk_department_departmentID PRIMARY KEY (departmentID),

);


INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Managar', 'bob', 'one', 'male', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Cleaner', 'bob', 'two', 'male', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Cleaner', 'bob', 'jane', 'female', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('assistant-manager', 'bob', 'three', 'male', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Cleaner', 'bob', 'seven', 'male', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Cleaner', 'bob', 'four', 'male', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Cleaner', 'bob', 'five', 'female', '11/23/1938', '10/10/2010');

INSERT INTO employee (jobtitle, firstname, lastname, gender, birthdate, hiredate)
Values ('Cleaner', 'bob', 'eight', 'female', '11/23/1938', '10/10/2010');

INSERT INTO department(departmentname, employeeid)
VALUES ('Sanitation', 1), ('Cleaners', 2), ('House', 3),  ('Sanitation', 4), ('Cleaners', 5), ('House', 6), ('Sanitation', 7), ('Cleaners', 8);

INSERT INTO project (projectname, startdate, employeeid)
VALUES ('Pro1', '11/23/1234', 1), ('Pro1', '11/23/1234', 2), ('Pro2', '11/23/1934', 3), ('Pro2', '11/23/1934', 4), ('Pro3', '11/23/1994', 5), ('Pro3', '11/23/1994', 6), ('Pro4', '11/23/2000', 7), ('Pro4', '11/23/2000', 8);