package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileSplitter {

	public static void main(String[] args) {
		int fileNumber = 1;
		File file = getFileFromUser();
		int line = getLinePerFile();
		String saveFileName = getBaseFileToSaveItTo();
		String saveFile = saveFileName + fileNumber + ".txt";
		PrintWriter writer = null;
		try(Scanner fileInput = new Scanner(file)){
				while(fileInput.hasNextLine()){
					int lineNumber = 1;
					saveFile = saveFileName + ++fileNumber + ".txt";
					writer = new PrintWriter(saveFile);
					while(lineNumber <= line && fileInput.hasNextLine()){
						String newLine = fileInput.nextLine();
						writer.println(newLine);
						lineNumber++;
					}
					writer.flush();
				}
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			writer.close();
		}
	}
	
	private static String getBaseFileToSaveItTo(){
		Scanner input = new Scanner(System.in);
		System.out.println("Choose base file to save it to");
		String saveFile = input.nextLine();
		return saveFile;
	}
	
	private static int getLinePerFile(){
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the maximum number of lines that should appear in each outputFile");
		int line = input.nextInt();
		input.nextLine();
		return line;
	}
	
	private static File getFileFromUser(){
		Scanner input = new Scanner(System.in);
			System.out.println("Please enter the path to the file: ");
			String path = input.nextLine();
			
			File file = new File(path);
			if(!file.exists()){
				System.out.print("That file doesn't exist");
				System.exit(1);
			}else if(!file.isFile()){
				System.out.println(path + " is a directory.");
				System.exit(1);
			}
			return file;
	}
}
