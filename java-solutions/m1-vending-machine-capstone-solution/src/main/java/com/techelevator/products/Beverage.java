package com.techelevator.products;

import com.techelevator.DollarAmount;

public class Beverage extends Product {
	public Beverage(String slot, String name, DollarAmount price, int quantity) {
		super(slot, name, price, quantity);
	}

	@Override
	public String getConsumeMessage() {
		return "Glug, Glug! Yum!";
	}
}
