package com.techelevator.products;

import com.techelevator.DollarAmount;

public class Candy extends Product {
	public Candy(String slot, String name, DollarAmount price, int quantity) {
		super(slot, name, price, quantity);
	}

	@Override
	public String getConsumeMessage() {
		return "Munch, Munch! Yum!";
	}
}
