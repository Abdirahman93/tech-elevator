package com.techelevator.products;

import com.techelevator.DollarAmount;

public abstract class Product {
	private String slot;
	private String name;
	private DollarAmount price;
	private int quantity;
	
	public Product(String slot, String name, DollarAmount price, int quantity) {
		this.slot = slot;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	
	public abstract String getConsumeMessage();

	public String getSlot() {
		return slot;
	}

	public String getName() {
		return name;
	}

	public DollarAmount getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Product)) {
			return false;
		} else {
			return slot.equals(((Product)other).slot);
		}
	}
}
