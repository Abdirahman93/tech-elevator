package com.techelevator.products;

import com.techelevator.DollarAmount;

public class Chips extends Product {
	public Chips(String slot, String name, DollarAmount price, int quantity) {
		super(slot, name, price, quantity);
	}

	@Override
	public String getConsumeMessage() {
		return "Crunch, Crunch! Yum!";
	}
}
