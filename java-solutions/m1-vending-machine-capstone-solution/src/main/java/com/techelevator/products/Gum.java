package com.techelevator.products;

import com.techelevator.DollarAmount;

public class Gum extends Product {
	public Gum(String slot, String name, DollarAmount price, int quantity) {
		super(slot, name, price, quantity);
	}

	@Override
	public String getConsumeMessage() {
		return "Chew, Chew! Yum!";
	}
}
