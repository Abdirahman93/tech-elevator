package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import com.techelevator.products.Product;
import com.techelevator.view.Menu;

public class VendingMachineCLI {

	private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "Display Vending Machine Items";
	private static final String MAIN_MENU_OPTION_PURCHASE = "Purchase";
	private static final String[] MAIN_MENU_OPTIONS = { MAIN_MENU_OPTION_DISPLAY_ITEMS,
													   MAIN_MENU_OPTION_PURCHASE };
	
	private static final String PURCHASE_MENU_FEED_MONEY = "Feed Money";
	private static final String PURCHASE_MENU_SELECT_PRODUCT = "Select Product";
	private static final String PURCHASE_MENU_FINISH_TRANSACTION = "Finish Transaction";
	private static final String[] PURCHASE_MENU_OPTIONS = { PURCHASE_MENU_FEED_MONEY,
															PURCHASE_MENU_SELECT_PRODUCT,
															PURCHASE_MENU_FINISH_TRANSACTION };
	
	private Menu menu;
	private Inventory inventory;
	private DollarAmount balance;
	
	private List<Product> cart = new ArrayList<Product>();
	
	public VendingMachineCLI(Menu menu) {
		this.menu = menu;
		this.inventory = new Inventory();
		balance = new DollarAmount(0);
	}
	
	public void run() {
		while(true) {
			String choice = (String)menu.getChoiceFromOptions(MAIN_MENU_OPTIONS);

			if(choice.equals(MAIN_MENU_OPTION_DISPLAY_ITEMS)) {
				menu.displayProducts(inventory.getProducts());
			} else if(choice.equals(MAIN_MENU_OPTION_PURCHASE)) {
				purchaseMenu();
			}
		}
	}
	
	private void purchaseMenu() {
		while(true) {
			String choice = (String)menu.getChoiceFromOptions(PURCHASE_MENU_OPTIONS, "Current Money Provided: " + balance);
			
			if(choice.equals(PURCHASE_MENU_FEED_MONEY)) {
				feedMoney();
			} else if(choice.equals(PURCHASE_MENU_SELECT_PRODUCT)) {
				chooseProduct();
			} else if(choice.equals(PURCHASE_MENU_FINISH_TRANSACTION)) {
				finishTransaction();
				return;
			}
		}
	}
	
	private void feedMoney() {
		DollarAmount moneyInput = menu.feedMoney();
		balance = balance.plus(moneyInput);
		
		// TODO: Log feed money
	}
	
	private void chooseProduct() {
		Product product = menu.chooseProduct(inventory.getProducts());
		
		if(product.getQuantity() == 0) {
			System.out.println("Sorry, this item is sold out");
		} else if(balance.isLessThan(product.getPrice())) {
			System.out.println("Sorry, you don't have enough money");
		} else {
			balance = balance.minus(product.getPrice());
			System.out.println("Enjoy your " + product.getName());
			
			cart.add(product);
			
			// TODO: Log purchase
		}
	}
	
	private void finishTransaction() {
		int pennies = balance.getDollars() * 100 + balance.getCents();
		int quarters = pennies / 25;
		pennies %= 25;
		int dimes = pennies / 10;
		pennies %= 10;
		int nickles = pennies / 5;
		pennies %= 5;
		
		balance = new DollarAmount(0);
		
		System.out.println(String.format("Your change is %d quarters, %d dimes, %d nickels and %d pennies", quarters, dimes, nickles, pennies));
		
		// TODO: Log change
		
		for(Product product : cart) {
			System.out.println(product.getConsumeMessage());
		}
		
		cart.clear();
	}
}