package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import com.techelevator.products.Beverage;
import com.techelevator.products.Candy;
import com.techelevator.products.Chips;
import com.techelevator.products.Gum;
import com.techelevator.products.Product;

public class Inventory {
	private Map<String, Product> products = new TreeMap<String, Product>();

	public Inventory() {
		try(Scanner fileInput = new Scanner(new File("vendingmachine.csv"))) {
			while(fileInput.hasNextLine()) {
				String line = fileInput.nextLine();
				String[] parts = line.split("\\|");

				String slot = parts[0];
				String name = parts[1];
				DollarAmount price = DollarAmount.parseDollarAmount(parts[2]);

				if(slot.startsWith("A")) {
					products.put(slot, new Chips(slot, name, price, 5));
				} else if(slot.startsWith("B")) {
					products.put(slot, new Candy(slot, name, price, 5));
				} else if(slot.startsWith("C")) {
					products.put(slot, new Beverage(slot, name, price, 5));
				} else if(slot.startsWith("D")) {
					products.put(slot, new Gum(slot, name, price, 5));
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Product getProduct(String slot) {
		return products.get(slot);
	}
	
	public Collection<Product> getProducts() {
		return products.values();
	}
}
