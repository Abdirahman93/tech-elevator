package com.techelevator.view;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Scanner;

import com.techelevator.DollarAmount;
import com.techelevator.products.Product;

public class Menu {

	private PrintWriter out;
	private Scanner in;

	public Menu(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);
	}
	
	public void displayProducts(Collection<Product> products) {
		for(Product product : products) {
			String quantity = product.getQuantity() == 0 ? "SOLD OUT" : Integer.toString(product.getQuantity());
			out.println(String.format("%-3s%-18s%6s %s", product.getSlot(), product.getName(), product.getPrice(), quantity));
		}
	}

	public Object getChoiceFromOptions(Object[] options) {
		return getChoiceFromOptions(options, null);
	}
	
	public Object getChoiceFromOptions(Object[] options, String footer) {
		Object choice = null;
		while(choice == null) {
			displayMenuOptions(options, footer);
			choice = getChoiceFromUserInput(options);
		}
		return choice;
	}
	
	public DollarAmount feedMoney() {
		DollarAmount money = null;
		while(money == null) {
			out.println("\nPlease enter a whole dollar value (e.g. $1, $2, $5, $10)");
			out.flush();
			
			String userInput = in.nextLine();
			
			try{
				int value = Integer.parseInt(userInput);
				if(value == 1 || value == 2 || value == 5 || value == 10) {
					money = new DollarAmount(value * 100);
				} else {
					throw new NumberFormatException();
				}
			} catch(NumberFormatException ime) {
				out.println("\n*** "+userInput+" is not a valid dollar amount ***\n");
			}
		}
		return money;
	}
	
	public Product chooseProduct(Collection<Product> products) {
		Product choice = null;
		while(choice == null) {
			displayProducts(products);
			out.print("\nPlease choose an option >>> ");
			out.flush();
			
			choice = getProductChoice(products);
		}
		return choice;
	}

	private Object getChoiceFromUserInput(Object[] options) {
		Object choice = null;
		String userInput = in.nextLine();
		try {
			int selectedOption = Integer.valueOf(userInput);
			if(selectedOption > 0 && selectedOption <= options.length) {
				choice = options[selectedOption - 1];
			}
		} catch(NumberFormatException e) {
			// eat the exception, an error message will be displayed below since choice will be null
		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
	}

	private void displayMenuOptions(Object[] options, String footer) {
		out.println();
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") "+options[i]);
		}
		
		if(footer != null) {
			out.println(footer);
		}
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}
	
	private Product getProductChoice(Collection<Product> products) {
		String userInput = in.nextLine();
		for(Product product : products) {
			if(product.getSlot().equals(userInput)) {
				return product;
			}
		}
		
		out.println("\n*** "+userInput+" is not a valid option ***\n");
		return null;
	}
}