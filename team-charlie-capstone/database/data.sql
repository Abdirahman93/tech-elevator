-- *****************************************************************************
-- This script contains INSERT statements for populating tables with seed data
-- *****************************************************************************

BEGIN;

INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (1, 'teacher', 'Albus', 'Dumbledore', 'headmaster@hogwarts.edu', 'password');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (2, 'teacher', 'Charles', 'Xavier', 'professorx@xavierschool.edu', 'hunter2');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (3, 'student', 'John', 'Connor', 'saviorofthefuture@terminator.edu', 'hastalavista');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (4, 'parent', 'Sarah', 'Connor', 'thesarahconnor@terminator.edu', 'kylereese');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (5, 'student', 'Barron', 'Trump', 'thecyber@whitehouse.gov', 'ineveraskedforthis');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (6, 'parent', 'Donald', 'Trump', 'potus@whitehouse.gov', 'hillarysemails');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (7, 'parent', 'Melania', 'Trump', 'flotus@whitehouse.gov', 'imissnewyork');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (8, 'student', 'Raphael', 'Turtle', 'redmask@sewers.ny', 'angrydaggers');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (9, 'student', 'Donatello', 'Turtle', 'bluemask@sewers.ny', '2swordzzz');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (10, 'student', 'Leonardo', 'Turtle', 'purplemask@sewers.ny', 'bigstick4u');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (11, 'student', 'Michaelangelo', 'Turtle', 'orangemask@sewers.ny', 'nunchucksnsuch');
INSERT INTO appuser (user_id, role, first_name, last_name, email, password) VALUES (12, 'parent', 'Master', 'Splinter', 'kungfurat@sewers.ny', 'pizzarat2012');

SELECT setval(pg_get_serial_sequence('appuser', 'user_id'), 12);

INSERT INTO parent_student (parent_id, student_id) VALUES (4, 3);
INSERT INTO parent_student (parent_id, student_id) VALUES (7, 5);
INSERT INTO parent_student (parent_id, student_id) VALUES (6, 5);
INSERT INTO parent_student (parent_id, student_id) VALUES (12, 8);
INSERT INTO parent_student (parent_id, student_id) VALUES (12, 9);
INSERT INTO parent_student (parent_id, student_id) VALUES (12, 10);
INSERT INTO parent_student (parent_id, student_id) VALUES (12, 11);

INSERT INTO teacher_student (teacher_id, student_id) VALUES (1, 3);
INSERT INTO teacher_student (teacher_id, student_id) VALUES (1, 5);
INSERT INTO teacher_student (teacher_id, student_id) VALUES (1, 8);
INSERT INTO teacher_student (teacher_id, student_id) VALUES (2, 8);
INSERT INTO teacher_student (teacher_id, student_id) VALUES (2, 9);
INSERT INTO teacher_student (teacher_id, student_id) VALUES (2, 10);
INSERT INTO teacher_student (teacher_id, student_id) VALUES (2, 11);

INSERT INTO homework_assignment (homework_assignment_id, teacher_id) VALUES (1, 1);
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (1, 1, 'tf', 'True or False: Harry Potter is the boy who lived.', null);
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (1, 2, 'mcma', 'Which of the following are Batman villains? Choose all that apply.', '(A) Bane (B) The Joker (C) The Vulture (D) Howard the Duck');
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (1, 3, 'essay', 'In 50 words or less, explain why Dumbledore is the best teacher at this school.', null);


INSERT INTO homework_assignment (homework_assignment_id, teacher_id) VALUES (2, 2);
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 4, 'essay', 'What is your name?', null);
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 5, 'essay', 'What is your quest?', null);
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 6, 'essay', 'What is your favorite color?', null);
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 7, 'mcoa', 'What is the capital of Assyria?', '(A) Constantinople (B) Sydney (C) Agrabah (D) Assur');
INSERT INTO homework_questions (homework_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 8, 'essay', 'What is the average air speed of an unladen swallow?', null);

SELECT setval(pg_get_serial_sequence('homework_assignment', 'homework_assignment_id'), 2);
SELECT setval(pg_get_serial_sequence('homework_questions', 'question_number'), 8);

INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (1, 1, 3, 'true', null);
INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (1, 2, 3, 'a,b', null);
INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (1, 3, 3, 'Because he fought the law and won.', null);

INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 4, 11, null, null);
INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 5, 11, null, null);
INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 6, 11, null, null);
INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 7, 11, 'D', null);
INSERT INTO homework_response (homework_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 8, 11, 'African or European?', null);

INSERT INTO quiz_assignment (quiz_assignment_id, teacher_id) VALUES (1, 1);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (1, 1, 'essay', 'How much wood could a woodchuck chuck?', null);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (1, 2, 'essay', 'Who is the captain of the Milennium Falcon?', null);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (1, 3, 'mcoa', 'What was the name of the first Robin that Batman trained?', '(A) Jason Todd (B) Dick Grayson (C) Tim Drake (D) Damien Wayne');

INSERT INTO quiz_assignment (quiz_assignment_id, teacher_id) VALUES (2, 2);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 4, 'essay', 'Describe the common behavioral traits of non-Newtonian fluid.', null);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 5, 'essay', 'What is the sum of the interior angles of a traingle in non-Euclidean space?', null);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 6, 'essay', 'List the Seven Deadly Sins.', null);
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 7, 'mcoa', 'To whom does the Infinity Gauntlet truly belong?', '(A) Thanos (B) No one (C) The people (D) Those who created it');
INSERT INTO quiz_questions (quiz_assignment_id, question_number, question_type, question_text1, question_text2) VALUES (2, 8, 'mcoa', 'What is the most widely-consumed Italian food in the U.S.?', '(A) Pizza (B) Lamb (C) Bowtie Pasta (D) Stromboli');

SELECT setval(pg_get_serial_sequence('quiz_assignment', 'quiz_assignment_id'), 2);
SELECT setval(pg_get_serial_sequence('quiz_questions', 'question_number'), 8);

INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (1, 1, 3, '3.14', null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (1, 2, 3, 'Han Solo', null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (1, 3, 3, 'B', null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 4, 11, null, null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 5, 11, null, null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 6, 11, null, null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 7, 11, null, null);
INSERT INTO quiz_response (quiz_assignment_id, question_number, student_id, student_response, grade) VALUES (2, 8, 11, 'A', null);

INSERT INTO behavior_report (behavior_report_id, teacher_id, student_id, issued, report_text, positive) VALUES (1, 1, 3, now(), 'He gets all of his work done in a timely fashion and is very polite in class', true);
INSERT INTO behavior_report (behavior_report_id, teacher_id, student_id, issued, report_text, positive) VALUES (2, 2, 11, now(), 'He has yet to complete anything and is generally rude and disruptive', false);

SELECT setval(pg_get_serial_sequence('behavior_report', 'behavior_report_id'), 2);

INSERT INTO resources (resources_id, teacher_id, link_content, text_content, file_content) VALUES (1, 1, null, 'Now is the winter of our discontent made glorious summer by this son of York.', null);
INSERT INTO resources (resources_id, teacher_id, link_content, text_content, file_content) VALUES (2, 1, null, 'It was the best of times, it was the worst of times', null);
INSERT INTO resources (resources_id, teacher_id, link_content, text_content, file_content) VALUES (3, 2, 'https://www.google.com/', null, null);

SELECT setval(pg_get_serial_sequence('resources', 'resources_id'), 3);

COMMIT;