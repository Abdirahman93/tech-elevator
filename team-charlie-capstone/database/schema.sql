-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;

Create Table appuser (
		user_id serial not null,
		role varchar(64) not null,
		first_name varchar(64) not null,
		last_name varchar(64) not null,
		email varchar(128) not null,
		password varchar(128) not null,
		
		constraint pk_user_id primary key (user_id)
);

Create Table parent_student (
	parent_id integer not null,
	student_id integer not null,
	
	constraint pk_parent_student_parent_id_student_id primary key (parent_id, student_id),
	constraint fk_parent_id foreign key (parent_id) references appuser(user_id),
	constraint fk_student_id foreign key (student_id) references appuser(user_id)
);

Create Table teacher_student (
	teacher_id integer not null,
	student_id integer not null,
	
	constraint pk_teacher_student_teacher_id_student_id primary key (teacher_id, student_id),
	constraint fk_teacher_id foreign key (teacher_id) references appuser(user_id),
	constraint fk_student_id foreign key (student_id) references appuser(user_id)
);

Create Table homework_assignment (
	homework_assignment_id serial not null,
	teacher_id integer not null,
	
	constraint pk_homework_assignment_id primary key (homework_assignment_id),
	constraint fk_teacher_id foreign key (teacher_id) references appuser(user_id)
);

Create Table homework_questions (
	homework_assignment_id integer not null,
	question_number serial not null,
	question_type varchar(8) not null,
	question_text1 varchar(256) not null,
	question_text2 varchar(256),
	
	constraint fk_homework_questions_homework_assigment_id foreign key (homework_assignment_id) references homework_assignment(homework_assignment_id)
);

Create Table homework_response (
	homework_assignment_id integer not null,
	question_number integer not null,
	student_id integer not null,
	student_response varchar(1028),
	grade varchar(10),
	
	constraint fk_homework_assignment_id foreign key (homework_assignment_id) references homework_assignment(homework_assignment_id),
	constraint fk_student_id foreign key (student_id) references appuser(user_id)
);

Create Table quiz_assignment (
	quiz_assignment_id serial not null,
	teacher_id integer not null,
	
	constraint pk_quiz_assignment_id primary key (quiz_assignment_id),
	constraint fk_teacher_id foreign key (teacher_id) references appuser(user_id)
);

Create Table quiz_questions (
	quiz_assignment_id integer not null,
	question_number serial not null,
	question_type varchar(8) not null,
	question_text1 varchar(256) not null,
	question_text2 varchar(256),
	
	constraint fk_quiz_questions_quiz_assigment_id foreign key (quiz_assignment_id) references quiz_assignment(quiz_assignment_id)
);

Create Table quiz_response (
	quiz_assignment_id integer not null,
	question_number integer not null,
	student_id integer not null,
	student_response varchar(1028),
	grade varchar(10),
	
	constraint fk_quiz_assignment_id foreign key (quiz_assignment_id) references quiz_assignment(quiz_assignment_id),
	constraint fk_student_id foreign key (student_id) references appuser(user_id)
);

Create Table behavior_report (
	behavior_report_id serial not null,
	teacher_id integer not null,
	student_id integer not null,
	issued timestamp not null,
	report_text varchar(256) not null,
	positive boolean DEFAULT false not null,
	
	constraint pk_behavior_report_id primary key (behavior_report_id),
	constraint fk_teacher_id foreign key (teacher_id) references appuser(user_id),
	constraint fk_student_id foreign key (student_id) references appuser(user_id)
);

Create Table resources (
	resources_id serial not null,
	teacher_id integer not null,
	link_content varchar(1028),
	text_content varchar(1028),
	file_content varchar(1028),
	
	constraint pk_resources_id primary key (resources_id), 
	constraint fk_teacher_id foreign key (teacher_id) references appuser(user_id)
);

COMMIT;