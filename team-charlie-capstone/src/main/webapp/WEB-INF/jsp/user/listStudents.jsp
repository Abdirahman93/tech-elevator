<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />


<c:choose><c:when test="${currentUser.role == 'teacher'}">
<div id="Account">
<h2>View Students</h2>

<c:forEach var="student" items="${studentList}">

	<c:url var="listStudents" value="/user/bindAccounts/${student.id}"/>
	 <a href="${listStudents}"> ${student.firstName} ${student.lastName} </a>
	 <br>
	
	
</c:forEach> 
</div>
</c:when></c:choose>


<c:import url="/WEB-INF/jsp/common/footer.jsp" />