<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<c:choose><c:when test="${currentUser.role == 'student'}">
<div id="Account">
<h2>View Assignments</h2>

<c:forEach var="assignment" items="${assignments}">

	 <a href="submitHomework/${assignment.assignmentId}">Homework Assignment ${assignment.assignmentId }</a>
	
	
</c:forEach> 
</div>
</c:when></c:choose>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />