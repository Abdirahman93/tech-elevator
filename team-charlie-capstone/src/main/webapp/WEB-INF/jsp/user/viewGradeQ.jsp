<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<c:choose><c:when test="${currentUser.role == 'teacher'}">
<div id="Account">
<h2>View Assignments</h2>

<c:forEach var="assignment" items="${assignments}">
	<c:url var="gradeQuizUrl" value="/user/gradeQuiz/${id}/${assignment.quizId}"/>
	 <a href="${gradeQuizUrl}">Quiz ${assignment.quizId }</a>
	
	
</c:forEach> 
</div>
</c:when></c:choose>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />