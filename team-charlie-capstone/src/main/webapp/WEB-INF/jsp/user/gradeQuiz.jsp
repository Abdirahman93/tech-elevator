<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
>
<c:url var="formAction0" value="/user/gradeQuiz/${id}/${assignmentId}" />
	<form action="${formAction0}" method="POST">
<c:choose>

	<c:when test="${currentUser.role == 'teacher'}">
	<div class="Account">
		<h2>Grade Quiz</h2>
		
		<c:forEach var="response" items="${responses}">
		<c:forEach var="question" items="${questions}">
		<c:if test="${question.questionNumber == response.qNum }">
			<input type="hidden" name="qNum" value="${response.qNum}"/>
				<c:out value="${question.text1}" />
				<br>
				<c:out value="${question.text2}" />
				<br>
				<strong>Student Response:</strong>
				<br>
				<c:out value="${response.response }" />
				<input type="hidden" name="response" value="${response.response}" />
				<br><br>
				<div class="form-group">
					<label for="LinkResource">Correct/Incorrect </label> <input type="text"
						id="text" name="grade" class="form-control" />
				</div>
				</c:if>
			</c:forEach>
		</c:forEach>
		</div>
	</c:when>

</c:choose>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />