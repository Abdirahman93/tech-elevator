/// <reference path="../jquery-3.1.1.js" />
/// <reference path="../jquery.validate.js" />

$(document).ready(function () {

   
	
	$("#login #form0").validate({
		
		debug: false,
		rules: {
			email: {
				email: true,
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			email: {
				email: "Please enter a valid email",
				required: "Email is required!"
			
			},
			password: {
				required: "Password is required"
			}
		},
		errorClass: "field-validation-error",
		validClass : "valid"
	})
	
	$(".createUser").validate({
		
		debug: false,
		rules: {
			email: {
				email: true,
				required: true,
			},
			firstName: {
				required: true,         
                       
              
			},
			lastName: {
				required: true,
				
			}
		},
		messages: {
			email: {
				email: "Please enter a valid email",
				required: "Email is required!"
			
			},
			firstName: {
				required: "Please enter a first name!",
				
			
			},
				
			lastName: {
				required:"Please enter a first name!",
				
			}
		},
		errorClass: "field-validation-error",
		validClass : "valid"
	})
	

});