package com.techelevator;
 
 import java.util.ArrayList;
 import java.util.List;
 
 import javax.sql.DataSource;
 
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.jdbc.core.JdbcTemplate;
 import org.springframework.jdbc.support.rowset.SqlRowSet;
 import org.springframework.stereotype.Component;
 
 @Component
 public class JdbcQAssessmentDao implements QAssessmentDao{
 		private JdbcTemplate jdbcTemplate;
 
 		@Autowired
 		public JdbcQAssessmentDao(DataSource dataSource) {
 			this.jdbcTemplate = new JdbcTemplate(dataSource);
 		}
 	
 		@Override
 		public List<QAssessment> studentGetAllQuiz(long studentId) {
 			List<QAssessment> allQuiz = new ArrayList<QAssessment>();
 			String sqlStudentGetAllQuiz = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, quiz_assignment.quiz_assignment_id, quiz_assignment.teacher_id "+
 											  "FROM quiz_assignment "+
 											  "JOIN appuser ON quiz_assignment.teacher_id = appuser.user_id "+
 											  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
 											  "WHERE teacher_student.student_id = ? ORDER BY quiz_assignment.teacher_id ";
 			SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentGetAllQuiz, studentId);
 			while (result.next()) {
 				QAssessment view = mapRowToAssessment(result);
 				allQuiz.add(view);
 			}
 			return allQuiz;
 		}
 		
 		@Override
 		public List<QAssessment> teacherGetAllQuiz(long teacherId) {
 			List<QAssessment> allQuiz = new ArrayList<QAssessment>();
 			String sqlTeacherGetAllHomework = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, quiz_assignment.quiz_assignment_id "+
 											  "FROM quiz_assignment "+
 											  "JOIN appuser ON quiz_assignment.teacher_id = appuser.user_id "+
 											  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
 											  "WHERE teacher_student.teacher_id = ? ORDER BY quiz_assignment.teacher_id ";
 			SqlRowSet result = jdbcTemplate.queryForRowSet(sqlTeacherGetAllHomework, teacherId);
 			while (result.next()) {
 				QAssessment view = mapRowToAssessment(result);
 				allQuiz.add(view);
 			}
 			return allQuiz;
 		}
 		
		@Override
		public List<QAssessment> teacherGetAllStudentQuizes(long studentId, long teacherId) {
			List<QAssessment> allQuiz = new ArrayList<QAssessment>();
			String sqlStudentGetAllQuiz = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, quiz_assignment.quiz_assignment_id, quiz_assignment.teacher_id "+
											  "FROM quiz_assignment "+
											  "JOIN appuser ON quiz_assignment.teacher_id = appuser.user_id "+
											  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
											  "WHERE teacher_student.student_id = ? AND teacher_student.teacher_id= ? ORDER BY quiz_assignment.teacher_id ";
			SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentGetAllQuiz, studentId, teacherId);
			while (result.next()) {
				QAssessment view = mapRowToAssessment(result);
				allQuiz.add(view);
			}
			return allQuiz;
		}
		
 		private QAssessment mapRowToAssessment(SqlRowSet results) {
 			QAssessment theAssessment = new QAssessment();
 			theAssessment.setQuizId(results.getInt("quiz_assignment_id"));
 			theAssessment.setTeacherId(results.getInt("teacher_id"));
 			return theAssessment;
 		
 		}

		@Override
		public void createNewQuizAssignment(long teacherId) {
			String sqlCreateNewQuiz = "INSERT INTO quiz_assignment (teacher_id) " +
					  "VALUES (?)";
			jdbcTemplate.update(sqlCreateNewQuiz, teacherId);
		}

		@Override
		public int getMostRecentQuizId() {
			int quizId = 0;
			String sqlGetMostRecentQuizId = "SELECT quiz_assignment_id "+
										 		"FROM quiz_assignment "+
										 		"ORDER BY quiz_assignment_id DESC "+
										 		"LIMIT 1";
			SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetMostRecentQuizId);
			if (result.next()) {
				quizId = result.getInt("quiz_assignment_id");
			}
			return quizId;
		}
 }