package com.techelevator;

import java.util.List;

public interface quizResponsesDao {

	public quizResponses submitQuizResponse(quizResponses response);
	public quizResponses submitQuizGrade(quizResponses response);
	List<quizResponses> getQuizResponses(long quiz_id, long student_id);
	List<quizResponses> getAllQuizResponses(long student_id);
}
