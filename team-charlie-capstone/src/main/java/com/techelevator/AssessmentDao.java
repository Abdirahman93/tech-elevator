package com.techelevator;

import java.util.List;

public interface AssessmentDao {
	
	public void createNewHomeworkAssignment(long teacherId);
	public int getMostRecentHomeworkId();
	public List<Assessment> studentGetAllHomework(long studentId);
	public List<Assessment> teacherGetAllHomework(long teacherId);
	public List<Assessment> teacherGetAllStudentHomework(long studentId, long teacherId);

//	public void createNewQuizAssignment(long teacherId);
//	public int getMostRecentQuizId();
//	public List<Assessment> teacherGetAllQuizzes(long teacherId);
//	public List<Assessment> studentGetAllQuizzes(long studentId);
//	public List<Assessment> teacherGetAllStudentQuizzes(long studentId, long teacherId);
	
//	public Assessment getHomeworkByAssessmentId(long assessmentId);
//	public List<Assessment> parentGetAllHomework(long parentId);
//	public Assessment gradeHomework(long teacherId, long studentId);
//	
//	public void assignQuiz(long teacherId);
//	public Assessment getQuiz(long studentId);
//	public Assessment getQuizByAssignmentId(long assessmentId);
//	public Assessment gradeQuiz(long teacherId, long studentId);
//	public List<Assessment>teacherGetAllQuizzes(long teacherId);
//	public List<Assessment>studentGetAllQuizzes(long studentId);
//	List<Assessment> studentGetAllHomework(long studentId, long teacherId);
	
}
