package com.techelevator;

import java.util.List;

import com.techelevator.User;

public interface UserDao {

	public List<User> getAllStudents();

	public boolean searchForEmailAndPassword(String email, String password);

	public void updatePassword(String email, String password);
	
	public User getUserById(long id);
	public User getUser(String email);
	
	public List<User> getStudentsByTeacherId(long teacherId);
	
	public List<User> getStudentsByParentId(long parentId);
	
	public User saveUser(User user);
	
	public List<User> getAllUsers();
	
	public void bindStudentToTeacher(long teacherId, long studentId);
	
	public void bindStudentToParents(long parentId, long studentId);
	
	public  List<User> bindStudentToParentS(long studentId);

	public  List<User> bindStudentToTeacherS(long studentId);
	
}
