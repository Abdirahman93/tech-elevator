package com.techelevator;

import java.util.List;

public interface QAssessmentDao {

	public void createNewQuizAssignment(long teacherId);
	public int getMostRecentQuizId();
	public List<QAssessment> studentGetAllQuiz(long studentId);
	public List<QAssessment> teacherGetAllQuiz(long teacherId);
	public List<QAssessment> teacherGetAllStudentQuizes(long studentId, long teacherId);

}
