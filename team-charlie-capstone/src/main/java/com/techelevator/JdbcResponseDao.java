package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcResponseDao implements ResponseDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcResponseDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Response submitHomeworkResponse(Response response) {
		String sqlInsertResponse = "INSERT INTO homework_response(homework_assignment_id, question_number, student_id, student_response) "
				+ "VALUES (?, ?, ?, ?) ";
		jdbcTemplate.update(sqlInsertResponse, response.getId(), response.getqNum(), response.getsId(),
				response.getResponse());
		return response;
	}

	@Override
	public List <Response> getHomeworkResponses(long homework_id, long student_id) {
		List <Response> viewResponse = new ArrayList<Response>();
		String sqlGetResource = "SELECT homework_response.* "
				+ "FROM homework_response "
				+ "JOIN homework_assignment ON homework_response.homework_assignment_id = homework_assignment.homework_assignment_id "
				+ "LEFT JOIN homework_questions ON homework_response.homework_assignment_id = homework_questions.homework_assignment_id "
				+ "AND homework_response.question_number = homework_questions.question_number "
				+ "WHERE homework_response.homework_assignment_id = ? AND homework_response.student_id = ?";

		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetResource, homework_id, student_id);
		while(result.next()) {
			Response view = mapRowToResponse(result);
			viewResponse.add(view);
		}
		return viewResponse;
	}
	
	@Override
	public Response submitHomeworkGrade(Response response) {
		String sqlInsertResponse = "UPDATE homework_response "
				+ "SET grade = ? "
				+ "WHERE student_id = ? AND question_number = ?";


		jdbcTemplate.update(sqlInsertResponse, response.getGrade(), response.getsId(), response.getqNum());
		return response;
	}

	

	private Response mapRowToResponse(SqlRowSet results) {
		Response theResponse = new Response();
		theResponse.setId(results.getLong("homework_assignment_id"));
		theResponse.setqNum(results.getInt("question_number"));
		theResponse.setsId(results.getInt("student_id"));
		theResponse.setResponse(results.getString("student_response"));
		theResponse.setGrade(results.getString("grade"));
 
		return theResponse;
	}

	@Override
	public List<Response> getAllHomeworkResponses(long student_id) {
		List<Response> allResponse = new ArrayList<Response>();
		String sqlGetAllHomeworkResponses = "SELECT * " +
											"FROM homework_response " +
											"WHERE student_id = ? ORDER BY question_number";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetAllHomeworkResponses, student_id);
		while(result.next()) {
			Response view = mapRowToResponse(result);
			allResponse.add(view);
		}
		return allResponse;
	}
}
