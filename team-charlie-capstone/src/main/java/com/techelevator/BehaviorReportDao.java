package com.techelevator;

import java.util.List;

public interface BehaviorReportDao {
	public BehaviorReport saveBehaviorReport(BehaviorReport behaviorReport);
	public List<BehaviorReport> getBehaviorReport(long studentId);


}
