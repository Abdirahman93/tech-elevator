package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;


@Component
public class JdbcQuizQuestionsDao implements QuizQuestionsDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcQuizQuestionsDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public quizQuestions insertQuizQuestion(quizQuestions question) {
		String sqlInsertQuestion = "INSERT INTO quiz_questions(quiz_assignment_id, question_type, question_text1, question_text2) "+
								   "VALUES (?, ?, ?, ?) ";
		jdbcTemplate.update(sqlInsertQuestion, question.getId(), question.getQuestionType(), question.getText1(), question.getText2());
		return question;
	}
	
	
	
	@Override
	public List <quizQuestions> getQuizQuestions(long quiz_id, long student_id) {
		List <quizQuestions> viewQuestions = new ArrayList<quizQuestions>();
		String sqlGetResource = "SELECT * FROM quiz_questions "
				+ "JOIN quiz_assignment ON quiz_questions.quiz_assignment_id = quiz_assignment.quiz_assignment_id "
				+ "JOIN teacher_student ON quiz_assignment.teacher_id = teacher_student.teacher_id "
				+ "WHERE quiz_questions.quiz_assignment_id = ? AND teacher_student.student_id = ? ORDER by question_number";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetResource, quiz_id, student_id);
		while(result.next()) {
			quizQuestions view = mapRowToQuizQuestion(result);
			viewQuestions.add(view);
		}
		return viewQuestions;
	}
	
	
	
	private quizQuestions mapRowToQuizQuestion(SqlRowSet results) {
		quizQuestions theQuestion = new quizQuestions();
		theQuestion.setQuestionType(results.getString("question_type"));
		theQuestion.setText1(results.getString("question_text1"));
		theQuestion.setText2(results.getString("question_text2"));
		theQuestion.setQuestionNumber(results.getInt("question_number"));
		theQuestion.setId(results.getLong("quiz_assignment_id"));
		
		return theQuestion;
	}

	@Override
	public List<quizQuestions> getAllStudentQuizQuestions(long student_id) {
		List<quizQuestions> viewAllQuestions = new ArrayList<quizQuestions>();
		String sqlGetAllQuestions = "SELECT quiz_questions.quiz_assignment_id, quiz_questions.question_number, quiz_questions.question_type, quiz_questions.question_text1, quiz_questions.question_text2 "+
									"FROM quiz_questions "+
									"JOIN quiz_response ON quiz_questions.question_number = quiz_response.question_number "+
									"WHERE student_id = ? ORDER BY quiz_questions.question_number";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetAllQuestions, student_id);
		while(result.next()) {
			quizQuestions view = mapRowToQuizQuestion(result);
			viewAllQuestions.add(view);
		}
		return viewAllQuestions;
	}
}