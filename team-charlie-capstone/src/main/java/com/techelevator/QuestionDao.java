package com.techelevator;

import java.util.List;

public interface QuestionDao {

	public Question insertHomeworkQuestion(Question question);
	public List<Question> getHomeworkQuestions(long homework_id, long student_id);
	public List<Question> getAllStudentHomeworkQuestions(long student_id);
	
//	public List<Question> getQuizQuestion(long homeworkAssignmentId);
//	public Question setQuizQuestion();
//	public Question submitQuizResponse();
	
}
