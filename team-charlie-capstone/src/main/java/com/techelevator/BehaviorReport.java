package com.techelevator;

import java.time.LocalDate;

public class BehaviorReport {

	private long behaviorReportId;
	private long teacherId;
	private long studentId;
	private LocalDate issued;
	private String reportText;
	private boolean positive;
	
	public long getBehaviorReportId() {
		return behaviorReportId;
	}
	public void setBehaviorReportId(long behaviorReportId) {
		this.behaviorReportId = behaviorReportId;
	}
	public long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(long teacherId) {
		this.teacherId = teacherId;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public LocalDate getIssued() {
		return issued;
	}
	public void setIssued(LocalDate issued) {
		this.issued = issued;
	}
	public String getReportText() {
		return reportText;
	}
	public void setReportText(String reportText) {
		this.reportText = reportText;
	}
	public boolean isPositive() {
		return positive;
	}
	public void setPositive(boolean positive) {
		this.positive = positive;
	}
	
	
}
