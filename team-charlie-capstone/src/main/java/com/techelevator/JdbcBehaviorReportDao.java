package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcBehaviorReportDao implements BehaviorReportDao{
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcBehaviorReportDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	@Override
	public BehaviorReport saveBehaviorReport(BehaviorReport behavior) {
		String sqlCreateBehaviorReport = "INSERT INTO behavior_report(teacher_id, student_id, issued, report_text, positive) VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlCreateBehaviorReport, behavior.getTeacherId(), behavior.getStudentId(), behavior.getIssued(), behavior.getReportText(), behavior.isPositive());
		return behavior;
	}
	
	private BehaviorReport mapRowToBehaviorReport(SqlRowSet results) {
		BehaviorReport theBehaviorReport;
		theBehaviorReport = new BehaviorReport();
		theBehaviorReport.setBehaviorReportId(results.getLong("behavior_report_id"));
		theBehaviorReport.setTeacherId(results.getInt("teacher_id"));
		theBehaviorReport.setStudentId(results.getInt("student_id"));
		theBehaviorReport.setIssued(results.getDate("issued").toLocalDate());
		theBehaviorReport.setReportText(results.getString("report_text"));
		theBehaviorReport.setPositive(results.getBoolean("positive"));

	    return theBehaviorReport;
	}


	@Override
	public List<BehaviorReport> getBehaviorReport(long studentId) {
		List<BehaviorReport> reportList = new ArrayList<BehaviorReport>();
		String sqlGetBehaviorReportById = "Select * FROM behavior_report WHERE student_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetBehaviorReportById, studentId);
		while(results.next()){
			 BehaviorReport thisReport = mapRowToBehaviorReport(results);
			 reportList.add(thisReport);
		}
		return reportList;
	}
	

}
