package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcQuestionDao implements QuestionDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcQuestionDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public Question insertHomeworkQuestion(Question question) {
		String sqlInsertQuestion = "INSERT INTO homework_questions(homework_assignment_id, question_type, question_text1, question_text2) "+
								   "VALUES (?, ?, ?, ?) ";
		jdbcTemplate.update(sqlInsertQuestion, question.getId(), question.getQuestionType(), question.getText1(), question.getText2());
		return question;
	}
	
	@Override
	public List <Question> getHomeworkQuestions(long homework_id, long student_id) {
		List <Question> viewQuestions = new ArrayList<Question>();
		String sqlGetQuestions = "SELECT * FROM homework_questions "
				+ "JOIN homework_assignment ON homework_questions.homework_assignment_id = homework_assignment.homework_assignment_id "
				+ "JOIN teacher_student ON homework_assignment.teacher_id = teacher_student.teacher_id "
				+ "WHERE homework_questions.homework_assignment_id = ? AND teacher_student.student_id = ? ORDER by question_number";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetQuestions, homework_id, student_id);
		while(result.next()) {
			Question view = mapRowToQuestion(result);
			viewQuestions.add(view);
		}
		return viewQuestions;
	}
	
	private Question mapRowToQuestion(SqlRowSet results) {
		Question theQuestion = new Question();
		theQuestion.setQuestionType(results.getString("question_type"));
		theQuestion.setText1(results.getString("question_text1"));
		theQuestion.setText2(results.getString("question_text2"));
		theQuestion.setQuestionNumber(results.getInt("question_number"));
		theQuestion.setId(results.getLong("homework_assignment_id"));
		
		return theQuestion;
	}

	@Override
	public List<Question> getAllStudentHomeworkQuestions(long student_id) {
		List<Question> viewAllQuestions = new ArrayList<Question>();
		String sqlGetAllQuestions = "SELECT homework_questions.homework_assignment_id, homework_questions.question_number, homework_questions.question_type, homework_questions.question_text1, homework_questions.question_text2 "+
									"FROM homework_questions "+
									"JOIN homework_response ON homework_questions.question_number = homework_response.question_number "+
									"WHERE student_id = ? ORDER BY homework_questions.question_number";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetAllQuestions, student_id);
		while(result.next()) {
			Question view = mapRowToQuestion(result);
			viewAllQuestions.add(view);
		}
		return viewAllQuestions;
	}
	
}
