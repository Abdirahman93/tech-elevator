package com.techelevator;

import java.util.List;

public interface QuizQuestionsDao {

	public quizQuestions insertQuizQuestion(quizQuestions question);
	public List<quizQuestions> getQuizQuestions(long quiz_id, long student_id);
	public List<quizQuestions> getAllStudentQuizQuestions(long student_id);

}
