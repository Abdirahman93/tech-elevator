package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcQuizResponseDao implements quizResponsesDao{

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcQuizResponseDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public quizResponses submitQuizResponse(quizResponses response) {
		String sqlInsertResponse = "INSERT INTO quiz_response(quiz_assignment_id, question_number, student_id, student_response) "+
								   "VALUES (?, ?, ?, ?) ";
		jdbcTemplate.update(sqlInsertResponse, response.getId(), response.getqNum(), response.getsId(), response.getResponse());
		return response;
	}

	@Override
	public List <quizResponses> getQuizResponses(long quiz_id, long student_id) {
		List <quizResponses> viewResponse = new ArrayList<quizResponses>();
		String sqlGetResource = "SELECT quiz_response.* "
				+ "FROM quiz_response "
				+ "JOIN quiz_assignment ON quiz_response.quiz_assignment_id = quiz_assignment.quiz_assignment_id "
				+ "LEFT JOIN quiz_questions ON quiz_response.quiz_assignment_id = quiz_questions.quiz_assignment_id "
				+ "AND quiz_response.question_number = quiz_questions.question_number "
				+ "WHERE quiz_response.quiz_assignment_id = ? AND quiz_response.student_id = ?";

		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetResource, quiz_id, student_id);
		while(result.next()) {
			quizResponses view = mapRowToResponse(result);
			viewResponse.add(view);
		}
		return viewResponse;
	}
	
	@Override
	public quizResponses submitQuizGrade(quizResponses response) {
		String sqlInsertResponse = "UPDATE quiz_response "
				+ "SET grade = ? "
				+ "WHERE student_id = ? AND question_number = ?";


		jdbcTemplate.update(sqlInsertResponse, response.getGrade(), response.getsId(), response.getqNum());
		return response;
	}

	private quizResponses mapRowToResponse(SqlRowSet results) {
		quizResponses theResponse = new quizResponses();
		theResponse.setId(results.getLong("quiz_assignment_id"));
		theResponse.setqNum(results.getInt("question_number"));
		theResponse.setsId(results.getInt("student_id"));
		theResponse.setResponse(results.getString("student_response"));
		theResponse.setGrade(results.getString("grade"));

		
		return theResponse;
	}

	@Override
	public List<quizResponses> getAllQuizResponses(long student_id) {
		List<quizResponses> allResponses = new ArrayList<quizResponses>();
		String sqlGetAllQuizResponses = "SELECT * " +
										"FROM quiz_response " +
										"WHERE student_id = ? ORDER BY question_number";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetAllQuizResponses, student_id);
		while(result.next()) {
			quizResponses view = mapRowToResponse(result);
			allResponses.add(view);
		}
		return allResponses;
	}
}

