package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcAssessmentDao implements AssessmentDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcAssessmentDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	private Assessment mapRowToAssessment(SqlRowSet results) {
		Assessment theAssessment = new Assessment();
		theAssessment.setAssignmentId(results.getInt("homework_assignment_id"));
		theAssessment.setTeacherId(results.getInt("teacher_id"));
		return theAssessment;
	}
	
	@Override
	public List<Assessment> studentGetAllHomework(long studentId) {
		List<Assessment> allHomework = new ArrayList<Assessment>();
		String sqlStudentGetAllHomework = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, homework_assignment.homework_assignment_id, homework_assignment.teacher_id "+
										  "FROM homework_assignment "+
										  "JOIN appuser ON homework_assignment.teacher_id = appuser.user_id "+
										  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
										  "WHERE teacher_student.student_id = ? ORDER BY homework_assignment.teacher_id";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentGetAllHomework, studentId);
		while (result.next()) {
			Assessment view = mapRowToAssessment(result);
			allHomework.add(view);
		}
		return allHomework;
	}
		
	@Override
	public List<Assessment> teacherGetAllStudentHomework(long studentId, long teacherId) {
		List<Assessment> allHomework = new ArrayList<Assessment>();
		String sqlStudentGetAllHomework = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, homework_assignment.homework_assignment_id, homework_assignment.teacher_id "+
										  "FROM homework_assignment "+
										  "JOIN appuser ON homework_assignment.teacher_id = appuser.user_id "+
										  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
										  "WHERE teacher_student.student_id = ? AND teacher_student.teacher_id= ? ORDER BY homework_assignment.teacher_id ";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentGetAllHomework, studentId, teacherId);
		while (result.next()) {
			Assessment view = mapRowToAssessment(result);
			allHomework.add(view);
		}
		return allHomework;
	}

	@Override
	public List<Assessment> teacherGetAllHomework(long teacherId) {
		List<Assessment> allHomework = new ArrayList<Assessment>();
		String sqlTeacherGetAllHomework = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, homework_assignment.homework_assignment_id "+
										  "FROM homework_assignment "+
										  "JOIN appuser ON homework_assignment.teacher_id = appuser.user_id "+
										  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
										  "WHERE teacher_student.teacher_id = ? ORDER BY homework_assignment.teacher_id";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlTeacherGetAllHomework, teacherId);
		while (result.next()) {
			Assessment view = mapRowToAssessment(result);
			allHomework.add(view);
		}
		return allHomework;
	}	
	
	@Override
	public int getMostRecentHomeworkId() {
		int homeworkId = 0;
		String sqlGetMostRecentHomeworkId = "SELECT homework_assignment_id "+
									 		"FROM homework_assignment "+
									 		"ORDER BY homework_assignment_id DESC "+
									 		"LIMIT 1";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetMostRecentHomeworkId);
		if (result.next()) {
			homeworkId = result.getInt("homework_assignment_id");
		}
		return homeworkId;
	}

	@Override
	public void createNewHomeworkAssignment(long teacherId) {
		String sqlCreateNewHomework = "INSERT INTO homework_assignment (teacher_id) " +
									  "VALUES (?)";
		jdbcTemplate.update(sqlCreateNewHomework, teacherId);
	}
	
//	@Override
//	public Assessment getHomeworkByAssessmentId(long assessmentId) {
//		String sqlGetHomeworkId = "SELECT homework_assignment_id "+
//								  "FROM homework_assignment "+
//								  "WHERE homework_assignment_id = ?";
//		
//		jdbcTemplate.update(sqlGetHomeworkId, assessmentId);
//		return getHomeworkByAssessmentId(assessmentId);
//	}
	
//	@Override
//	public Assessment getQuizByAssignmentId(long assessmentId) {
//		String sqlGetQuizId = "SELECT quiz_assignment_id "+
//				  			  "FROM quiz_assignment "+
//				  			  "WHERE quiz_assignment_id = ?";
//
//		jdbcTemplate.update(sqlGetQuizId, assessmentId);
//		return getQuizByAssignmentId(assessmentId);
//	}
	
//	@Override
//	public List<Assessment> studentGetAllQuizzes(long studentId) {
//		List<Assessment> allQuizzes = new ArrayList<Assessment>();
//		String sqlStudentGetAllQuizzes =  "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, quiz_assignment.quiz_assignment_id "+
//										  "FROM quiz_assignment "+
//										  "JOIN appuser ON quiz_assignment.teacher_id = appuser.user_id "+
//										  "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
//										  "WHERE teacher_student.student_id = ? ORDER BY quiz_assignment.teacher_id";
//		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlStudentGetAllQuizzes, studentId);
//		while (result.next()) {
//			Assessment view = mapRowToAssessment(result);
//			allQuizzes.add(view);
//		}
//		return allQuizzes;
//	}
	

}
	

