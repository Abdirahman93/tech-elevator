package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcResourceDao implements ResourceDao{

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcResourceDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public Resource saveResource(Resource resource) {
		String sqlCreateResource = "INSERT INTO resources(teacher_id, link_content, text_content, file_content) VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sqlCreateResource, resource.getTeacherId(), resource.getLinkContent(), resource.getTextContent(), resource.getFileContent());
		return resource;
	}
	
	@Override
	public List <Resource> getResource(long student_id) {
		List <Resource> viewResource = new ArrayList<Resource>();
		String sqlGetResource = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, link_content, text_content, file_content, resources_id, resources.teacher_id "+
							    "FROM resources JOIN appuser ON resources.teacher_id = appuser.user_id "+
							    "JOIN teacher_student ON appuser.user_id = teacher_student.teacher_id "+
							    "WHERE teacher_student.student_id = ? ORDER BY resources.teacher_id ";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetResource, student_id);
		while(result.next()) {
			Resource view = mapRowToResource(result);
			viewResource.add(view);
		}
		return viewResource;
	}
	
	@Override
	public List <Resource> getResourceByTeacherId(long teacher_id) {
		List <Resource> viewResource = new ArrayList<Resource>();
		String sqlGetResource = "SELECT appuser.first_name AS first_name, appuser.last_name AS last_name, link_content, text_content, file_content, resources_id, resources.teacher_id "+
							    "FROM resources JOIN appuser ON resources.teacher_id = appuser.user_id " +
							    "WHERE resources.teacher_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetResource, teacher_id);
		while(result.next()) {
			Resource view = mapRowToResource(result);
			viewResource.add(view);
		}
		return viewResource;
	}
	
	private Resource mapRowToResource(SqlRowSet results) {
	    Resource theResource;
	    theResource = new Resource();
	    theResource.setResourceId(results.getLong("resources_id"));
	    theResource.setTeacherId(results.getInt("teacher_id"));
	    theResource.setTeacherName(results.getString("first_name") + " " + results.getString("last_name"));
	    theResource.setLinkContent(results.getString("link_content"));
	    theResource.setTextContent(results.getString("text_content"));
	    theResource.setFileContent(results.getString("file_content"));

	    return theResource;
	}

}
