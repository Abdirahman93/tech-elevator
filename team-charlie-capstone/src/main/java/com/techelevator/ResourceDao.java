package com.techelevator;

import java.util.List;

public interface ResourceDao {

	public Resource saveResource(Resource resource);

	public List<Resource> getResource(long student_id);
	
	public List<Resource> getResourceByTeacherId(long teacher_id);

}
