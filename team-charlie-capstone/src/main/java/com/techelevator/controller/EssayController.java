package com.techelevator.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.Question;
import com.techelevator.QuestionDao;
import com.techelevator.QuizQuestionsDao;
import com.techelevator.quizQuestions;

@Controller
@SessionAttributes({"currentUser", "assignId"})
public class EssayController {

	private QuestionDao questionDao;
	private QuizQuestionsDao quizQuestionsDao;

	@Autowired
	public EssayController(QuestionDao questionDao, QuizQuestionsDao quizQuestionsDao) {
		this.questionDao = questionDao;
		this.quizQuestionsDao = quizQuestionsDao;
	}
	

	
	@RequestMapping(path="user/assignHomework/Essay", method = RequestMethod.POST)
	public String setEssay(@RequestParam String text1, @RequestParam (value="text3",required= false) long text3, ModelMap model) {
		Question question = new Question();
		question.setQuestionType("essay");
		question.setText1(text1);
		question.setId(text3);
		questionDao.insertHomeworkQuestion(question);
		return "user/assignHomework";
	}
	
	@RequestMapping(path="user/assignQuiz/Essay", method = RequestMethod.POST)
	public String setEssayQuiz(@RequestParam String text1, @RequestParam (value="text3",required= false) long text3, ModelMap model) {
		quizQuestions question = new quizQuestions();
		question.setQuestionType("essay");
		question.setText1(text1);
		question.setId(text3);
		quizQuestionsDao.insertQuizQuestion(question);
		return "user/assignQuiz";
	}
	
}
