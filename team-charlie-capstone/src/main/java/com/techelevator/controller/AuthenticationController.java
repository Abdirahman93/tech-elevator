package com.techelevator.controller;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.UserDao;
import com.techelevator.User;

@Controller
@SessionAttributes("currentUser")
public class AuthenticationController {

	private UserDao userDao;

	@Autowired
	public AuthenticationController(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@RequestMapping(path = "/", method=RequestMethod.GET)
	public String showHomePage(HttpServletRequest request) {
		
		return "login";
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
    public String loginUser(ModelMap model,
    						@RequestParam String email, 
    						@RequestParam String password,
    						HttpSession session) {   
		
		if (userDao.searchForEmailAndPassword(email, password)) {
			session.invalidate();
			User currentUser = userDao.getUser(email);
			model.addAttribute("currentUser", currentUser);
			return "redirect:/user/userDashboard";
		} else {
			model.addAttribute("Incorrect", "Wrong Username or Password");
			return "redirect:/";
		}
		
     }
	
	@RequestMapping(path="/logout", method=RequestMethod.GET)
	public String logout(ModelMap model, HttpSession session) {
		model.remove("currentUser");
		session.removeAttribute("currentUser");
		return "redirect:/";
	}
	
}
