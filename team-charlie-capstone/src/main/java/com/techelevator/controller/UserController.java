package com.techelevator.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.UserDao;
import com.techelevator.quizQuestions;
import com.techelevator.quizResponses;
import com.techelevator.quizResponsesDao;
import com.techelevator.Assessment;
import com.techelevator.AssessmentDao;
import com.techelevator.BehaviorReport;
import com.techelevator.BehaviorReportDao;
import com.techelevator.QAssessment;
import com.techelevator.QAssessmentDao;
import com.techelevator.Question;
import com.techelevator.QuestionDao;
import com.techelevator.QuizQuestionsDao;
import com.techelevator.Response;
import com.techelevator.ResponseDao;
import com.techelevator.User;


@Controller
@SessionAttributes("currentUser")
public class UserController {
private UserDao userDao;
private AssessmentDao assessmentDao;
private QuestionDao questionDao;
private ResponseDao responseDao;
private QAssessmentDao qAssessmentDao;
private QuizQuestionsDao quizQuestionsDao;
private quizResponsesDao quizResponsesDao;
private BehaviorReportDao behaviorReportDao;

	
	@Autowired
	public UserController(UserDao userDao, AssessmentDao assessmentDao, QuestionDao questionDao, ResponseDao responseDao, QAssessmentDao qAssessmentDao, QuizQuestionsDao quizQuestionsDao, quizResponsesDao quizResponsesDao, BehaviorReportDao behaviorReportDao) {
		this.userDao = userDao;
		this.assessmentDao = assessmentDao;
		this.questionDao = questionDao;
		this.responseDao = responseDao;
		this.qAssessmentDao = qAssessmentDao;
		this.quizQuestionsDao = quizQuestionsDao;
		this.quizResponsesDao = quizResponsesDao;
		this.behaviorReportDao = behaviorReportDao;
	}
		
	@RequestMapping(path="/user/userDashboard", method=RequestMethod.GET)
	public String showUserDashboard(HttpServletRequest request, ModelMap model) {
		User user = (User)model.get("currentUser");
		if (user.getRole().equals("teacher")) {
			List<User> students = userDao.getStudentsByTeacherId(user.getId());
			request.setAttribute("students", students);
		} else if(user.getRole().equals("parent")) {
			List<User> students = userDao.getStudentsByParentId(user.getId());
			request.setAttribute("students", students);
		}
		return "user/userDashboard";
	}
	
	@RequestMapping(path="/user/createAccount")
	public String inputNewUser(ModelMap model) {
		if (((User)model.get("currentUser")).getRole().equals("teacher")) { 
		return "user/createAccount";
		}
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/createAccount", method=RequestMethod.POST)
    public String createNewUser(@RequestParam String firstName, 
    							@RequestParam String lastName, 
    							@RequestParam String role, 
    							@RequestParam String email, 
    							@RequestParam String password, 
    							ModelMap model) {
		
    	User user = new User();
    	user.setFirstName(firstName);
    	user.setLastName(lastName);
    	user.setRole(role);
    	user.setEmail(email);
    	user.setPassword(password);
    	userDao.saveUser(user);
    
    	return "redirect:/user/bindAccounts";
    }
	
	@RequestMapping(path="/user/viewStudentProgress", method=RequestMethod.GET)
	public String viewStudentProgress(@RequestParam (value="student_id", required=false) Long student_id,
			                          ModelMap model) {
		User user = (User) model.get("currentUser");
		if (user.getRole().equals("student")){
			student_id = user.getId();
		}
		List<Assessment> homeworkList = assessmentDao.studentGetAllHomework(student_id);
		List<Question> homeworkQuestionList = questionDao.getAllStudentHomeworkQuestions(student_id);
		List<Response> homeworkResponseList = responseDao.getAllHomeworkResponses(student_id);
		List<QAssessment> quizList = qAssessmentDao.studentGetAllQuiz(student_id);
		List<quizQuestions> quizQuestionList = quizQuestionsDao.getAllStudentQuizQuestions(student_id);
		List<quizResponses> quizResponseList = quizResponsesDao.getAllQuizResponses(student_id);
		List<BehaviorReport> behavior = behaviorReportDao.getBehaviorReport(student_id);
		model.addAttribute("behaviorReports", behavior);
		model.addAttribute("homework", homeworkList);
		model.addAttribute("homeworkQuestions", homeworkQuestionList);
		model.addAttribute("homeworkResponses", homeworkResponseList);
		model.addAttribute("quiz", quizList);
		model.addAttribute("quizQuestions", quizQuestionList);
		model.addAttribute("quizResponses", quizResponseList);
		return "/user/viewStudentProgress";
	}
	
	@RequestMapping(path ="/user/bindAccounts", method = RequestMethod.GET)
	public String bindStudent(HttpServletRequest request, ModelMap model) {
		User user = (User) model.get("currentUser");

		if (user.getRole().equals("teacher")) {
			List<User> studentList = userDao.getAllStudents();
			request.setAttribute("studentList", studentList);
			return "user/listStudents";
		}
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path ="/user/bindAccounts/{studentId}", method = RequestMethod.GET)
	public String bindAccounts(HttpServletRequest request, @PathVariable int studentId, ModelMap model) {
		User user = (User) model.get("currentUser");

		if (user.getRole().equals("teacher")) {
			List<User> teacherList = userDao.bindStudentToTeacherS(studentId);
			List<User> parentList = userDao.bindStudentToParentS(studentId);
			User student = userDao.getUserById(studentId);

			request.setAttribute("student", student);
			request.setAttribute("teacherList", teacherList);
			request.setAttribute("parentList", parentList);
			return "user/bindAccounts";
		}
		return "redirect:/user/userDashboard";
	}
	
	@RequestMapping(path="/user/bindAccounts/{studentId}", method=RequestMethod.POST)
	public String bindUserAccounts(@RequestParam (value="teacherId",required=false) Long teacherId,
								   @PathVariable int studentId,
								   @RequestParam (value="parentId", required=false) Long parentId,
								   ModelMap model) {
			if (teacherId != null) {
				userDao.bindStudentToTeacher(teacherId, studentId);
			}
			if (parentId != null) {
				userDao.bindStudentToParents(parentId, studentId);
			}
			return "redirect:/user/userDashboard";
		}
	
	

	
	
	
}
