package com.techelevator.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.Question;
import com.techelevator.QuestionDao;
import com.techelevator.QuizQuestionsDao;
import com.techelevator.quizQuestions;



@Controller
@SessionAttributes({"currentUser", "assignId"})
public class TrueFalseController {
	private QuestionDao questionDao;
	private QuizQuestionsDao quizQuestionsDao;
	@Autowired
	public TrueFalseController(QuestionDao questionDao, QuizQuestionsDao quizQuestionsDao) {
		this.questionDao = questionDao;
		this.quizQuestionsDao = quizQuestionsDao;
	}
	
	@RequestMapping(path="user/assignHomework/TrueFalse", method = RequestMethod.POST)
	public String setTrueFalse(@RequestParam String text1,@RequestParam String text2, @RequestParam (value="text3",required= false) long text3, ModelMap model) {
		Question question = new Question();
		question.setQuestionType("tf");
		question.setText1(text1);
		question.setId(text3);
		questionDao.insertHomeworkQuestion(question);
		return "user/assignHomework";
	}
	
	@RequestMapping(path="user/assignQuiz/TrueFalse", method = RequestMethod.POST)
	public String setTrueFalseQuiz(@RequestParam String text1, @RequestParam (value="text3",required= false) long text3 , ModelMap model) {
		quizQuestions question = new quizQuestions();
		question.setQuestionType("tf");
		question.setText1(text1);
		question.setId(text3);
		quizQuestionsDao.insertQuizQuestion(question);
		return "user/assignQuiz";
	}
	
}
