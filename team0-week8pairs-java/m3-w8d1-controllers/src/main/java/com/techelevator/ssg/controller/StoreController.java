package com.techelevator.ssg.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.ssg.model.store.DollarAmount;
import com.techelevator.ssg.model.store.Product;
import com.techelevator.ssg.model.store.ProductDao;
import com.techelevator.ssg.model.store.ShoppingCartProduct;


@Controller
@SessionAttributes({"shoppingCart", "grandTotal"})
public class StoreController {

	@Autowired
	ProductDao productDao;
	
	@RequestMapping(path={"/productLandingPage"}, method=RequestMethod.GET)
	public String showProductHomePage(ModelMap map){
		List<Product> products = productDao.getAllProducts();
		
		map.put("products", products);
		
		return "productLandingPage";
	}
	
	@RequestMapping(path={"/productDetails"}, method=RequestMethod.GET)
	public String showProductDetails(@RequestParam long productId, ModelMap map){
		Product product = productDao.getProductById(productId);
		map.put("product", product);
		return "productDetails";
	}
	
	@RequestMapping(path={"/productDetails"}, method=RequestMethod.POST)
	public String addProductToCart(@RequestParam long productId, @RequestParam int quantity, ModelMap map){
		
		ShoppingCartProduct item = new ShoppingCartProduct();
		item.setProduct(productDao.getProductById(productId));
		item.setQuantity(quantity);
		
		List<ShoppingCartProduct> cart;
		if(map.containsKey("shoppingCart")) {
			cart = (List<ShoppingCartProduct>)map.get("shoppingCart");
		} else {
			cart = new ArrayList<ShoppingCartProduct>();
			map.addAttribute("shoppingCart", cart);
		}

		cart.add(item);
		
		DollarAmount grandTotal = new DollarAmount(0);
		for(ShoppingCartProduct product : cart) {
			grandTotal = grandTotal.plus(product.getTotalCost());
		}
		map.addAttribute("grandTotal", grandTotal);
		
		return "redirect:/shoppingCart";
	}
	
	@RequestMapping(path={"/shoppingCart"}, method=RequestMethod.GET)
	public String showShoppingCart(){
		
		return "shoppingCart";
	}
	
	
	@RequestMapping(path={"/shoppingCart"}, method=RequestMethod.POST)
	public String showShoppingCart(ModelMap map){
		
		map.addAttribute("shoppingCart", new ArrayList<ShoppingCartProduct>());
		
		return "redirect:/";
	}
	

}