package com.techelevator.ssg.controller;

import java.text.DecimalFormat;

public class Planet {
	
	private int earthWeight;
	private double alienWeight;
	private String planetName;
	private String imagePath;
	private int earthAge;
	private double alienAge;
	private String formOfTransportation;
	private int travelSpeed;
	private DecimalFormat format = new DecimalFormat("000.00");
	private double travelTime;

	public Planet(int earthWeight, String planetName){
		this.earthWeight = earthWeight;
		this.planetName = planetName;
		this.imagePath = "img/" + planetName.toLowerCase() + ".jpg";
	}
	
	public Planet(String planetName, int earthAge){
		this.earthAge = earthAge;
		this.planetName = planetName;
	}
	
	public Planet(String planetName, String formOfTransportation, int earthAge){
		this.earthAge = earthAge;
		this.planetName = planetName;
		this.formOfTransportation = formOfTransportation;
	}
	
	public int getEarthWeight(){
		return earthWeight;
	}
	
	public int getEarthAge(){
		return earthAge;
	}
	
	public double getAlienWeight(){
		return alienWeight;
	}
	
	public double getAlienAge(){
		return alienAge;
	}
	
	public String getPlanetName(){
		return planetName;
	}
	
	public String getFormOfTransportation(){
		return formOfTransportation;
	}
	
	public double getTravelTime(){
		return travelTime;
	}
	
	public int getTravelSpeed(){
		return travelSpeed;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
    public void setAlienWeight(){
    	
		switch(planetName) {
			case "Mercury":
				alienWeight = earthWeight * 0.37;
				break;
			case "Venus":
				alienWeight = earthWeight * 0.90;
				break;
			case "Mars":
				alienWeight = earthWeight * 0.38;
				break;
			case "Jupiter":
				alienWeight = earthWeight * 2.65;
				break;
			case "Saturn":
				alienWeight = earthWeight * 1.13;
				break;
			case "Uranus":
				alienWeight = earthWeight * 1.09;
				break;
			case "Neptune":
				alienWeight = earthWeight * 1.43;
				break;
			default:
				alienWeight = earthWeight;
				break;
				
		}
		
	}
 
    public void calculateTravelTime(String formOfTransportation){
    	double miles;
    
    	switch(planetName) {
		case "Mercury":
			miles = 56974146.0;
			travelTime = (miles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		case "Venus":
			miles = 25724767.0;
			travelTime = (miles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		case "Mars":
			miles = 48678219.0;
			travelTime = (miles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		case "Jupiter":
			miles = 390674710.0;
			travelTime = (miles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		case "Saturn":
			miles = 792248270.0;
			travelTime = (miles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		case "Uranus":
			miles = 1692662530.0;
			travelTime = (miles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		case "Neptune":
			long longMiles = (long) 2703959960.0;
			travelTime = (longMiles / (double)travelSpeed) / 8760;
			travelTime = Double.parseDouble(format.format(travelTime));
			earthAge+=travelTime;
			break;
		default:
			travelTime = 0;
			break;
    	}
    }
    
    
    public void setAlienAge(){
    	
    	double yearLength;
   
		switch(planetName) {
			case "Mercury":
				yearLength = (365.0/87.96);
				alienAge = Double.parseDouble(format.format(earthAge * yearLength));
				break;
			case "Venus":
				yearLength = (365.0/224.68);
				alienAge = Double.parseDouble(format.format(earthAge * yearLength));
				break;
			case "Mars":
				yearLength = (365.0/686.98);
				alienAge = Double.parseDouble(format.format(earthAge * yearLength));
				break;
			case "Jupiter":
				alienAge = Double.parseDouble(format.format((double)earthAge / 11.862));
				break;
			case "Saturn":
				alienAge = Double.parseDouble(format.format((double)earthAge / 29.456));
				break;
			case "Uranus":
				alienAge = Double.parseDouble(format.format((double)earthAge /84.07));
				break;
			case "Neptune":
				alienAge = Double.parseDouble(format.format((double)earthAge / 164.81));
				break;
			default:
				alienAge = earthAge;
				break;
				
		}
		
	}
	
	public void setTravelSpeed(){
		
		switch(formOfTransportation){
		case "Walking":
			travelSpeed = 3;
			break;
		case "Car":
			travelSpeed = 100;
			break;
		case "Bullet Train":
			travelSpeed = 200;
			break;
		case "Boeing 747":
			travelSpeed = 570;
			break;
		case "Concorde":
			travelSpeed = 1350;
			break;
		}
	}
	
	
	
}
