package com.techelevator.ssg.model.store;

public class ShoppingCartProduct {
	
	private int quantity;
	private DollarAmount totalCost;
	private Product product;
	
	public void setProduct(Product product){
		this.product = product;
		totalCost = product.getPrice().multiply(quantity);
	}
	
	public int getQuantity(){
		return quantity;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
		if(product != null) {
			totalCost = product.getPrice().multiply(quantity);
		}
	}
	
	public DollarAmount getTotalCost(){
		return totalCost;
	}

}
