package com.techelevator.ssg.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.techelevator.ssg.model.forum.ForumDao;
import com.techelevator.ssg.model.forum.ForumPost;

@Controller
public class HomeController {
	@Autowired
	private ForumDao forumDao;

	@RequestMapping("/")
	public String displayHomePage() {
		return "homePage";
	}
	
	@RequestMapping("/alienWeightInput")
	public String showAlienWeightInput() {
		return "alienWeightInput";
	}
	
	@RequestMapping("/alienWeightResults")
	public String showAlienWeightResults(HttpServletRequest results) {
		
		String planetName = results.getParameter("planetName");
		int earthWeight = Integer.parseInt(results.getParameter("earthWeight"));
		
		Planet planet = new Planet(earthWeight, planetName);
		planet.setAlienWeight();
		
		results.setAttribute("planet", planet);
		return "alienWeightResults";
	}
	
	@RequestMapping("/alienAgeInput")
	public String showAlienAgeInput() {
		return "alienAgeInput";
	}
	
	@RequestMapping("/alienAgeResults")
	public String showAlienAgeResults(HttpServletRequest results) {
		
		String planetName = results.getParameter("planetName");
		int earthAge = Integer.parseInt(results.getParameter("earthAge"));
		
		Planet planet = new Planet(planetName, earthAge);
		planet.setAlienAge();
		
		results.setAttribute("planet", planet);
		return "alienAgeResults";
	}
	
	@RequestMapping("/driveTimeInput")
	public String showDriveTimeInput() {
		return "driveTimeInput";
	}
	
	@RequestMapping("/driveTimeResults")
	public String showDriveTimeResults(HttpServletRequest results) {
		
		String planetName = results.getParameter("planetName");
		String formOfTransportation = results.getParameter("formOfTransportation");
		int earthAge = Integer.parseInt(results.getParameter("earthAge"));
		
		Planet planet = new Planet(planetName, formOfTransportation, earthAge);
		planet.setTravelSpeed();
		planet.calculateTravelTime(formOfTransportation);
		planet.setAlienAge();
		
		results.setAttribute("planet", planet);
		return "driveTimeResults";
	}
	
	@RequestMapping(path="/spaceForumInput", method=RequestMethod.GET)
	public String showForumInput(){
		return "spaceForumInput";

	}
		
	@RequestMapping(path="/spaceForumInput", method=RequestMethod.POST)
	public String makeForumPost(ForumPost post){
		post.setDatePosted(LocalDateTime.now());
		forumDao.save(post);
		
		return "redirect:/spaceForumOutput";

	}
	
	@RequestMapping("/spaceForumOutput")
	public String showForumPosts(HttpServletRequest request){
		
		List<ForumPost> postList = forumDao.getAllPosts();
		request.setAttribute("postList", postList);
	
		return "spaceForumOutput";
	}
	
}
