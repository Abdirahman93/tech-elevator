<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

	
<div id="alienpage">
	<h1> Solar System Geek Gift Shop </h1>
	
	<div id="four">
	<c:forEach var = "product" items= "${products}">
	  <div>
	 	 <c:url value="/productDetails" var="productDetails">
	 	 	<c:param name="productId" value="${product.id}" />
	 	 </c:url>
	  	<a href="${productDetails}"><img style = "width:100px" src="img/${product.imageName}" /></a>
		<c:out value="${product.name}" /> <br>
		<c:out value="${product.price}" /> <br>
		      </div>
	</c:forEach>
		
</div>
</div>