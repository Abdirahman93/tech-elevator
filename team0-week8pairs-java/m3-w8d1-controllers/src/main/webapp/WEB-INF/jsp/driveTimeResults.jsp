<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

	<div id="alienpage">
		<img style="width:200px" src="${planet.imagePath}" />
		<p> Traveling by ${planet.formOfTransportation} you will reach ${planet.planetName} in ${planet.travelTime} years. You will be ${planet.alienAge} years old. </p>