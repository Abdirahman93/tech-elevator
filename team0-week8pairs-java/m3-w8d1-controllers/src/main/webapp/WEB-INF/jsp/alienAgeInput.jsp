<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

	<h1> Alien Age Calculator </h1>
	<link rel="stylesheet" href="css/solarsystemgeek.css" />
	<c:url var="formAction" value="/alienAgeResults" />
	
	<div id="alienpage">
		<form method="GET" action="${formAction}">
		
			<label for="planetName">Choose a Planet </label>
			<select name="planetName">
				<option value="Mercury">Mercury</option>
				<option value="Venus">Venus</option>
				<option value="Mars">Mars</option>
				<option value="Jupiter">Jupiter</option>
				<option value="Saturn">Saturn</option>
				<option value="Uranus">Uranus</option>
				<option value="Neptune">Neptune</option>		
			</select>
			
			<br>
		
			<label for="earthAge">Enter Your Earth Age</label>
			<input type="text" name="earthAge">
			<br>
			<input type="submit" value="Calculate Age" />
		</form>
	
	</div>