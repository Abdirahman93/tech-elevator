<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div id="alienpage">
	
	<h3 style="text-align: center"> Solar System Geek Gift Shop</h3>
	<div id = details>
	
	<img style = "width:250px" src="img/${product.imageName}" />
	
	<div id = "one">
		<c:out value="${product.name}" /> <br>
 		<c:out value="${product.price}" /> <br>
		<c:out value="${product.description}" /> <br>
	

	
	<c:url var="formAction" value="/productDetails"/>

	<form method="POST" action="${formAction}">
		<label for="quantity">Quantity to Buy:  </label>
		<input type="hidden" name="productId" value="${product.id}" />
		<input type="text" name="quantity" /> <br>
		
		<input type="submit" value="Add to Cart" />
	
	</form>
	</div>
		</div>
</div>