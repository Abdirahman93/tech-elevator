<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

	<h1> Make a New Post </h1>
	
	<c:url var="formAction" value="/spaceForumInput" />
	
	<div id="alienpage">
		<form method="POST" action="${formAction}">
		
			<label for="username">Username</label>
				<input type="text" name="username" /> <br>
				
			<label for="subject">Subject</label>
				<input type="text" name="subject" /> <br>
			
			<label for="message">Message</label>
				<input type="text" name="message" /> <br>
				
			<input type="submit" value="Make Post" />
		
		</form>
	</div>
		