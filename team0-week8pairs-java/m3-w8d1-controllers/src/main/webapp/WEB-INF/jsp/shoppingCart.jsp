<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<div id="alienpage">

	<h3 style="text-align: center">Shopping Cart</h3>
	<div id="two">
		<c:forEach var="item" items="${shoppingCart}">
			<div>
				<img style="width: 100px" src="img/${item.product.imageName}">
				<c:out value="${item.product.name}" />
				<c:out value="${item.quantity}" />
				<c:out value="${item.totalCost}" />
			</div>
		</c:forEach>

		<br>

		<c:out value="Grand total: ${grandTotal}" />


		<c:url value="/shoppingCart" var="shoppingCart" />
		<form method="POST" action="${shoppingCart}">
			<input type="submit" value="Check Out" />
		</form>
	</div>
</div>
