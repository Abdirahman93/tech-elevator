<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

	<div id="alienpage">
		<c:url var="input" value="/spaceForumInput" />
		<a href="${input}">Make a Post </a>
		
		<br> 
		<div class = posts>
		<c:forEach var="post" items="${postList}">
			<div class="post">
				<p> ${post.subject}
				<p> by ${post.username} on ${post.datePosted} </p>
				<p> ${post.message} </p>
			
			</div>
		</c:forEach>
		</div>
	</div>
	